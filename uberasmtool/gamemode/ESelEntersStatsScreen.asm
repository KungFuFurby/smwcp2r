;OW game mode: Press Select to enter Stats Screen by Blind Devil
;Includes:
;nmi: check the overworld pointer process and the move OW screen flag, and if select is pressed, go to game mode 30 and save the game
;...don't get why it breaks on main... something related to how the (hijacked) gamemode pointer works?
;also handles main map/sky custom warps depending on map Y-pos and the respective flag value set by OW codes.

!WarpTimer = $0F08|!addr	;1 byte, from the vanilla status bar area
!Flaggity = $0F09|!addr		;same as above

nmi:
LDA $13D4		;load free map camera moving flag
BNE cwarp		;if set, skip ahead.
LDA $13D9		;load overworld pointers
CMP #$03		;check if player is standing still on a tile
BNE cwarp		;if not, skip ahead.

LDA $16			;load controller data 1, first frame only
AND #$20		;check if select is pressed
BEQ cwarp		;if not, skip ahead.

LDA #$00		;load value
STA $7FC0C0		;store to "apparently a pointer/flags to run exanimation dmas?" low byte.
STA $7FC0C1		;store to "apparently a pointer/flags to run exanimation dmas?" high byte.
			;this seem to fix an issue in which certain exanimation stuff would overwrite our vram uploads in our custom game modes.

JSL $009BC9|!bank	;save game

LDA #$30		;load game mode number
STA $0100		;store and go to respective game mode.

ret:
RTL

cwarp:
LDA !Flaggity
BEQ ret

LDX #$0B
STX $0100|!addr

STZ !Flaggity

LDX #$20
STX !WarpTimer

CMP #$02
BEQ .mainmapwarp

REP #$20
LDA #$0018
STA $1F17|!addr
LDA #$0208		;has to end with 8
STA $1F19|!addr
SEP #$20

LDA #$03
STA $1F11|!addr		;go to sky map
RTL

.disableflag
STZ !Flaggity
RTL

.mainmapwarp
REP #$20
LDA #$01D8
STA $1F17|!addr
LDA #$0008		;has to end with 8
STA $1F19|!addr
SEP #$20

STZ $1F11|!addr		;go to main map
RTL