;SMW Central logo thing by Blind Devil
;(uses no free RAM - instead, it uses a few sprite addresses as free RAM lol)

;NOTE: THERE ARE SOME BUILT-IN HIJACKS TO DISABLE WRITING THE ORIGINAL LOGO TO OAM AND DECREMENTING $1DF5!

;Configurable tables/defines:
;GFX FILE NUMBER FOR LOGO
;What (Ex)GFX file number should be used for the logo.
	!ExGFX = $80

;TILEMAPS
;What tiles are used by each part of the logo.
	!STop = $2A
	!SBottom = $2C

	!MTopLeft = $02
	!MTopRight = $04
	!MBottomLeft = $22
	!MBottomRight = $24

	!WTopLeft = $06
	!WTopRight = $08
	!WBottomLeft = $26
	!WBottomRight = $28

	!CPart1 = $0A
	!CPart2 = $0C
	!CPart3 = $0E
	!CPart4 = $20

;PALETTES
;These are the colors used by guess what. They apply to all tiles.
LogoPal:
dw $1084,$1CE7,$04D8,$051F,$02E0,$13A4,$06DA,$03BF,$6201,$7680

BriHDMATable:
db $70,$0F,$6F,$00,$00

;Code starts below.

init:
LDA #$38		;load amount of frames
STA $CB			;store to pointer timer.

LDA #$68		;load value
STA $C5			;store to SMW letters' Y-pos offset.
LDA #$7A		;load value
STA $C2			;store to M letter's X-pos offset.
LDA #$70		;load value
STA $C3			;store to W letter's X-pos offset.

LDA #$70		;load scanline count
STA $1510|!addr		;store to address.
STA $1512|!addr		;store to address.
LDA #$0F		;load brightness value
STA $1511|!addr		;store to address.
STZ $1514|!addr		;reset address (end of HDMA table).

REP #$20
LDX #$12
-
LDA LogoPal,x
STA $0809|!addr,x
DEX #2
BPL -
;SEP #$20

JSL DMA_CGRAM		;upload palette
RTL

main:
LDA $C8			;load bitflags
AND #$02		;check if HDMA bit is set
BEQ +			;if not, skip ahead.

REP #$20		;16-bit A
LDA.w #$1510|!addr	;load address of brightness HDMA table
STA $00			;store to scratch RAM.
if !sa1
	LDX #$00		;load bank value
else
	LDX #$7E		;load bank value
endif
STX $02			;store to scratch RAM.
JSL HDMA_Brightness	;call brightness HDMA handling code.

+
LDA $CB			;load pointer timer
BEQ +			;if equal zero, don't decrement it anymore.
DEC $CB			;decrement timer by one
+

LDA $CA			;load step pointer
CMP #$01		;compare to value
BEQ Step1		;if equal, run step 1 code.
CMP #$02		;compare again
BEQ Step2		;if equal, run step 2 code.

LDA $0DAE|!addr		;load brightness address
CMP #$0F		;compare to value
BEQ +			;if equal, skip ahead.

LDA $13			;load frame counter
AND #$01		;preserve bit 0
BNE Ret			;if set, draw GFX and return.

INC $0DAE|!addr		;increment brightness by one

+
LDA $CB			;load pointer timer
BNE NoInc1		;if not equal zero, don't increment the pointer.

LDA #$02		;load bit value
TSB $C8			;set to bitflags address (enable brightness HDMA).
LDA #$40		;load amount of frames
STA $CB			;store to pointer times.
INC $CA			;increment step pointer by one.
RTL			;return.

NoInc1:
CMP #$10		;compare to value
BCC Ret			;if lower, stop moving the letters.

LDA $13			;load frame counter
AND #$01		;preserve bit 0
BNE Ret			;if set, draw GFX and return.

DEC $C5			;decrement SMW letters' Y-pos offset by one

Ret:
JSR DrawGFX		;call OAM drawing routine
RTL			;return.

Step1:
LDA $CB			;load pointer timer
BNE NoInc2		;if not equal zero, don't increment the pointer.

LDA #$01		;load bit value
TSB $C8			;set to bitflags address (enable 'Central' GFX).
LDA #$98		;load amount of frames
STA $CB			;store to pointer timer.
INC $CA			;increment step pointer by one.
RTL			;return.

NoInc2:
CMP #$10		;compare to value
BCC Ret			;if lower, stop moving the letters.

LDA $13			;load frame counter
AND #$01		;preserve bit 0
BNE Ret			;if set, draw GFX and return.

DEC $C2			;decrement S letter's X-pos by one
INC $C3			;increment W letter's X-pos by one
BRA Ret			;call OAM drawing routine and return

Step2:
LDA $CB			;load pointer timer
BNE +			;if not equal zero, skip ahead.

LDA #$02		;load bit value
TRB $C8			;clear from bitflags (stop processing HDMA).
STZ $0D9F|!addr		;disable HDMA.
STZ $1DF5|!addr		;reset logo timer.

+
LDA $1513|!addr		;load brightness from second half of screen
CMP #$0F		;compare to value
BEQ Ret			;if equal, call OAM drawing routine and return.

LDA $13			;load frame counter
AND #$01		;preserve bit 0
BNE Ret			;if set, draw GFX and return.

INC $1513|!addr		;increment brightness by one
BRA Ret			;call OAM drawing routine and return

XDisp:
db $00,$00,$10,$10

YDisp:
db $00,$10,$00,$10

STiles:
db !STop, !SBottom

MTiles:
db !MTopLeft,!MBottomLeft,!MTopRight,!MBottomRight

WTiles:
db !WTopLeft,!WBottomLeft,!WTopRight,!WBottomRight

CXDisp:
db $00,$10,$20,$30

CTiles:
db !CPart1,!CPart2,!CPart3,!CPart4

DrawGFX:
LDY #$00		;load OAM index into Y

LDX #$01		;loop count (S)
-
LDA $C2			;load X-pos from address
CLC			;clear carry
ADC XDisp,x		;add X displacement from table according to index
STA $0200|!addr,y	;store to OAM.
LDA $C5			;load Y-pos from address
CLC			;clear carry
ADC YDisp,x		;add X displacement from table according to index
STA $0201|!addr,y	;store to OAM.
LDA STiles,x		;load tilemap from table according to index
STA $0202|!addr,y	;store to OAM.
LDA #$30		;load palette/properties/flips/whatever
STA $0203|!addr,y	;store to OAM.
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL -			;loop while it's positive.

LDX #$03		;loop count (M)
-
LDA #$70		;load X-pos value
CLC			;clear carry
ADC XDisp,x		;add X displacement from table according to index
STA $0200|!addr,y	;store to OAM.
LDA $C5			;load Y-pos from address
CLC			;clear carry
ADC YDisp,x		;add X displacement from table according to index
STA $0201|!addr,y	;store to OAM.
LDA MTiles,x		;load tilemap from table according to index
STA $0202|!addr,y	;store to OAM.
LDA #$30		;load palette/properties/flips/whatever
STA $0203|!addr,y	;store to OAM.
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL -			;loop while it's positive.

LDX #$03		;loop count (W)
-
LDA $C3			;load X-pos from address
CLC			;clear carry
ADC XDisp,x		;add X displacement from table according to index
STA $0200|!addr,y	;store to OAM.
LDA $C5			;load Y-pos from address
CLC			;clear carry
ADC YDisp,x		;add X displacement from table according to index
STA $0201|!addr,y	;store to OAM.
LDA WTiles,x		;load tilemap from table according to index
STA $0202|!addr,y	;store to OAM.
LDA #$30		;load palette/properties/flips/whatever
STA $0203|!addr,y	;store to OAM.
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL -			;loop while it's positive.

LDA $C8			;load bitflags
AND #$01		;check if Central bit is set
BEQ +			;if not, don't draw it.

LDX #$03		;loop count (Central)
-
LDA #$62		;load X-pos
CLC			;clear carry
ADC CXDisp,x		;add X displacement from table according to index
STA $0200|!addr,y	;store to OAM.
LDA #$70		;load Y-pos
STA $0201|!addr,y	;store to OAM.
LDA CTiles,x		;load tilemap from table according to index
STA $0202|!addr,y	;store to OAM.
LDA #$30		;load palette/properties/flips/whatever
STA $0203|!addr,y	;store to OAM.
INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL -			;loop while it's positive.

+
LDA #$AA		;load tile size/high bit/what's this I don't even (make all tiles 16x16)
STA $0400|!addr		;store to OAM.
STA $0401|!addr		;store to OAM.
STA $0402|!addr		;store to OAM.
STA $0403|!addr		;store to OAM.
RTS			;return.

;BUILT-IN HIJACKS BELOW!
pushpc
org $00939A
BRA $1F			;branch to $8093BB, don't write Nintendo tiles to OAM.

org $0093CB
db $00			;set brightness to zero (AFFECTS TIME UP/GAME OVER/MARIO START TOO!)

org $00940F
LDA $1DF5|!addr		;load logo timer instead of decrementing it

org $00A9CD
db !ExGFX		;(Ex)GFX file to use for the logo
pullpc