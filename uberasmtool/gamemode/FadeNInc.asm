;Custom Game Mode: Fade music and screen to black and increment gamemode address
;by Blind Devil

!fademusic = 0

init:
if !fademusic
LDA #$FF		;load value
STA $1DFB|!addr		;store to music address to make the song fade out.
endif

STA $9D			;store also to sprites/animations locked flag.
RTL			;return.

main:
LDA $0DAE|!addr		;load brightness mirror
BEQ IncGM		;if equal zero, increment game mode.

DEC $0DAE|!addr		;decrement brightness by one
RTL			;return.

IncGM:
INC $0100|!addr		;increment game mode by one
RTL			;return.