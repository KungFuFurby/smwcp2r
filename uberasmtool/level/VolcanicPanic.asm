;Volcanic Panic ASM.
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: parallax X offset processing, 2-channel HDMA gradient
;main: parallax X offset processing, layer 2 parallax logic, perfect slippery blocks handler
;nmi: manual BG Y-pos updater

load:
STZ $1414|!addr			;set BG Y-scroll in LM to none.
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax
JSL FireIceParallax_HDMA	;execute HDMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

main:
JSL FireIceParallax_Logic	;handle layer 2 parallax logic
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax

nmi:
JML FireIceParallax_NMIBGUpd

Table1:
db $10,$28,$81
db $0F,$28,$82
db $12,$29,$82
db $1C,$29,$83
db $02,$29,$84
db $14,$2A,$84
db $0B,$2A,$85
db $09,$2B,$85
db $0D,$2B,$86
db $22,$2A,$86
db $3A,$2B,$86
db $00

Table2:
db $0C,$42
db $14,$43
db $15,$44
db $10,$45
db $0C,$46
db $0A,$47
db $0B,$48
db $09,$49
db $09,$4A
db $1A,$4B
db $1A,$4C
db $34,$4D
db $00