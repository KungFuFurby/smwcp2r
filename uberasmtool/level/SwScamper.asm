;Switch Scamper ASM.
;Includes:
;init: 2-channel HDMA gradient, message box text DMA
;init and nmi: calls multilayer background handling - parallax HDMA, layer 3 message/parallax HDMA positioning.
;main: parallax logic

!WindowTable = $7F8900

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL GirderMultilayer_PrepareTable
JSL GirderMultilayer_Logic

REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
BRA +				;branch ahead - part of nmi code isn't relevant here

nmi:
lda $71
cmp #$09
bne .hhh
rtl

.hhh
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
JML GirderMultilayer_HDMAOAM

main:
STZ $87				;reset address (corresponds to !ManualMsg in GirderMultilayer.asm, library file)

LDA $95				;load player's X-pos within the level, next frame, high byte
CMP #$05			;compare to value
BCC +				;if lower, skip ahead.

LDA #$01			;load value
STA $87				;store to address.

+
JML GirderMultilayer_Logic

Table1:
db $80,$3E,$55
db $18,$3E,$55
db $48,$30,$52
db $00

Table2:
db $80,$88
db $18,$88
db $48,$87
db $00

MsgDMA1:
db $FE,$28,$07,$29,$54,$29,$51,$29,$51,$29,$58,$29,$1A,$29,$FE,$28
db $11,$29,$54,$29,$52,$29,$47,$29,$1A,$29,$FE,$28,$11,$29,$54,$29
db $52,$29,$47,$29,$1A,$29,$FE,$28,$13,$29,$47,$29,$44,$29,$FE,$28
db $53,$29,$48,$29,$4C,$29,$44,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$4B,$29,$48,$29,$4C,$29,$48,$29,$53,$29,$FE,$28,$4E,$29
db $4D,$29,$FE,$28,$53,$29,$47,$29,$44,$29,$52,$29,$44,$29,$FE,$28
db $52,$29,$56,$29,$48,$29,$53,$29,$42,$29,$47,$29,$44,$29,$52,$29
db $FE,$28,$48,$29,$52,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$52,$29,$47,$29,$4E,$29,$51,$29,$53,$29,$1D,$29,$FE,$28
db $52,$29,$4E,$29,$FE,$28,$4C,$3D,$40,$3D,$4A,$3D,$44,$3D,$FE,$3C
db $53,$3D,$47,$3D,$44,$3D,$FE,$3C,$4C,$3D,$4E,$3D,$52,$3D,$53,$3D
db $FE,$3C,$4E,$3D,$54,$3D,$53,$3D,$FE,$3C,$4E,$3D,$45,$3D,$FE,$28
db $FE,$28,$53,$3D,$47,$3D,$44,$3D,$4C,$3D,$1A,$29,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28

MsgDMA2:
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$00,$29,$4C,$29,$40,$29,$59,$29,$48,$29
db $4D,$29,$46,$29,$1A,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$16,$29,$47,$29,$40,$29,$53,$29
db $FE,$28,$40,$29,$4D,$29,$FE,$28,$44,$29,$57,$29,$42,$29,$44,$29
db $4B,$29,$4B,$29,$44,$29,$4D,$29,$53,$29,$FE,$28,$52,$29,$56,$29
db $48,$29,$53,$29,$42,$29,$47,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$52,$29,$42,$29,$40,$29,$4C,$29,$4F,$29,$44,$29
db $51,$29,$48,$29,$4D,$29,$46,$29,$1A,$29,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28