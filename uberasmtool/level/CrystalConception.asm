;Crystal Conception ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $07,$20,$40
db $01,$21,$40
db $0B,$21,$41
db $04,$22,$41
db $09,$22,$42
db $06,$23,$42
db $07,$23,$43
db $08,$24,$43
db $04,$24,$44
db $0B,$25,$44
db $02,$25,$45
db $0D,$26,$45
db $0C,$27,$46
db $03,$28,$46
db $09,$28,$47
db $03,$29,$47
db $07,$29,$48
db $02,$2A,$48
db $08,$2A,$49
db $01,$2B,$49
db $09,$2B,$4A
db $09,$2C,$4B
db $09,$2D,$4C
db $01,$2D,$4D
db $08,$2E,$4D
db $02,$2E,$4E
db $07,$2F,$4E
db $03,$2F,$4F
db $06,$30,$4F
db $04,$30,$50
db $05,$31,$50
db $05,$31,$51
db $04,$32,$51
db $05,$32,$52
db $04,$33,$52
db $06,$33,$53
db $03,$34,$53
db $06,$34,$54
db $00

Table2:
db $04,$80
db $07,$81
db $07,$82
db $06,$83
db $07,$84
db $07,$85
db $07,$86
db $07,$87
db $07,$88
db $06,$89
db $07,$8A
db $07,$8B
db $07,$8C
db $07,$8D
db $07,$8E
db $08,$8F
db $09,$90
db $0A,$91
db $0A,$92
db $09,$93
db $0A,$94
db $0A,$95
db $09,$96
db $0A,$97
db $0A,$98
db $09,$99
db $0A,$9A
db $0A,$9B
db $00