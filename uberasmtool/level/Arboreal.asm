;Arboreal Ascent ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;main: spawn cluster leaves when on sublevel 10

;blindnote: No leaves on 5B anymore - the star sparkles mess up with leaves.

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

main:
LDA $010B|!addr			;load current sublevel number, low byte
CMP #$10			;compare to value
BNE .ret			;if not equal, return.

LDA #$0D		;load sprite number to spawn
JSL clusterspawn_run	;spawn cluster leafy leaves

.ret
RTL				;return.

Table1:
	db $0E,$3A,$5D
	db $1B,$3B,$5D
	db $1B,$3C,$5D
	db $1B,$3D,$5D
	db $1C,$3E,$5D
	db $43,$3F,$5D
	db $07,$3F,$5C
	db $06,$3F,$5B
	db $07,$3F,$5A
	db $06,$3F,$59
	db $07,$3F,$58
	db $01,$3F,$57
	db $00

Table2:
	db $10,$9C
	db $0F,$9B
	db $11,$9A
	db $10,$99
	db $0F,$98
	db $10,$97
	db $0E,$96
	db $11,$95
	db $10,$94
	db $0F,$93
	db $43,$92
	db $00