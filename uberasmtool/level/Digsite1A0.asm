;Digsite Dangers ASM (level 1A0).
;Includes:
;load: SMWC coin custom trigger init
;init: layer 2 RAM initializer
;main: layer 2 trigger handler, violent quicksand handling

!L2Timer = $0F5E|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL quicksink_prephdma		;prepare HDMA effects and stuff for level from library code
JSL HDMA_Color3			;execute HDMA (A goes 8-bit after this routine)

STZ !L2Timer			;reset ram address
return:
RTL				;return.

main:
JSL quicksink_run		;run violent quicksand code

LDA $9D				;load sprites/animation locked flag
ORA $13D4|!addr			;OR with game paused flag
BNE return			;if any of them are set, return.

	LDA $9D		;\ If sprites locked,
	BEQ GOGO	;/ branch
	RTL
GOGO:
	LDA $14AF	;Load On/Off...
	BEQ Off		;If on, branch to off
Rising:
	LDA $1869	;
	CMP #$3F	;Load height RAM
	BEQ DoneG	;Branch if too high
	
NoSound:

	LDA $1879	;
	CMP #$30	;Set delay timer
	BEQ NEXTA	;branch if ready

	INC $1879	;increase delay timer.
	RTL
NEXTA:

	INC $1864	;Increase timer

	LDA $1864	;
	CMP #$03	;See if timer is 4
	BNE ReturnG	;If not, branch

	LDA #$03	;Earthquake
	STA $1887	;
	LDA #$1A
	STA $1DFC

	INC $1869	;Increase HeightRAM
	STZ $1864	;Increase timer
	INC $1468	;Increase layer 2
	RTL
Off:
	LDA $1869	;
	BEQ DoneG	;If zero, return.


	LDA $1879	;
	CMP #$30	;Set delay timer
	BEQ NEXTOR	;branch if ready

	INC $1879	;increase delay timer.
	RTL

NEXTOR:
	INC $1864	;Increase timer

	LDA $1864	;
	CMP #$03	;See if timer is 4
	BNE ReturnG	;If so, branch

	LDA #$03	;Earthquake
	STA $1887	;
	LDA #$1A
	STA $1DFC

	DEC $1869	;Increase HeightRAM
	STZ $1864	;Increase timer
	DEC $1468	;Increase layer 2
ReturnG:
	RTL
DoneG: 	
	
YEEEAAAH:
	STZ $13D3
	STZ $1879
	
Turn_off:
	LDA $7FC0FC
	AND #$FE
	STA $7FC0FC
	RTL		;Return