;Girder Grassland ASM.
;Includes:
;load: SMWC coin trigger initializer
;init: 2-channel HDMA gradient
;init and nmi: calls multilayer background handling - parallax HDMA, and layer 3 message/parallax HDMA positioning.
;main: parallax logic

!WindowTable = $7F8900

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL GirderMultilayer_PrepareTable
JSL GirderMultilayer_Logic

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
BRA +				;branch ahead - part of nmi code isn't relevant here

nmi:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BNE +			;if not, skip ahead.

LDA #$02		;specifically for this level, layer 3 windowing behaves differently so we gotta mess up the window mask settings for this.
STA $43			;also note how this is on nmi code - it doesn't work on main at all

+
JML GirderMultilayer_HDMAOAM

main:
STZ $87				;reset address (corresponds to !ManualMsg in GirderMultilayer.asm, library file)

LDA $95				;load player's X-pos within the level, next frame, high byte
CMP #$09			;compare to value
BCC +				;if lower, skip ahead.

LDA #$01			;load value
STA $87				;store to address.

+
JML GirderMultilayer_Logic

Table1:
db $80,$20,$52
db $12,$20,$52
db $53,$24,$53
db $00

Table2:
db $80,$9F
db $12,$9F
db $53,$87
db $00