;Blazing Brush ASM (level 6D).
;Includes:
;load: SMWC coin trigger initializer
;init: 2-channel HDMA gradient (FG)
;main: spawn cluster leaves

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JML HDMA_BlazingBrush		;run HDMA routine and return.

main:
LDA #$0D		;load sprite number to spawn
JML clusterspawn_run	;spawn cluster leafy leaves