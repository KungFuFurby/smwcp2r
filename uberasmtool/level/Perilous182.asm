;Perilous Paths ASM (level 182).
;Includes:
;init: message box text DMA
;main: on/off (reverse) gravity handler, kill player if on water (swimming on lava lol)

!flip_gravity		= $00B091|!bank
!reversed		= $1B7F|!addr

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA2>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA2			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return.

main:
LDA $75
BEQ +

STZ $19
JSL $00F5B7|!bank

+
		LDA $14AF|!addr
		CMP !reversed
		BEQ .return
		
		JSL !flip_gravity
		LDA $7D
		EOR #$FF
		INC
		STA $7D
		
.return
RTL

MsgDMA2:
incbin "msgbin/131-1msg2msg.bin":200-400