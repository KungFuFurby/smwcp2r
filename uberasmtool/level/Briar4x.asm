;Briar Mire ASM #2.
;Includes:
;init: layers 2 and 3 on subscreen
;main: kill player when water is touched

init:
LDA #$11		;layer 1 and sprites on main screen
STA $212C
LDA #$17		;everything else on subscreen
STA $212D
RTL

main:
LDA $75			;load player is in water flag
BEQ +			;if not, skip ahead.

STZ $19			;remove player's powerup.
JSL $00F5B7|!bank	;hurt the player

+
RTL