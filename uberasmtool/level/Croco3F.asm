;Crocodile Crossing ASM (levels 3D and 3F).
;Includes:
;load: SMWC coin trigger initializer
;init: 2-channel HDMA gradient
;nmi: custom layer 3 scrolling

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

nmi:
REP #$20
LDA $1466|!addr
LSR #2
CLC
ADC $1466|!addr
STA $22
SEP #$20

+
RTL				;return.

Table1:
db $02,$32,$51
db $07,$33,$51
db $05,$33,$52
db $05,$34,$52
db $01,$34,$53
db $04,$35,$53
db $02,$36,$53
db $02,$36,$54
db $06,$37,$54
db $05,$38,$54
db $03,$38,$55
db $04,$39,$55
db $03,$3A,$55
db $02,$3A,$56
db $04,$3B,$56
db $02,$3C,$56
db $03,$3C,$57
db $06,$3D,$57
db $09,$3E,$58
db $15,$3F,$58
db $08,$3E,$57
db $06,$3E,$56
db $04,$3E,$55
db $01,$3D,$55
db $05,$3D,$54
db $05,$3D,$53
db $02,$3D,$52
db $02,$3C,$52
db $05,$3C,$51
db $06,$3C,$50
db $08,$3C,$4F
db $11,$3C,$4E
db $3B,$3C,$4D
db $00

Table2:
db $21,$97
db $06,$96
db $07,$95
db $0F,$94
db $25,$93
db $10,$92
db $0D,$91
db $0D,$90
db $54,$8F
db $00