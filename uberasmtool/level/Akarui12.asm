;Akarui Avenue ASM (levels 12 and 1C2).
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: message box text DMA, 2-channel HDMA gradient, parallax X offset processing
;main: change (low byte of) exit of screen 07 depending on the on/off switch status, parallax X offset processing
;nmi: very slow BG V-scroll relative to layer 1

load:
LDA $1464|!addr			;load layer 1 Y-pos, next frame
STA $1C				;store to layer 1 Y-pos, current frame.
STZ $1414|!addr			;set BG Y-scroll in LM to none.
JSL Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins
BRA nmi

init:
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_HDMA		;execute HDMA

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA2>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDX.b #Table1>>16		;load bank of one of the tables
STX $02				;store to scratch RAM.
STX $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
SEP #$20			;8-bit A

nmi:
JML OrientParallax_NMIBGUpd	;update layer 2 Y-pos manually

main:
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax

LDY #$07			;load screen number into Y
LDA #$C0			;load exit number, low byte

LDX $14AF|!addr			;load on/off switch flag into X
BEQ +				;if not set, return.

INC				;increment exit number in A by one

+
STA $19B8|!addr,y		;store to low byte of screen exit destination according to index.
RTL				;return.

Table1:
db $05,$3A,$49
db $01,$3A,$4A
db $05,$3B,$4A
db $02,$3B,$4B
db $03,$3C,$4B
db $05,$3C,$4C
db $05,$3D,$4D
db $07,$3D,$4E
db $07,$3D,$4F
db $06,$3D,$50
db $07,$3D,$51
db $01,$3D,$52
db $06,$3E,$52
db $07,$3E,$53
db $07,$3E,$54
db $07,$3E,$55
db $08,$3E,$56
db $07,$3E,$57
db $07,$3E,$58
db $07,$3E,$59
db $03,$3E,$5A
db $05,$3F,$5A
db $07,$3F,$5B
db $08,$3F,$5C
db $5B,$3F,$5D
db $00

Table2:
db $1C,$9D
db $09,$9C
db $0A,$9B
db $09,$9A
db $0A,$99
db $09,$98
db $0A,$97
db $0A,$96
db $0A,$95
db $0B,$94
db $0A,$93
db $0B,$92
db $57,$91
db $00

MsgDMA2:
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$4b,$39,$4E,$39,$4E,$39,$4A,$39,$FE,$38,$14,$3D,$0F,$3D
db $FE,$38,$53,$39,$4E,$39,$FE,$38,$53,$39,$47,$39,$44,$39,$FE,$38
db $52,$39,$4A,$39,$58,$39,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$40,$39,$4D,$39,$43,$39,$FE,$38
db $4b,$39,$44,$39,$53,$39,$FE,$38,$48,$39,$53,$39,$52,$39,$FE,$38
db $47,$39,$40,$39,$4b,$39,$42,$39,$58,$39,$4E,$39,$4D,$39,$FE,$38
db $4b,$39,$48,$39,$46,$39,$47,$39,$53,$39,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$42,$39,$4b,$39,$44,$39,$40,$39,$4D,$39,$52,$39
db $44,$39,$FE,$38,$53,$39,$47,$39,$44,$39,$FE,$38,$43,$39,$40,$39
db $51,$39,$4A,$39,$FE,$38,$40,$39,$56,$39,$40,$39,$58,$39,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38