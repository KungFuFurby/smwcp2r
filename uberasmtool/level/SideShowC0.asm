;Sideshow Showdown ASM (level C0).
;Includes:
;load: setup custom trigger 1 depending on BP/games
;init: setup custom trigger 0 and screen 03 exit depending on BP/games
;init/nmi: 2-channel HDMA gradient, store gradient table to RAM
;nmi: setup scrolling gradient

!win = $00C3		;level dest

load:
	JSR countgames
	CMP #$03
	BCC closed

LDA $7FC0FC
ORA #$02
STA $7FC0FC
RTL

closed:
LDA $7FC0FC
AND #$FD
STA $7FC0FC
RTL

init:
JSL SideshowHDMA_init

	JSR countgames
	CMP #$03
	BCC ThreeGames
	BEQ ThreeGames
	CMP #$04
	BEQ FourGames
FiveGames:
	LDA $7F9A81
	CMP #$0B
	BCS NoGo		;survived all 5 games? go on ahead

DoorToHeaven:
LDA $7FC0FC
AND #$FE
STA $7FC0FC

	LDX #$03
	LDA.b #!win
	STA $19B8|!addr,x
	LDA.b #!win>>8
	ORA #$04
	STA $19D8|!addr,x
	RTL

FourGames:
	LDA $7F9A81
	CMP #$06		;lost 5 or less points after 4 games? go ahead
	BCC DoorToHeaven
	BRA NoGo

ThreeGames:
	LDA $7F9A81
	BEQ DoorToHeaven	;no loss after 3 games? good to go

NoGo:
LDA $7FC0FC
ORA #$01
STA $7FC0FC
RTL

countgames:
	LDX #$04
	LDY #$00
-	LDA $7F9A83
	AND DoorsWhich,x
	BEQ +
	INY
+	DEX
	BPL -

	TYA
	RTS

nmi:
JML SideshowHDMA_nmi

DoorsWhich:
db $01,$02,$04,$08,$10