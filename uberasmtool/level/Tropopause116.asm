;Tropopause Trail ASM (level 116).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 handler, 2-channel HDMA gradient, initialize autoscroll
;main: vertical autoscroll whenever Mario steps on a Lakitu cloud

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

LDA #$3F			;load value
STA $0F5E|!addr			;store to our autoscroll timer.
STZ $0F5F|!addr			;reset autoscroll flag.
RTL				;return.

main:
LDA $0F5F|!addr			;load autoscroll flag
BNE +				;if not zero, run autoscroll stuff.

LDA $18C2|!addr			;load player is inside Lakitu cloud flag
BEQ ret				;if not on a cloud, return.

INC $0F5F|!addr			;increment autoscroll flag
RTL				;return.

+
LDA $9D				;load sprites/animations locked flag
ORA $13D4|!addr			;OR with game paused flag
BNE ret				;if any are set, return.

LDA $0F5E|!addr			;load timer which is not really a timer
BEQ +				;if zero, skip ahead.

DEC $0F5E|!addr			;decrement address by one
LDA $0F5E|!addr			;load timer which is now a timer

+
LSR #4				;divide by 2 four times
TAY				;transfer to Y

LDA $14				;load effective frame counter
AND FramalAccel,y		;preserve bits according to index
BNE ret				;if any are set, return.

STZ $55				;make sure that tiles and sprites are uploaded from the top of the screen

REP #$20			;16-bit A
LDA $1C				;load layer 1 X-pos, current frame
BEQ ret16			;if zero, return.				

DEC $1464|!addr			;decrement layer 1 X-pos, next frame, by one

ret16:
SEP #$20			;8-bit A

ret:
RTL				;return.

FramalAccel:
db $01,$03,$07,$0F

Table1:
db $11,$34,$9F
db $0D,$35,$9F
db $0A,$36,$9F
db $09,$37,$9F
db $0A,$38,$9F
db $0C,$39,$9F
db $20,$3A,$9F
db $02,$3A,$9E
db $09,$3B,$9E
db $06,$3B,$9D
db $02,$3C,$9D
db $06,$3C,$9C
db $04,$3C,$9B
db $05,$3C,$9A
db $07,$3C,$99
db $02,$3D,$99
db $0B,$3D,$98
db $01,$3D,$97
db $18,$3E,$97
db $07,$3E,$98
db $06,$3E,$99
db $05,$3F,$9A
db $06,$3F,$9B
db $06,$3F,$9C
db $0C,$3F,$9D
db $00

Table2:
db $0A,$43
db $07,$44
db $05,$45
db $04,$46
db $03,$47
db $04,$48
db $03,$49
db $03,$4A
db $03,$4B
db $03,$4C
db $03,$4D
db $03,$4E
db $03,$4F
db $03,$51
db $03,$52
db $04,$53
db $04,$54
db $05,$55
db $06,$56
db $19,$57
db $0D,$58
db $09,$59
db $12,$5A
db $0B,$5B
db $1D,$5C
db $0E,$5D
db $0E,$5E
db $0C,$5F
db $00