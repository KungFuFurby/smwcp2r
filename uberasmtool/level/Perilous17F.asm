init:
	REP #$20		; HDMA gradient
	LDA #$3200
	STA $4330
	LDA.w #.Table1
	STA $4332
	SEP #$20
	LDA.b #.Table1>>16
	STA $4334
	LDA #$08
	TSB $0D9F
	
	LDA #$17
	STA $212C
	STA $212E
	LDA #$00
	STA $212D
	STA $212F
	
	LDA #$84
	STA $40
	RTL

.Table1:
db $13,$E0
db $04,$E1
db $03,$E2
db $03,$E3
db $02,$E4
db $02,$E5
db $02,$E6
db $02,$E7
db $02,$E8
db $93,$E9,$EA,$EB,$EC,$ED,$EE,$EF,$F0,$F1,$F2,$F3,$F4,$F5,$F6,$F7,$F8,$F9,$FB,$FD
db $72,$FF
db $93,$FD,$FB,$F9,$F8,$F7,$F6,$F5,$F4,$F3,$F2,$F1,$F0,$EF,$EE,$ED,$EC,$EB,$EA,$E9
db $02,$E8
db $02,$E7
db $02,$E6
db $02,$E5
db $02,$E4
db $03,$E3
db $03,$E2
db $04,$E1
db $04,$E0
db $00
