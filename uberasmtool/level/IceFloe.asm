;Ice-Floe Inferno ASM.
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: parallax X offset processing, 2-channel HDMA gradient
;init and nmi: layer 2 parallax logic
;main: parallax X offset processing, perfect slippery blocks handler

load:
STZ $1414|!addr			;set BG Y-scroll in LM to none.
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax
JSL FireIceParallax_HDMA	;execute HDMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

main:
JSL FireIceParallax_Logic	;handle layer 2 parallax logic
JSL FireIceParallax_ProcXOffsets;handle X position update for layer 2 parallax
JML SlipperyBlocks_main		;call slippery blocks handling routine and return.

nmi:
JML FireIceParallax_NMIBGUpd	;update BG pos manually

Table1:
db $0B,$2A,$41
db $05,$2B,$41
db $01,$2C,$41
db $03,$2C,$42
db $04,$2D,$42
db $04,$2E,$42
db $04,$2F,$42
db $03,$30,$42
db $01,$30,$43
db $04,$31,$43
db $04,$32,$43
db $03,$33,$43
db $03,$34,$43
db $02,$35,$43
db $0C,$35,$44
db $11,$36,$44
db $0F,$37,$44
db $01,$37,$45
db $04,$38,$45
db $06,$38,$46
db $03,$38,$47
db $02,$39,$47
db $06,$39,$48
db $05,$39,$49
db $06,$3A,$4A
db $05,$3A,$4B
db $03,$3A,$4C
db $57,$3B,$4C
db $00

Table2:
db $0D,$92
db $11,$93
db $11,$94
db $06,$95
db $06,$94
db $06,$93
db $08,$92
db $08,$91
db $09,$90
db $07,$8F
db $03,$8E
db $02,$8D
db $03,$8C
db $03,$8B
db $03,$8A
db $03,$89
db $02,$88
db $03,$87
db $03,$86
db $03,$85
db $03,$84
db $02,$83
db $03,$82
db $03,$81
db $58,$80
db $00