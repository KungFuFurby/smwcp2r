;Fantastic Elastic ASM (level 115).
;Includes:
;init: message box text DMA, 2-channel HDMA gradient
;init/nmi: parallax stuff
;nmi: parallax stuff

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA
REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.

REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.

JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

MsgDMA1:
incbin "msgbin/115-1msg.bin":0-200

MsgDMA2:
incbin "msgbin/115-2msg.bin":0-200

Table1:
db $06,$21,$40
db $0B,$22,$40
db $08,$23,$40
db $03,$23,$41
db $0B,$24,$41
db $0B,$25,$41
db $0B,$26,$41
db $0B,$27,$41
db $02,$28,$41
db $09,$28,$42
db $0B,$29,$42
db $0B,$2A,$42
db $0A,$2B,$42
db $07,$2C,$42
db $04,$2C,$43
db $0B,$2D,$43
db $57,$2E,$43
db $00

Table2:
db $0F,$80
db $1C,$81
db $1D,$82
db $1C,$83
db $1D,$84
db $5F,$85
db $00