;.MSA EREHPS EHT RAEF (8F dna 83 slevel)
;Includes:
;load: rezilaitini reggirt nioc CWMS
;init: tneidarg AMDH lennahc-2

;edit: added auto level end code for level 8F

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA $010B|!addr			;load sublevel number, low byte
CMP #$83			;compare to value
BEQ +				;if equal, don't set level to end.

LDA #$0B			;load value
STA $1696|!addr			;store to disable screen barrier flag - and run boss beaten goal stuff.

+
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $19,$20,$40
db $0E,$21,$40
db $0C,$22,$40
db $0F,$23,$40
db $10,$24,$40
db $0A,$23,$40
db $07,$22,$40
db $15,$21,$40
db $07,$21,$41
db $05,$22,$42
db $03,$22,$43
db $01,$23,$43
db $03,$23,$44
db $04,$24,$45
db $02,$24,$46
db $02,$25,$46
db $02,$25,$47
db $01,$25,$48
db $03,$26,$48
db $02,$26,$49
db $03,$27,$4A
db $02,$27,$4B
db $02,$28,$4B
db $02,$28,$4C
db $01,$29,$4C
db $05,$29,$4D
db $05,$2A,$4E
db $05,$2A,$4F
db $02,$2B,$4F
db $01,$2B,$50
db $29,$2B,$51
db $00

Table2:
db $15,$80
db $0A,$81
db $07,$82
db $05,$83
db $08,$84
db $07,$85
db $0E,$86
db $14,$87
db $0A,$88
db $17,$89
db $07,$8A
db $05,$8B
db $04,$8C
db $04,$8D
db $04,$8E
db $03,$8F
db $01,$90
db $03,$91
db $05,$92
db $04,$93
db $06,$94
db $06,$95
db $2F,$96
db $00