;Reverse Universe ASM (levels 133 and E2).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $04,$2E,$4E
db $05,$2E,$4F
db $0A,$2F,$4F
db $07,$30,$4F
db $03,$30,$50
db $0A,$31,$50
db $0A,$32,$50
db $0A,$33,$51
db $0A,$34,$51
db $02,$35,$51
db $08,$35,$52
db $0A,$36,$52
db $04,$37,$52
db $05,$37,$53
db $0A,$38,$53
db $02,$39,$53
db $06,$39,$54
db $01,$3A,$54
db $07,$3A,$55
db $07,$3B,$56
db $07,$3C,$57
db $01,$3C,$58
db $05,$3D,$58
db $02,$3D,$59
db $10,$3E,$59
db $14,$3E,$5A
db $0F,$3E,$5B
db $07,$3F,$5B
db $14,$3F,$5C
db $00

Table2:
db $19,$9F
db $30,$9E
db $47,$9D
db $02,$9E
db $0A,$9D
db $09,$9C
db $0A,$9B
db $0B,$9A
db $0B,$99
db $0B,$98
db $0B,$97
db $05,$96
db $00