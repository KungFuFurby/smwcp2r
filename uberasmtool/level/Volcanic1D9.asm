;Volcanic Panic ASM (level 1D9).
;Includes:
;init: initialize autoscroll
;main: vertical autoscroll

init:
LDA #$3F			;load value
STA $0F5E|!addr			;store to our autoscroll timer.
RTL				;return.

main:
LDA $9D				;load sprites/animations locked flag
ORA $13D4|!addr			;OR with game paused flag
BNE ret				;if any are set, return.

REP #$20			;16-bit A
LDA $1464|!addr			;load layer 1 Y-pos
BEQ ret16			;if zero, return.
SEP #$20			;8-bit A

LDA $0F5E|!addr			;load timer which is not really a timer
BEQ +				;if zero, skip ahead.

DEC $0F5E|!addr			;decrement address by one
LDA $0F5E|!addr			;load timer which is now a timer

+
LSR #4				;divide by 2 four times
TAY				;transfer to Y

LDA $14				;load effective frame counter
AND FramalAccel,y		;preserve bits according to index
BNE ret				;if any are set, return.

STZ $55				;make sure that tiles and sprites are uploaded from the top of the screen

REP #$20			;16-bit A
LDA $1C				;load layer 1 Y-pos, current frame
BEQ ret16			;if zero, return.				

DEC $1464|!addr			;decrement layer 1 Y-pos, next frame, by one

ret16:
SEP #$20			;8-bit A

ret:
RTL				;return.

FramalAccel:
db $01,$03,$07,$0F