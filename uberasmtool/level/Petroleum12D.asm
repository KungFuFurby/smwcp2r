;Petroleum Passage ASM (level 12D).
;Includes:
;init: message box text DMA
;init/main: set manual trigger C to 0 when prior screen 0D, and 1 when past screen 0D

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)

main:
LDA $95
CMP #$0D
BCC .nope

LDA #$01
BRA +

.nope
LDA #$00
+
STA $7FC07C
RTL

MsgDMA1:
incbin "msgbin/12D-1msg.bin":0-200