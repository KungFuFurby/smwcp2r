;Codes related to the overworld, though not restricted for usage in other places.

;This gets the translevel number according to the OW player's coordinates.
;Useful for OW counters, or for getting the translevel number to update things correctly on level load ._.
;Credits to Ladida for the code below.
;No inputs.
;Outputs:
;A (8-bit): level number.
GetLvlFromCoord:
LDY $0DD6|!addr
LDA $1F17|!addr,y
LSR #4 : STA $00
LDA $1F19|!addr,y
AND #$F0
ORA $00 : STA $00
LDA $1F1A|!addr,y
ASL : ORA $1F18|!addr,y
LDY $0DB3|!addr
LDX $1F11|!addr,y
BEQ +
CLC : ADC #$04
+
STA $01
REP #$10
LDX $00
LDA $7ED000,x
SEP #$10
RTL