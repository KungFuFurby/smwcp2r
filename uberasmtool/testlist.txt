verbose: on

;Use this list on a lighter ROM build to test up codes for specific places.

; UberASM Tool code list.
; You can use the same .asm file for multiple levels/OW/etc. for saving space.

; Level list. Valid values: 000-1FF.
; Insert files here
level:
6E		BlimpBattle.asm

; OW list. Valid values: 0 = Main map; 1 = Yoshi's Island; 2 = Vanilla Dome;
; 3 = Forest of Illusion; 4 = Valley of Bowser; 5 = Special World; and
; 6 = Star World.
; Insert files here
overworld:
0		Map0.asm
2		Map2.asm
3		Map3.asm
4		Map4.asm
5		Map5.asm
6		Map6.asm

; Game mode list. Valid values: 00-FF.
; Insert files here
gamemode:
1		SMWClogo.asm
2		NoLayer3.asm
8		SFXOnFileSelectOpen.asm
B		NoLayer3.asm
C		NoLayer3.asm
E		ESelEntersStatsScreen.asm
11		InitCDM16.asm
14		GoalDeath.asm
17		BriAndNoHDMA.asm
30		FadeNInc.asm
31		31PrepStatsMenu.asm
32		FadeNInc2.asm
33		33Stats.asm
global:		other/global_code.asm	; global code.
statusbar:	other/status_code.asm	; status bar code.
macrolib:	other/macro_library.asm	; macro library.
sprite:		$7FAC80			; 38 (SNES) or 68 (SA-1) bytes of free RAM.
rom:		SMWCP2R-testrom.smc		; ROM file to use.