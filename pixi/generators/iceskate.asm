;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	PRINT "INIT ",pc
	PRINT "MAIN ",pc                                    
	PHB                     
	PHK                     
	PLB                     
	JSR MAIN 
	PLB                     
	RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MAIN:
JSR IceSkatingSux

LDA #$02
STA $1401		;prevent L/R scrolling at any moment

LDA $9D
ORA $13D4
ORA $71
BNE .nocammove

LDA $7B
BPL .checkpositive

CMP #$DB
BCS .nocammove

LDA $142A
CMP #$C0
BCS .nocammove

INC $142A
BRA .nocammove

.checkpositive
CMP #$25
BCC .nocammove

LDA $142A
CMP #$30
BCC .nocammove

DEC $142A

.nocammove
	;STZ $86		;why remove slipperiness?

	LDA $81
	BEQ NORETURN

	RTS

NORETURN:
	LDX $13E0

	LDA $19
	BEQ SMALLMARIOHAT

	LDA $0311
	CLC
	ADC BIGPOSTBL,x
	SEC
	SBC #$09
	STA $0209

	BRA BIGMARIOHAT

SMALLMARIOHAT:
	LDA $0311
	CLC
	ADC SMALLPOSTBL,x
	SEC
	SBC #$09
	STA $0209

BIGMARIOHAT:
	LDA $0310
	STA $0208

	LDA #$40
	STA $020A

	LDA $0313
	ORA #$01
	STA $020B

	REP #$20
	LDA $0314
	CLC
	ADC #$0F00
	STA $0200
	LDA #$6152
	STA $0202

	LDA $0314
	CLC
	ADC #$0F08
	STA $0204
	LDA #$6153
	STA $0206
	SEP #$20

	STZ $0420
	STZ $0421
	LDA #$02
	STA $0422

nope:
	RTS

IceSkatingSux:
LDA $9D
ORA $13D4
ORA $71
BNE nope

	LDA $7D
	BPL .IceSkateSpeed

	LDA #$01
	STA $140D	; Set spin-jump flag if jumping in the air

.IceSkateSpeed
LDA $72
BEQ .doaccel

LDA #$01
STA $0F0C	;set custom air flag
BRA .NoXAccel

.doaccel
LDA $0F0C
BEQ +		;if already on ground don't restore mario's direction

LDA $0F0B
STA $76		;when the player lands, mario's direction must be restored to prior to jumping (because spin jump)
STZ $0F0C	;clear custom air flag

+
LDA $76
STA $0F0B	;save player direction for when landing next time

LDA $76
BNE .accright

LDA $15E8	;load X speed mirror
BPL +		;if positive, always accel.
CMP #$C3	;compare
BCC .NoXAccel	;if lower, stop accelerating.
+
DEC $15E8	;decrement speed
DEC $15E8	;twice
BRA .NoXAccel	;branch ahead

.accright
LDA $15E8	;load X speed mirror
BMI +		;if negative, always accel.
CMP #$3D	;compare
BCS .NoXAccel	;if higher, stop accelerating.
+
INC $15E8	;increment speed
INC $15E8	;twice

.NoXAccel
LDA $77		;load player blocked flags
AND #$03	;check if blocked on sides
BEQ .nobounce	;if not, skip ahead.

LDA #$01
STA $1DF9	;bonk sfx

LDA $15E8	;load X speed mirror
EOR #$FF	;flip all bits
INC		;add +1
STA $15E8	;store result back.

.nobounce
LDA $15E8
STA $7B		;finally store the custom speed to the actual player X speed address
RTS

BIGPOSTBL:
	db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08
	db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$0B,$08,$08
	db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08
	db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$15,$0B,$0B,$08
	db $08,$08,$08,$08,$08,$08,$08

SMALLPOSTBL:
	db $10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10
	db $10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$0E,$11,$10,$10
	db $10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10
	db $10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$17,$11,$11,$10
	db $10,$10,$10,$10,$10,$10,$10