;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FOR USE WITH DESERT BOSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!TimesToBounce = $05
			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR SpriteCode
			PLB
			print "INIT ",pc
			RTL
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
			JSR GraphicsRoutine
			
			LDA $14C8,x
			CMP #$08
			BNE .Return
			
			LDA $9D
			BNE .Return

			LDA $1504,x
			CMP #!TimesToBounce
			BCC .DontDestroy
			JSR PuffOfSmoke
			RTS
			
.DontDestroy
			JSL $81802A
			JSL $81A7DC
			BCC .NoContact
			
			LDA $0E
			CMP #$E6
			BPL .Hurt
			
			JSL $81AA33
			LDA #$07
			STA $1DF9
			JSR PuffOfSmoke
			RTS
			
.Hurt
			JSL $80F5B7
			
.NoContact
			LDA $1588,x
			BIT #$04
			BNE .HitGround
			BIT #$03
			BNE .HitWall
			
.Return		
			RTS
			
.HitGround
			LDA $AA,x
			EOR #$FF
			STA $AA,x
			INC $1504,x
			RTS
			
.HitWall
			LDA $B6,x
			EOR #$FF
			STA $B6,x
			INC $1504,x
			RTS
			
PuffOfSmoke:
			LDA #$04
			STA $14C8,x
			LDA #$20
			STA $1540,x
			RTS
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YarnTiles:
			db $08,$0A

GraphicsRoutine:
			LDA $14
			LSR #3
			AND #$01
			STA $02
			
			%GetDrawInfo()
			
			LDA $00
			STA $0300,y
			LDA $01
			STA $0301,y
			
			PHY
			LDY $02
			LDA YarnTiles,y
			PLY
			STA $0302,y
			
			LDA $15F6,x
			STA $0303,y
			
			LDY #$02
			LDA #$00
			JSL $01B7B3
			RTS