;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; FOR USE WITH DESERT BOSS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!SandBlockTile = $28

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR GraphicsRoutine			
			LDA $14C8,x
			CMP #$09
			BCS CarryCode
			JSR SpriteCode
			PLB
			print "INIT ",pc
			RTL
			
CarryCode:
			JSR MarioContact
			PLB
			RTL
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
			LDA $14C8,x
			CMP #$08
			BNE .Return
			
			LDA $9D
			BNE .Return

			JSL $81802A
			JSR MarioContact
			
			LDA $1588,x
			AND #$04
			BEQ .Return

			LDA #$01
			STA $1DF9
			
			LDA $C2,x
			BNE .NotCarryable
			
			LDA #$09
			STA $14C8,x
			BRA .Return
			
.NotCarryable

			LDA #$20
			STA $1540,x
			LDA #$04
			STA $14C8,x
			
.Return		
			RTS
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GraphicsRoutine:
			%GetDrawInfo()
			
			LDA $00
			STA $0300,y
			LDA $01
			STA $0301,y
			
			LDA #!SandBlockTile
			STA $0302,y
			
			LDA $15F6,x
			STA $0303,y
			
			LDA $14C8,x
			CMP #$09
			BCC .NotCarried
			
			LDA #$F0
			STA $0309,y
			
.NotCarried			
			LDY #$02
			LDA #$00
			JSL $81B7B3
			RTS

MarioContact:
			JSL $81A7DC
			BCC .NoContact
			
			LDA $14C8,x
			CMP #$09
			BEQ .Carried
			CMP #$0A
			BEQ .NoContact
			CMP #$0B
			BEQ .NoContact
			
			JSL $80F5B7
			RTS
			
.Carried
			BIT $15
			BVC .NoContact

			LDA $1470
			BNE .NoContact

			LDA #$0B
			STA $14C8,x

.NoContact
			RTS