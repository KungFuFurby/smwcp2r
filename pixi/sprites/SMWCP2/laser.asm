;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Laser
; by yoshicookiezeus
; bd note: interesting sprite
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!RAM_FrameCounterB	= $14
			!RAM_SpritesLocked	= $9D
			!RAM_ExtraBits 		= $7FAB10
			!RAM_ManualAnimFrame	= $7FC074

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "INIT ",pc
			LDA #$1F
			STA $1528,x
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR MainCode
			PLB
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MainCode:		LDA #$01 : %SubOffScreen()
			LDA !RAM_SpritesLocked
			BNE Return
			LDA !RAM_FrameCounterB
			AND #$03
			BNE Return
			LDA $151C,x
			JSL $8086DF

			dw Preparing
			dw Growing
			dw Active
			dw Shrinking

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Preparing:		LDA $1528,x
			AND #$03
			BNE .Decrease
			LDA #$23
			STA $1DFC

.Decrease		DEC $1528,x
			BNE .Return

			LDA #$01
			JSR SetFrame

			LDA #$17
			STA $1DFC

			INC $151C,x
			LDA #$10
			STA $1528,x

.Return
Return:			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Growing:		LDA $1528,x
			CMP #$03
			BCS .Line

			LDA #$03
			JSR SetFrame

.Line			DEC $1528,x
			BNE .Return

			LDA #$05
			JSR SetFrame			

			INC $151C,x
			LDA #$20
			STA $1528,x

.Return			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Active:			DEC $1528,x
			BNE .Return

			INC $151C,x
			LDA #$05
			STA $1528,x

			LDA #$17
			STA $1DFC
			
.Return			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Shrinking:		LDA $1528,x
			DEC A
			JSR SetFrame

			DEC $1528,x
			BNE .Return

			STZ $14C8,x

.Return			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SetFrame:		PHX
			PHA
			LDA !RAM_ExtraBits,x
			AND #$04
			LSR
			LSR
			TAX
			PLA
			STA !RAM_ManualAnimFrame,x
			PLX
			RTS