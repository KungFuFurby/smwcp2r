
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SMW Hammer Bro
; Disassembled by Sonikku
; Description: Like the original Hammer Bro, but it 
; doesn't rely on the gray flying blocks. You can place 
; this one anywhere, and he will stay stationary, throwing
; hammers at Mario.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; init and main jsl targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc
                    PHB
                    PHK
                    PLB
                    JSR HAMMERBRO
                    PLB

                    PRINT "INIT ",pc
                    RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; hammer bro sprite routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
HAMMERFREQ:	db $1F,$1F,$1F,$1F,$1F,$1F,$1F

HAMMERBRO:	JSR HAMMERBROGFX
	LDA $9D
	BNE RETURN
	LDA #$08
	CMP $14C8,x
	BNE DEAD
	JSL $81803A
	LDA #$02
	%SubOffScreen()
	LDY $0DB3
	LDA $1F11,y
	TAY
	LDA $14
	AND HAMMERFREQ,y
	BEQ CODE_02DA89
	INC $1570,x
CODE_02DA89:	LDA $1570,x
	ASL
	CPY #$00
	BEQ CODE_02DA92
	ASL
CODE_02DA92:	AND #$40
	;STA $157C,x
	LDA $1570,x
	AND HAMMERFREQ,y
	ORA $15A0,x
	ORA $186C,x
	ORA $1540,x
	BNE RETURN
	LDA #$03
	STA $1540,x
	LDY #$10
	LDA $157C,x
	BNE CODE_02DAB6
	LDY #$F0
CODE_02DAB6: STY $00
	LDY #$07
CODE_02DABA: LDA $170B,y
	BEQ GENERATEHAMMER
	DEY
	BPL CODE_02DABA
RETURN:	RTS
DEAD:	STZ $157C,x
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; generate hammers routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GENERATEHAMMER:
	LDA $157C,x
	EOR #$41
	STA $157C,x
	LDA #$04
	STA $170B,y
	LDA $E4,x
	STA $171F,y
	LDA $14E0,x
	STA $1733,y
	LDA $D8,x
	STA $1715,y
	LDA $14D4,x
	STA $1729,y
	LDA #$D0
	STA $173D,y
	LDA $00
	STA $1747,y
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; hammer bro graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
HAMMERBRODISPX:	db $08,$10,$00,$10

HAMMERBRODISPY:	db $F8,$F8,$00,$00

HAMMERBROTILES:	db $5A,$4A,$46,$48,$4A,$5A,$48,$46
HAMMERBROTILESIZE:	db $00,$00,$02,$02

HAMMERBROGFX:	%GetDrawInfo()
	LDA $157C,x
	EOR #$41
	STA $02
	PHX
	LDX #$03
CODE_02DB08:	LDA $00
	CLC
	ADC HAMMERBRODISPX,x
	STA $0300,y
	LDA $01
	CLC
	ADC HAMMERBRODISPY,x
	STA $0301,y
	PHX
	LDA $02
	PHA
	ORA #$37
	STA $0303,y
	PLA
	BEQ CODE_02DB2A
	INX
	INX
	INX
	INX
CODE_02DB2A: LDA HAMMERBROTILES,x
	STA $0302,y
	PLX
	PHY
	TYA
	LSR
	LSR
	TAY
	LDA HAMMERBROTILESIZE,x
	STA $0460,y
	PLY
	INY
	INY
	INY
	INY
	DEX
	BPL CODE_02DB08
	PLX
	LDY #$FF
	LDA #$03
	JSL $81B7B3
	RTS