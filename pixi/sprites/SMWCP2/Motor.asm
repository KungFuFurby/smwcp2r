;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!FrameNum = $C2
!Direction = $157C

                    print "INIT ",pc
					STZ !FrameNum,x
					LDA #$01
					STA !Direction,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $00,$18,$E8,$18		;No left + right glitch for you
YSpeeds:
db $00,$18,$E8,$18
Frames:
db $17,$19,$1B,$1B
Carrying:
					LDA $13
					LSR #3
					AND #$03
					PHX
					TAX
					LDA Frames,x
					STA $13E0
					PLX
					;LDA #$19
					;STA $13E0
					LDA $190F,x
					AND #$7F
					STA $190F,x
					JSR SUB_GFX					;Draw the graphics
					STZ $7D
					STZ $7B
					REP #$20
					STZ $00
					STZ $02
					SEP #$20
					PHX
					LDA $15
					AND #$03
					TAX
					LDA XSpeeds,x
					STA $00
					LDA $15
					AND #$0C
					LSR #2
					TAX
					LDA YSpeeds,x
					STA $02
					PLX
					LDA $00
					BEQ .ZeroX
					BPL .PosX
					LDA $B6,x
					BPL +
					LDA $00
					CMP $B6,x
					BCS ++
					+
					DEC $B6,x
					++
					BRA .YSpeed
					.PosX
					LDA $B6,x
					BMI +
					LDA $00
					CMP $B6,x
					BEQ ++
					BCC ++
					+
					INC $B6,x
					++					
					BRA .YSpeed
					.ZeroX
					LDA $B6,x
					BEQ .YSpeed
					BPL +
					INC $B6,x
					BRA .YSpeed
					+
					DEC $B6,x
					.YSpeed
					
					LDA $02
					BEQ .ZeroY
					BPL .PosY
					LDA $AA,x
					BPL +
					LDA $02
					CMP $AA,x
					BCS ++
					+
					DEC $AA,x
					++
					BRA .Done
					.PosY
					LDA $AA,x
					BMI +
					LDA $02
					CMP $AA,x
					BEQ ++
					BCC ++
					+
					INC $AA,x
					++					
					BRA .Done
					.ZeroY
					LDA $AA,x
					BEQ .Done
					BPL +
					INC $AA,x
					BRA .Done
					+
					DEC $AA,x
					.Done
					JSL $819138
				
					
					LDA $77
					BIT #$03
					BEQ ++
					BIT #$01
					BNE +
					LDA $B6,x
					BPL ++
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					BMI ++
					STZ $B6,x
					++
					
					LDA $77
					BIT #$0C
					BEQ ++
					BIT #$08
					BNE +
					LDA $AA,x
					BMI ++
					STZ $AA,x
					BRA ++
					+
					LDA $AA,x
					BPL ++
					STZ $AA,x
					++
					
					LDA $13E1
					BEQ ++
					BIT #$08
					BEQ +
					LDA $B6,x
					BMI ++
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					BPL ++
					STZ $B6,x
					++
					
					JSL $81801A
					JSL $818022
					
					LDA $14E0,x
					XBA
					LDA $E4,x
					REP #$20
					STA $00
					SEP #$20
					LDA $1499
					BNE .Y
					LDA $76
					BNE .Right
					REP #$20
					LDA $00
					CLC
					ADC #$000B
					BRA .Store
					.Right
					REP #$20
					LDA $00
					SEC
					SBC #$000B
					.Store
					STA $94
					SEP #$20
					.Y
					LDA $19
					BEQ +
					LDA $14D4,x
					XBA
					LDA $D8,x
					REP #$20
					SEC
					SBC #$000D
					STA $96
					SEP #$20
					BRA ++
					+
					LDA $14D4,x
					XBA
					LDA $D8,x
					REP #$20
					SEC
					SBC #$000F
					STA $96
					SEP #$20
					++
					LDA $76
					STA !Direction,x
					RTS
Kicked:
					LDA #$08
					STA $14C8,x
					JMP return
					
JumpCarry:
					JMP Carrying
SPRITE_ROUTINE:	  	
					LDA $9D
					BNE return
					LDA $14C8,x
					CMP #$0B
					BEQ JumpCarry
					CMP #$09
					BEQ Kicked
					CMP #$08
					BNE return
					STZ $B6,x
					STZ $AA,x
					LDA $190F,x
					ORA #$80
					STA $190F,x
					JSL $019138
					JSL $01803A
					BCC +
					LDA $15
					BIT #$40
					BEQ +
					LDA $1470
                    ORA $187A
					BNE +
					LDA #$0B
					STA $14C8,x
					INC $1470
					+
					return:
					LDA #$03
					%SubOffScreen()
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TileSize:
db $02,$00,$00

TilePointers:
dw Tiles1
dw Tiles2

XOff:
db $04,$FC,$FC

YOff:
db $00,$00,$08

Tiles1:
db $49,$4B,$5B
Tiles2:
db $49,$48,$58

SUB_GFX:            %GetDrawInfo()     	;Get all the drawing info, duh
					LDA !Direction,x
					STA $07
					PHX
					LDA $14
					LSR #2
					AND #$01
					ASL
					TAX
					REP #$20
					LDA TilePointers,x
					STA $05
					SEP #$20
					LDX #$02
					.GFXLoop
					
					LDA $07
					BEQ +
					LDA $00
					CLC
					ADC XOff,x
					BRA ++
					+
					LDA $00
					SEC
					SBC XOff,x
					CPX #$01
					BCC ++
					CLC
					ADC #$08
					++
					STA $0300,y
					
					LDA $01
					CLC
					ADC YOff,x
					STA $0301,y
					
					PHY
					TXY
					LDA ($05),y
					PLY
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					PHX
					LDX $07
					BNE +
					ORA #$40
					+
					PLX
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA #$02
					LDY #$FF
					JSL $81B7B3					;/and then draw em
					EndIt:
					RTS