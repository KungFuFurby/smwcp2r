;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Score, by cstutor89
;; Displays the score (points you have obtained from minigames)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TILEMAP: db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5,$4D,$67

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA $7F9A82
BEQ Init
CLC
ADC $7F9A81
STA $7F9A81
LDA #$00
STA $7F9A82
Init:	RTL      

print "MAIN ",pc
Main:	PHB
	PHK
	PLB		
	JSR Run
	PLB
	RTL      				;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Run:	LDA $7FAB10,x
 	AND #$04
	BNE SKIP
 	JSR GFX
	RTS

SKIP:	JSR GFXTAR
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the timer
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!GamesBits = $7F9A83
!BP = $7F9A81

!YPosBP = $C0 
!YPosGames = !YPosBP+$10

Nums8x8:
db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5	;0-9

Tilemap:
db $EC,$EE		;BP:
db $D8,$DA,$DC,$DE	;GAMES:/5

XPos:
db $08,$18		;BP:
db $08,$18,$28,$48	;GAMES:/5

YPos:
db !YPosBP,!YPosBP
db !YPosGames,!YPosGames,!YPosGames,!YPosGames

CxBPXPos:
db $28,$30		;X-pos for amount of BP

!CxGamesXPos = $40	;X-pos for amount of games played

GFX:
LDA #$00
%SubOffScreen()

JSR Drawlolo

LDA $1462
CLC
ADC #$80
STA !E4,x
LDA $1463
ADC #$00
STA !14E0,x

LDA $1464
CLC
ADC #$80
STA !D8,x
LDA $1465
ADC #$00
STA !14D4,x

	LDA !BP
	LDY #$00
LoopX:
	CMP #$0A ; <- If A != 10 ..
	BCC Return
	SBC #$0A ; A - 10.
	INY	; Y + 1.
	BRA LoopX
Return:
	STA !1528,x
	TYA
	STA !1510,x

	PHX
	LDX #$04
	LDY #$00
-	LDA $7F9A83
	AND DoorsWhich,x
	BEQ +
	INY
+	DEX
	BPL -

	TYA
	PLX
	STA !C2,x
	CMP #$03
	BCC ThreeGames
	BEQ ThreeGames
	CMP #$04
	BEQ FourGames

FiveGames:
	LDA $7F9A81
	CMP #$0B
	BCC DoorToHeaven	;survived all 5 games? go on ahead
	BRA NoGo

FourGames:
	LDA $7F9A81
	CMP #$06		;lost 5 or less points after 4 games? go ahead
	BCC DoorToHeaven
	BRA NoGo

ThreeGames:
	LDA $7F9A81
	BEQ DoorToHeaven	;no loss after 3 games? good to go

NoGo:
LDA #$3E
BRA +

DoorToHeaven:
LDA #$3C

+
STA !1FD6,x
RTS

Drawlolo:
%GetDrawInfo()

STZ $00			;reset number of tiles drawn

LDA !1FD6,x		;load properties according to bp and games
STA $01			;store to scratch RAM.

LDA !1510,x
STA $02			;ones
LDA !1528,x
STA $03			;tens

LDA !C2,x
STA $05			;games played

PHX

LDX #$05

.loop
LDA XPos,x
STA $0300,y
LDA YPos,x
STA $0301,y
LDA Tilemap,x
STA $0302,y
LDA #$3D
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$02
STA $0460,y
PLY
INY #4
INC $00
DEX
BPL .loop

LDX #$01

.bploop
LDA CxBPXPos,x
STA $0300,y
LDA #!YPosBP+8
STA $0301,y

PHX
LDA $02,x
TAX
LDA Nums8x8,x
STA $0302,y
PLX
LDA $01
ORA $64
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$00
STA $0460,y
PLY
INY #4
INC $00
DEX
BPL .bploop

LDA #!CxGamesXPos
STA $0300,y
LDA #!YPosGames
STA $0301,y
PHX
LDX $05
LDA Nums8x8,x
STA $0302,y
PLX
LDA #$3C
STA $0303,y

PHY
TYA
LSR #2
TAY
LDA #$00
STA $0460,y
PLY
PLX

LDY #$FF
LDA $00
JSL $81B7B3
RTS

DoorsWhich:	db $01,$02,$04,$08,$10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the timer
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GFXTAR:	
tya
sta $08

	STZ $0F00	;NO STATUS BAR kthxbai

	LDA #$C0
	STA $0200,y	;xpos TARGET
	CLC
	ADC #$0E
	STA $0204,y	;xpos NUM1 TARGET
	CLC
	ADC #$08
	STA $0208,y	;xpos NUM2 TARGET
	CLC
	ADC #$08
	STA $020C,y	;xpos '/'
	CLC
	ADC #$08
	STA $0210,y	;xpos '2'
	CLC
	ADC #$08
	STA $0214,y	;xpos '0'
	LDA #$C0
	STA $0218,y	;xpos BULLET
	CLC
	ADC #$0E
	STA $021C,y	;xpos NUM1 BULLET
	CLC
	ADC #$08
	STA $0220,y	;xpos NUM2 BULLET
	CLC
	ADC #$08
	STA $0224,y	;xpos '/'
	CLC
	ADC #$08
	STA $0228,y	;xpos '2'
	CLC
	ADC #$08
	STA $022C,y	;xpos '5'

	LDA #$28
	STA $0201,y	;ypos TARGET
	STA $0205,y	;ypos NUM1 TARGET
	STA $0209,y	;ypos NUM2 TARGET
	STA $020D,y	;ypos '/'
	STA $0211,y	;ypos '2'
	STA $0215,y	;ypos '0'
	LDA #$30
	STA $0219,y	;ypos BULLET
	STA $021D,y	;ypos NUM1 BULLET
	STA $0221,y	;ypos NUM2 BULLET
	STA $0225,y	;ypos '/'
	STA $0229,y	;ypos '2'
	STA $022D,y	;ypos '5'

	LDA #$CA
	STA $0202,y
	LDA #$CB
	STA $020E,y
	LDA #$46
	STA $0212,y
	LDA #$76
	STA $0216,y
	LDA #$DA
	STA $021A,y
	LDA #$CB
	STA $0226,y
	LDA #$46
	STA $022A,y
	LDA #$C3
	STA $022E,y

	PHX
	LDA $7F9A90
LOOP3:	TAX
	CMP #$0A
	BCC E_LOOP3
	SEC
	SBC #$0A
	BRA LOOP3
E_LOOP3: LDA TILEMAP,x
	STA $020A,y
	LDX #$00
	LDA $7F9A90
LOOP4:	CMP #$0A
	BCC E_LP4
	SEC
	SBC #$0A
	INX
	BRA LOOP4
E_LP4:	LDA TILEMAP,x
	STA $0206,y
	PLX
	PHX
	LDA $7F9A70
LOOP5:	TAX
	CMP #$0A
	BCC E_LOOP5
	SEC
	SBC #$0A
	BRA LOOP5
E_LOOP5: LDA TILEMAP,x
	STA $0222,y
	LDX #$00
	LDA $7F9A70
LOOP6:	CMP #$0A
	BCC E_LP6
	SEC
	SBC #$0A
	INX
	BRA LOOP6
E_LP6:	LDA TILEMAP,x
	STA $021E,y
	PLX

	LDA #$38
	STA $0203,y	;prop
	STA $0207,y	;prop
	STA $020B,y	;prop
	STA $020F,y	;prop
	STA $0213,y	;prop
	STA $0217,y	;prop
	LDA #$33
	STA $021B,y	;prop
	LDA #$38
	STA $021F,y	;prop
	STA $0223,y	;prop
	STA $0227,y	;prop
	STA $022B,y	;prop
	STA $022F,y	;prop

	ldy #$0b
-
	phy
	tya
	clc
	adc $08
	tay
	lda #$00
	sta $0420,y
	ply
	dey
	bpl -

	LDX $15E9	;restore sprite index
	LDY #$00	;8X16
	LDA #$00	;2 tile
	JSL $81B7B3	;reserve
	RTS