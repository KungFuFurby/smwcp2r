;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Bute's Arrow
; Description: The arrow the Archer Bute shoots. It flies through the air until
; it hits something solid, then it stops and disappears.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	            PRINT "MAIN ",pc			
                    PHB
                    PHK				
                    PLB				
                    JSR SPRITE_ROUTINE	
                    PLB
                    PRINT "INIT ",pc
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
XSPD:	db $20,$E0
SPRITE_ROUTINE:	JSR SUB_GFX
	LDA #$00
	%SubOffScreen()
	LDA $9D
	BNE RETURN
	LDA !C2,x
	BNE STOPPED
	LDA !1588,x
	BNE NOSPD
	JSL $01A7DC|!BankB
	JSL $01802A|!BankB
	LDA !1540,x
	CMP #$20
	BCS SETSPD
SETSPD:	LDY !157C,x
	LDA XSPD,y
	STA !B6,x
	LDA !1540,x
	BEQ SETFRM
	LDA !1594,x
	STA !AA,x
SETFRM:	LDA !AA,x
	CMP #$08
	BCC STRT
	CMP #$10
	BCC DWN2
	CMP #$80
	BMI DWN1
	CMP #$F8
	BCS STRT
	CMP #$E1
	BCS UP
	STZ !1602,x
	RTS
DWN1:	DEC !AA,x
	DEC !AA,x
	LDA #$04
	BRA STORE
DWN2:	DEC !AA,x
	DEC !AA,x
	LDA #$03
	BRA STORE
STRT:	LDA #$02
STORE:	STA !1602,x
RETURN:	RTS
UP:	LDA #$01
	BRA STORE
NOSPD:	INC !C2,x
	STZ !AA,x
	STZ !B6,x
	RTS
STOPPED:	INC !C2,x
	LDA #$20
	CMP !C2,x
	BCS RETURN
	STZ !14C8,x
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:	db $60,$62,$64,$66,$68
SUB_GFX:	    %GetDrawInfo()     ; sets y = OAM offset
		    LDA $1602,x
		    STA $03		 ;  | $03 = index to frame start (0 or 1)
		    LDA $157C,x             ; $02 = sprite direction
		    STA $02
		    PHX		     ; /
                    LDA $00		 ; \ tile x position = sprite x location ($00)
		    STA $0300|!Base2,y	     ; /

		    LDA $01		 ; \ tile y position = sprite y location ($01)
                    CLC
                    ADC #$03
		    STA $0301|!Base2,y	     ; /

		    LDA $15F6,x	     ; tile properties xyppccct, format
		    LDX $02		 ; \ if direction == 0...
		    BNE NO_FLIP	     ;  |
		    ORA #$40		; /    ...flip tile
NO_FLIP:             ORA $64		 ; add in tile priority of level
		    STA $0303|!Base2,y	     ; STORE tile properties

		    LDX $03		 ; \ STORE tile
		    LDA TILEMAP,x	   ;  |
		    STA $0302|!Base2,y	     ; /

		    INY		     ; \ increase index to sprite tile map ($300)...
		    INY		     ;  |    ...we wrote 1 16x16 tile...
		    INY		     ;  |    ...sprite OAM is 8x8...
		    INY		     ; /    ...so increment 4 times

		    PLX		     ; pull, X = sprite index
		    LDY #$02		; \ 460 = 2 (all 16x16 tiles)
		    LDA #$00		;  | A = (number of tiles drawn - 1)
		    JSL $01B7B3|!BankB	     ; / don't draw if offscreen
		    RTS		     ; RETURN