;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite BE - Swooper
; adapted for Romi's Spritetool and commented by yoshicookiezeus
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!RAM_FrameCounter	= $13
			!RAM_FrameCounterB	= $14
			!RAM_MarioSpeedX	= $7B
			!RAM_MarioXPos		= $94
			!RAM_MarioXPosHi	= $95
			!RAM_SpritesLocked	= $9D
			!RAM_SpriteNum		= !9E
			!RAM_SpriteSpeedY	= !AA
			!RAM_SpriteSpeedX	= !B6
			!RAM_SpriteState	= !C2
			!RAM_SpriteYLo		= !D8
			!RAM_SpriteXLo		= !E4
			!OAM_DispX		= $0300|!Base2
			!OAM_DispY		= $0301|!Base2
			!OAM_Tile		= $0302|!Base2
			!OAM_Prop		= $0303|!Base2
			!OAM_TileSize		= $0460|!Base2
			!RAM_SpriteYHi		= !14D4
			!RAM_SpriteXHi		= !14E0
			!RAM_SpriteDir		= !157C
			!RAM_SprObjStatus	= !1588
			!RAM_OffscreenHorz	= !15A0
			!RAM_SprOAMIndex	= !15EA
			!RAM_SpritePal		= !15F6
			!RAM_OffscreenVert	= !186C

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR Sprite_Code_Start
			PLB
			print "INIT ",pc
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BatTiles:		db $AE,$C0,$E8

Sprite_Code_Start:	JSR Sprite_Graphics
			LDA !14C8,x			;\ if sprite status normal,
			CMP #$08			; |
			BEQ CODE_0388C0			;/ go to main code
			RTS

CODE_0388C0:
			LDA !RAM_SpritesLocked		;\ if sprites locked,
			BNE Return0388DF		;/ return
			LDA #$00
			%SubOffScreen()
			JSL $01803A|!BankB		; interact with Mario and with other sprites
			JSL $018022|!BankB		;\ update sprite position
			JSL $01801A|!BankB		;/
			LDA !RAM_SpriteState,x		;\ use sprite state to determine which code to go to
			JSL $8086DF|!BankB

SwooperPtrs:		dw CODE_0388E4			; waiting
			dw CODE_038905			; swooping down
			dw CODE_038936			; flying horizontally

Return0388DF:
			RTS				; return


Max_X_Speed:		db $10,$F0
Max_Y_Speed:		db $04,$FC
X_Acceleration:		db $01,$FF
Y_Acceleration:		db $01,$FF

CODE_0388E4:
			LDA !RAM_OffscreenHorz,x	;\ if sprite offscreen horizontally,
			BNE Return038904		;/ return
			%SubHorzPos()
			LDA $0E				;\ if Mario more than 0x50 pixels (5 16x16 tiles) from sprite,
			CLC				; |
			ADC #$50			; |
			CMP #$A0			; |
			BCS Return038904		;/ return
			INC !RAM_SpriteState,x		; sprite state = swooping down
			TYA				;\ make sprite face Mario
			STA !RAM_SpriteDir,x		;/

			LDA #$E0			;\ set initial y speed
			STA !RAM_SpriteSpeedY,x		;/

			LDA #$26			;\ play sound effect 
			STA $1DFC|!Base2		;/
Return038904:
			RTS				; return 

CODE_038905:
			LDA !RAM_FrameCounter		;\ three out of four frames,
			AND #$03			; |
			BNE CODE_038915			;/ branch
			LDA !RAM_SpriteSpeedY,x		;\ if sprite y speed == 0,
			BEQ CODE_038915			;/ branch
			INC !RAM_SpriteSpeedY,x
			BNE CODE_038915			; if new sprite y speed =/= 0, branch
			INC !RAM_SpriteState,x		; else, sprite state = flying horizontally
CODE_038915:
			LDA !RAM_FrameCounter		;\ three out of four frames,
			AND #$03			; |
			BNE CODE_03892B			;/ branch
			LDY !RAM_SpriteDir,x		;\ set sprite x speed according to direction
			LDA !RAM_SpriteSpeedX,x		;/
			CMP Max_X_Speed,y		;\ if max x speed achieved,
			BEQ CODE_03892B			;/ skip accelerating sprite
			CLC 				;\ else,
			ADC X_Acceleration,y		; |
			STA !RAM_SpriteSpeedX,x		;/ accelerate sprite
CODE_03892B:
			LDA !RAM_FrameCounterB		;\ use frame counter to determine graphics frame to use
			AND #$04			; |
			LSR				; |
			LSR				; |
			INC A				; |
			STA !1602,x			;/
			RTS				; return

CODE_038936:
			LDA !RAM_FrameCounter		;\ if number of current frame is odd,
			AND #$01			; |
			BNE CODE_038952			;/ branch
			LDA !151C,X			;\ use sprite vertical direction to determine vertical acceleration
			AND #$01			; |
			TAY				; |
			LDA !RAM_SpriteSpeedY,x		; |
			CLC				; |
			ADC Y_Acceleration,y		; |
			STA !RAM_SpriteSpeedY,x		;/
			CMP Max_Y_Speed,y		;\ if max y speed in current direction not achieved,
			BNE CODE_038952			;/ branch
			INC !151C,X			; else, swap sprite vertical direction
CODE_038952:
			BRA CODE_038915			; reuse normal movement routine from the previous sprite state


Sprite_Graphics:
			%GetDrawInfo()

			PHY
			LDY !1602,x
			LDA BatTiles,y
			PLY
			STA !OAM_Tile,y
			LDA $00
			STA !OAM_DispX,y
			LDA $01
			STA !OAM_DispY,Y
			LDA $15F6,x

			PHY
			LDY !RAM_SpriteDir,X
			BNE No_X_Flip
			EOR #$40
No_X_Flip:

			LDY !C2,x
			BEQ YFLIP
			LDY !14C8,x
			CPY #$08
			BEQ No_Y_Flip	
YFLIP:		
			ORA #$80
No_Y_Flip:
			PLY
			ORA $64
			STA !OAM_Prop,Y

			LDY #$02		; \ 460 = 2 (all 16x16 tiles)
			LDA #$00		;  | A = (number of tiles drawn - 1)
			JSL $01B7B3|!BankB	; / don't draw if offscreen
			RTS			; return