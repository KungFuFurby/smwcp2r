;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!Direction = $157C

                    print "INIT ",pc
			%SubHorzPos()
			TYA
					STA !Direction,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $20,$E0

SPRITE_ROUTINE:	  	
					LDA $9D
					BNE return
					LDA $14C8,x
					CMP #$08
					BNE return

					LDA $1588,x
					AND #$04
					BEQ +
					STZ !AA,x
+
					LDA !15AC,x
					BNE .nochangedir
					%SubHorzPos()
					TYA
					STA !Direction,x
					LDA #$08
					STA !15AC,x
.nochangedir
					PHX
					LDA !Direction,x
					TAX
					BNE .left
					LDA XSpeeds,x
					STA $00
					PLX
					LDA !B6,x
					BMI +
					CMP $00
					BCS .right
					+
					INC $B6,x
					BRA .right
					.left
					LDA XSpeeds,x
					STA $00
					PLX
					LDA !B6,x
					BPL +
					CMP $00
					BCC .right
					+
					DEC $B6,x
					.right
					JSL $01802A
					JSL $01803A
					LDA $1588,x
					BIT #$01
					BEQ +
					LDA #$E0
					STA $B6,x
					+
					LDA $1588,x
					BIT #$02
					BEQ +
					LDA #$20
					STA $B6,x
					+
					return:
					LDA #$00
					%SubOffScreen()
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Tiles:
db $85,$87

SUB_GFX:            %GetDrawInfo()     	;Get all the drawing info, duh
					STZ $08
					LDA !Direction,x
					BNE +
					LDA #$40
					TSB $08
					+
					PHX
					LDA $14
					LSR #2
					AND #$01
					TAX
					LDA $00
					STA $0300,y
					
					LDA $01
					STA $0301,y
					
					LDA Tiles,x
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					ORA $08
					STA $0303,y
					PLX
					LDA #$00
					LDY #$02
					JSL $81B7B3					;/and then draw em
					EndIt:
					RTS