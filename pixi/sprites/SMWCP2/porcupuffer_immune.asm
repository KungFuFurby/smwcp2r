;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Sumo brother disassembly
; By nekoh
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    LDY #$00                
                    LDA $D1                   
                    SEC                       
                    SBC !E4,x       
                    STA $0F                   
                    LDA $D2                   
                    SBC.w !14E0,X     
                    BPL RETURN01AD41          
                    INY                             
                    TYA                       
                    STA !157C,x     
RETURN01AD41:       RTL                       


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                           
                    PRINT "MAIN ",pc
                    PHB                       
                    PHK                       
                    PLB                             
                    JSR PORCUPUFFER         
                    PLB                       
                    RTL                       ; Return 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PORCUPUFFACCEL:     db $01,$FF
PORCUPUFFMAXSPEED:  db $10,$F0

PORCUPUFFER:        JSR POCRUPUFFERGFX         
                    LDA $9D     
                    BNE RETURN038586          
                    LDA !14C8,X             
                    CMP #$08                
                    BNE RETURN038586          
		LDA #$03
		%SubOffScreen()
                    JSL $01803A|!BankB
                    %SubHorzPos()     
                    TYA                       
                    STA !157C,x     
                    LDA $14     
                    AND #$03                
                    BNE CODE_03855E           
                    LDA !B6,x    ; \ Branch if at max speed 
                    CMP PORCUPUFFMAXSPEED,Y ;  | 
                    BEQ CODE_03855E           ; / 
                    CLC                       ; \ Otherwise, accelerate 
                    ADC PORCUPUFFACCEL,Y    ;  | 
                    STA !B6,x    ; / 
CODE_03855E:        LDA !B6,x    
                    PHA                       
                    LDA $17BD|!Base2
                    ASL                       
                    ASL                       
                    ASL                       
                    CLC                       
                    ADC !B6,x    
                    STA !B6,x    
                    JSL $018022|!BankB
                    PLA                       
                    STA !B6,x    
                    JSL $019138|!BankB
                    LDY #$04                
                    LDA.w !164A,X             
                    BEQ CODE_038580           
                    LDY #$FC                
CODE_038580:        STY !AA,X    
                    JSL $01801A|!BankB
RETURN038586:       RTS                       ; Return 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; PORCUPUFFER graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

POCRUPUFFERDISPX:   db $F8,$08,$F8,$08,$08,$F8,$08,$F8
POCRUPUFFERDISPY:   db $F8,$F8,$08,$08
POCRUPUFFERTILES:   db $86,$C0,$A6,$C2,$86,$C0,$A6,$8A
POCRUPUFFERGFXPROP: db $0D,$0D,$0D,$0D,$4D,$4D,$4D,$4D

POCRUPUFFERGFX:     %GetDrawInfo()
                    LDA $14     
                    AND #$04                
                    STA $03                   
                    LDA !157C,x     
                    STA $02                   
                    PHX                       
                    LDX #$03                
CODE_0385B4:        LDA $01                   
                    CLC                       
                    ADC POCRUPUFFERDISPY,X  
                    STA $0301|!Base2,Y         
                    PHX                       
                    LDA $02                   
                    BNE CODE_0385C6           
                    TXA                       
                    ORA #$04                
                    TAX                       
CODE_0385C6:        LDA $00                   
                    CLC                       
                    ADC POCRUPUFFERDISPX,X  
                    STA $0300|!Base2,Y         
                    LDA POCRUPUFFERGFXPROP,X 
                    ORA $64                   
                    STA $0303|!Base2,Y          
                    PLA                       
                    PHA                       
                    ORA $03                   
                    TAX                       
                    LDA POCRUPUFFERTILES,X  
                    STA $0302|!Base2,Y          
                    PLX                       
                    INY                       
                    INY                       
                    INY                       
                    INY                       
                    DEX                       
                    BPL CODE_0385B4           
                    PLX                       
                    LDY #$02                
                    LDA #$03                
                    JSL $01B7B3|!BankB      
                    RTS                       ; Return