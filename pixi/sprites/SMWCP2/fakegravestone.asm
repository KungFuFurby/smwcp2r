;Fake Gravestone, by Blind Devil

;shitty fucking bird fuck you

Tilemap:
db $CC,$EC	;idle (top, bottom)
db $CA,$EA	;moving (top, bottom)

YDisp:
db $EE,$FE	;top tile, bottom tile

Properties:
db $40,$00	;facing right, facing left

!Proximity = $60	;trigger the sprite when getting near by this amount of pixels
!Proximity2 = $04	;make the sprite fall straight down when near by this amount of pixels

!ShakeTime = $28	;amount of frames to shake before walking
!MoveTime = $80		;amount of frames to follow mario

XSpeed:
db $04,$FC		;going right, going left

print "INIT ",pc
JSR GetDir		;call SubHorzPos from label
TYA			;transfer Y to A
STA !157C,x		;store to sprite direction.
STZ !1510,x		;reset leap index.
RTL			;return.

print "MAIN ",pc
PHB			;preserve data bank
PHK			;push program bank
PLB			;pull it as new data bank
JSR SpriteCode		;call sprite code
PLB			;restore original data bank
RTL			;return.

SpriteCode:
JSR Graphics		;call graphics drawing code

LDA !14C8,x		;load sprite status
CMP #$08		;check if default/alive
BNE Return		;if not equal, return.

LDA $9D			;load sprites/animations locked flag
BNE Return		;if set, return.

LDA #$00		;load value
%SubOffScreen()		;call offscreen handling routine

JSL $01A7DC|!BankB	;call player/sprite interaction routine (use default)

LDA !1588,x		;load sprite blocked status flags
AND #$08		;check if blocked above
BEQ +			;if not blocked, skip ahead.

STZ !AA,x		;reset sprite's Y speed.

+
LDA !C2,x		;load sprite state pointer
CMP #$06		;compare to value
BCS idleatzero		;if higher, force idle status.
JSL $0086DF|!BankB	;call 2-byte pointer subroutine
dw idle			;$00 - idle (wait for player to approach)
dw shake		;$01 - shake (warn the player)
dw move			;$02 - walk towards the player for a while, at zero timer, aim at their head and jump
dw jumping		;$03 - sprite is jumping - if getting above the player's head, pause. if touching the ground (jump failed), go idle again.
dw pause		;$04 - stand still in midair
dw fall			;$05 - fall straight down, stun the player, and the sprite goes back to idle
dw idleatzero		;$06 - idle (code failsafe)

idleatzero:
STZ !C2,x		;reset sprite state value.
RTS			;return.

idle:
STZ !1510,x		;reset leap index.

LDA !15AC,x		;load timer
BNE Return		;if not zero, return.

STZ !B6,x		;reset X speed.
JSR updpos		;update sprite positions and stuff

LDA !15AC,x		;load timer
BNE Return		;if not zero, stay idle regardless of proximity.

JSR ProxCalc		;calculate proximity

XBA			;flip high/low bytes of A
BNE Return		;if over a screen away, keep the sprite idle.
XBA			;flip high/low bytes of A
CMP #$7F		;compare to value
BCS Return		;if over $7F pixels away, keep the sprite idle.

CMP #!Proximity		;compare to value
BCS Return		;if higher, don't change state.

INC !C2,x		;increment sprite state by one
LDA #!ShakeTime		;load amount of frames to shake
STA !15AC,x		;store to timer.

Return:
RTS			;return.

shake:
JSR GetDir		;call SubHorzPos from label
TYA			;transfer Y to A
STA !157C,x		;store to sprite direction.

LDA !15AC,x		;load timer
AND #$01		;preserve bit 0 only
BEQ +			;if not set, skip ahead.

LDA #$01		;load SFX value
STA $1DF9|!Base2	;store to address to play it.

+
LDA !15AC,x		;load timer again
BNE Return		;if not equal zero, return.

INC !C2,x		;increment sprite state by one
LDA #!MoveTime		;load amount of frames
STA !15AC,x		;store to timer.
RTS			;return.

move:
LDA !163E,x		;load turning timer
BNE +			;if not zero, skip ahead.
JSR GetDir		;call SubHorzPos from label
TYA			;transfer Y to A
CMP !157C,x		;compare to sprite direction value
BEQ +			;if equal, no need to reset the turn timer.
STA !157C,x		;store to sprite direction.
LDA #$08		;load value
STA !163E,x		;store to turning timer.

+
LDY !157C,x		;load sprite direction
LDA XSpeed,y		;load speed from table according to index
STA !B6,x		;store to sprite's X speed.

LDA !15AC,x		;load sprite timer
BEQ .endstate		;if zero, jump and change state.
CMP #$14		;compare to value
BCS updpos		;if higher, just keep moving

STZ !B6,x		;reset sprite's X speed.
LDA #$02		;load value
STA !1510,x		;store to leap index.
BRA updpos		;update sprite positions

.endstate
LDA !14E0,x		;load sprite's X-pos high byte
XBA			;flip high/low bytes of A
LDA !E4,x		;load sprite's X-pos low byte
REP #$20		;16-bit A
SEC			;set carry
SBC $94			;subtract player's X-pos within the level from it
STA $00			;store to scratch RAM.
SEP #$20		;8-bit A

LDA !14D4,x		;load sprite's Y-pos high byte
XBA			;flip high/low bytes of A
LDA !D8,x		;load sprite's Y-pos low byte
REP #$20		;16-bit A
SEC			;set carry
SBC $96			;subtract player's Y-pos within the level from it
CLC			;clear carry
ADC #$00A0		;add value, so sprite aims at above the player's head
STA $02			;store to scratch RAM.
SEP #$20		;8-bit A

LDA #$40		;load speed value
%Aiming()		;call aiming routine
LDA $00			;load X speed from scratch RAM
STA !B6,x		;store to sprite's X speed.
LDA #$A0		;load value
STA !AA,x		;store to sprite's Y speed.

INC !C2,x		;increment sprite state by one

updpos:
JSL $01802A|!BankB	;update sprite positions based on speed (with gravity and object interaction)
RTS			;return.

jumping:
STZ !1510,x		;reset leap index.

LDA !1588,x		;load sprite blocked status flags
AND #$04		;check if blocked below
BEQ .notonground	;if not touching ground, skip ahead.

LDA #$60		;load amount of frames
STA !15AC,x		;store to timer.
STZ !C2,x		;reset sprite state.
STZ !AA,x		;reset sprite's Y speed.
BRA updpos		;branch to update positions

.notonground
LDA !1588,x		;load sprite blocked status flags again
AND #$03		;check if blocked on sides
BEQ +			;if not, skip ahead.

STZ !B6,x		;reset sprite's X speed.

+
JSR ProxCalc		;calculate proximity

XBA			;flip high/low bytes of A
BNE updpos		;if over a screen away, keep the sprite going.
XBA			;flip high/low bytes of A
CMP #$7F		;compare to value
BCS updpos		;if over $7F pixels away, keep the sprite going.

CMP #!Proximity2	;compare to value
BCS updpos		;if higher, don't change state.

INC !C2,x		;increment sprite state by one
LDA #$10		;load amount of frames
STA !15AC,x		;store to timer.
RTS			;return.

pause:
STZ !AA,x		;reset sprite's Y speed.
STZ !B6,x		;reset sprite's X speed.

LDA !15AC,x		;load sprite timer
BNE +			;if not zero, return.

LDA #$10		;load value
STA !AA,x		;store to sprite's Y speed.
INC !C2,x		;increment sprite state by one

+
RTS			;return

fall:
LDA !1588,x		;load sprite blocked status flags
AND #$04		;check if blocked below
BEQ .notonground	;if not touching ground, skip ahead.

LDA #$18		;load value
STA $1887|!Base2	;store to shake ground timer.
LDA #$09		;load SFX value
STA $1DFC|!Base2	;store to address to play it.

STZ !C2,x		;reset sprite state.
STZ !AA,x		;reset sprite's Y speed.
LDA #$60		;load amount of frames
STA !15AC,x		;store to timer.
RTS			;return.

.notonground
JSR updpos		;update sprite positions
BRA updpos		;double the gravity, double the fun

GetDir:
%SubHorzPos()		;get player distance and side relative to sprite
RTS			;return.

;proximity calculator: gets 16-bit distance in pixels between player and sprite,
;always as a positive value
ProxCalc:
JSR GetDir		;call SubHorzPos from label
LDA !14E0,x		;load sprite's X-pos within the level, high byte
XBA			;flip high/low bytes of A
LDA !E4,x		;load sprite's X-pos within the level, low byte
REP #$20		;16-bit A
CMP $94			;compare to player's X-pos within the level
BCS .sprminusplayer	;if higher, subtract it from the player's pos. else do the opposite.

STA $06			;store 16-bit sprite's X-pos to scratch RAM for subtraction later.
LDA $94			;load player's X-pos within the level
SEC			;set carry
SBC $06			;subtract sprite's X-pos within the level
BRA +			;branch ahead

.sprminusplayer
SEC			;set carry
SBC $94			;subtract player's X-pos

+
SEP #$20		;8-bit A
RTS			;return.

Graphics:
%GetDrawInfo()		;get OAM slot and sprite coordinates on screen

LDA !157C,x		;load sprite direction
STA $02			;store to scratch RAM.

STZ $03			;reset GFX animation pointer

LDA !C2,x		;load sprite state
CMP #$02		;compare to value
BCC +			;if lower, skip ahead.

LDA #$02		;load value
STA $03			;store to animation pointer.

+
STZ $04			;reset shaking offset.

LDA !C2,x		;load sprite state
CMP #$01		;check if on shaking state
BNE +			;if not, skip ahead.

LDA !15AC,x		;load sprite timer
AND #$01		;preserve bit 0 only
ASL			;multiply by 2
STA $04			;store to scratch RAM.

+
LDA !1510,x		;load leap index
STA $05			;store to scratch RAM.

+
PHX			;preserve sprite index
LDX #$01		;loop count

GFXLoop:
LDA $00			;load X displacement
CLC			;clear carry
ADC $04			;add shake index
STA $0300|!Base2,y	;store to OAM.

LDA $01			;load Y displacement
CLC			;clear carry
ADC YDisp,x		;add offset from table according to index
ADC $05			;add leap offset
STA $0301|!Base2,y	;store to OAM.

PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $03			;add animation pointer index into X
TAX			;transfer A to X
LDA Tilemap,x		;load tile number from table according to index
STA $0302|!Base2,y	;store to OAM.

LDX $15E9|!Base2	;load index of current sprite being processed into X
LDA !15F6,x		;load palette/properties from CFG
LDX $02			;load sprite direction into X
ORA Properties,x	;set flip accordingly
ORA $64			;set in level priority bits
STA $0303|!Base2,y	;store to OAM.

INY #4			;increment OAM index four times
PLX			;restore loop count
DEX			;decrement X by one
BPL GFXLoop		;loop while it's positive.

PLX			;restore sprite index
LDY #$02		;load tiles size into Y (all tiles are 16x16)
LDA #$01		;load number of tiles drawn, minus one
JSL $01B7B3|!BankB	;bookkeeping
RTS			;return.