;rideable_balloon
;by smkdan

;DOESN'T USE EXTRA BIT

;balloon that you can step on for a ride

;Tilemaps for SP4

!DIR = $03
!PROPRAM = $04
!PROPINDEX = $05
!XDISPINDEX = $06
!YDISPINDEX = $07
!FRAMEINDEX = $08
!TEMP = $09

XSPD:		db $FC,$00,$04,$00
COUNT:		db $40,$10,$40,$10
XADJUST:	dw $FFFF,$0000,$0001,$0000

SPDCMP:	db $F8,$FB
INCDEC:	db $FF,$01

;C2: riding bit
;1570: behaviour COUNT
;1602: X scroll counter

HEIGHTTBL:
dw $FFE6,$FFD6,$FFD6

CLIPTBL:
dw $0016,$0026,$0026

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
PRINT "INIT ",pc
	RTL

RETURN_L:
	RTS

Run:
	LDA #$00
	%SubOffScreen()

	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE RETURN_L           
	LDA $9D			;locked sprites?
	BNE RETURN_L
	LDA $15D0,x		;yoshi eating?
	BNE RETURN_L

	STZ !C2,x		;reset riding sprite, may get set later

	JSL $01A7DC|!BankB	;mario interact
	BCC NOINTERACTION	;do nothing if there's no contact

        LDA $7D			;don't interact on rising speed
        BMI NOINTERACTION


	LDA $187A|!Base2	;yoshi flag
	CLC
	ASL A
	TAY			;into Y

;control height required to be on top of sprite

	LDA !14D4,x		;sprite high Y
	XBA
	LDA !D8,x		;sprite low Y
	REP #$20		;16bit math
	SEC
	SBC $96			;sub mario Y pos.  Mario is OVER the sprite.  Mario Y < sprite Y
	CMP CLIPTBL,y		;compare depending on mario or yoshi mario
	SEP #$20
	BCC RETURN_I		;RETURN if he's too low (underflows)

;interacting, set positions

	LDA #$01
	STA !C2,x		;set riding sprite for GFX routine

	LDA #$01
	STA $1471|!Base2	;'riding sprite'
	STZ $7D			;no Y speed

	LDA $187A|!Base2	;yoshi flag
	CLC
	ASL A			;2 bytes
	TAY			;into Y reg

	LDA !14D4,x		;sprite high Y
	XBA
	LDA !D8,x		;sprite low Y
	REP #$20
	CLC
	ADC HEIGHTTBL,y		;stepped on height
	STA $96			;new mario Y position
	SEP #$20

NOINTERACTION:
	INC !1602,x		;advance counter
	LDA !1602,x
	LDY !1570,x		;load behaviour as index
	CMP COUNT,y		;X frames
	BCC NOCHANGE		;if COUNT not yet reached, maintain C2

;advance behaviour since COUNT was reached

	LDA !1570,x		;load scroll behaviour...
	INC A
	AND #$03		;4 variables
	STA !1570,x		;...new scroll behaviour
	STZ !1602,x		;reset counter

;apply speed

NOCHANGE:
	LDY !1570,x		;load as index
	LDA XSPD,y		;speed value
	STA !B6,x		;new XSPD

	LDA !C2,x		;load riding bit
	BEQ LEAVEMARIO

;adjust Mario position
	
	LDA !1602,x		;load counter...
	AND #$03
	BNE LEAVEMARIO		;every 2nd frame

	LDA !1570,x		;load balloon behaviour as index
	ASL			;multiply by 2 (blindedit: fixed from remoderation - high byte is now properly handled)
	TAY			;transfer to Y
	REP #$20		;16-bit A
	LDA XADJUST,y		;load adjustment
	CLC			;clear carry
	ADC $94			;MarioX low added
	STA $94			;store
	SEP #$20		;8-bit A


LEAVEMARIO:
RETURN_I:
	LDA !AA,x		;load Yspd
	LDY !C2,x		;load ride bit as index
	CMP SPDCMP,y		;load targetspeed
	BEQ STOREY

	CLC
	ADC INCDEC,y		;add or dec to Yspd

STOREY:
	STA !AA,x		;store Yspd

UPDATEXPOSNOGRVTY:
	JSL $018022|!BankB
	JSL $01801A|!BankB	;update Y pos no gravity
	;JSL $01802A		;update speed

;-----------------------------------------------
;This bit of code will cause the sprite to "pop"
;-----------------------------------------------
	LDA !14D4,x
	XBA
	LDA !D8,x

	REP #$20
	SEC
	SBC $1C
	CMP #$0020
	BCC ERASESPRITE
	SEP #$20

	LDA !1588,x	; If not, check object interaction.
	AND #%00001000	; If the sprite is touching ceiling..
	BNE ERASESPRITE	; Explode.
	RTS

ERASESPRITE:	
	SEP #$20

	STZ !14C8,x
	LDA #$19
	STA $1DFC|!Base2

	LDA #$08 : STA $00 : STA $01
	LDA #$1B : STA $02
	LDA #$01 : %SpawnSmoke()
	RTS
			
;=====

TILEMAP:	db $E0,$E2,$A8,$AA

XDISP:	db $00,$10,$00,$10

YDISP:	db $FE,$FE,$0E,$0E
	db $00,$00,$0E,$0E

GFX:
        %GetDrawInfo()

	LDA !C2,x	;stepped on?
	ASL A		;x4
	ASL A
	STA !FRAMEINDEX

	LDA !157C,x	;direction...
	STA !DIR

	LDA !15F6,x	;properties...
	STA !PROPRAM

	LDX #$00	;loop index

OAM_LOOP:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300|!Base2,y	;xpos

	TXA	
	CLC
	ADC !FRAMEINDEX	;variable YDISP
	PHX
	TAX
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301|!Base2,y	;ypos
	PLX

;LDA $01
;CMP #$40
;BNE Continue
;STZ !14C8,x
;Continue

	LDA TILEMAP,x
	STA $0302|!Base2,y	;chr

	LDA !PROPRAM
	;ORA $64
	STA $0303|!Base2,y	;properties

	INY
	INY
	INY
	INY
	INX
	CPX #$04	;4 tiles
	BNE OAM_LOOP

	LDX $15E9|!Base2	;restore sprite index

	LDY #$02		;16x16 tiles
	LDA #$03		;4 tiles
	JSL $01B7B3|!BankB

	RTS			;RETURN