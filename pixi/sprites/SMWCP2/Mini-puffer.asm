!initxlo = !1504
!initxhi = !1510
!initylo = !151C
!inityhi = !1528

!xlo = !E4
!xhi = !14E0
!ylo = !D8
!yhi = !14D4

!anglelo = !1534
!anglehi = !1570

!rotationSpeed1 = #$0001	;#$0001, #$0002, #$0003, etc. are clockwise.  #$01FF, #$01FE, #$01FD, etc. are counter-clockwise.
!rotationSpeed2 = #$01FF	;This speed will be used when the intial X position is odd.

!rotationSpeed3 = #$0002
!rotationSpeed4 = #$FFFE	;original default values (deemed too fast), but used in bedazzling bastion

tiles:		db $6B,$6D

!property 	= #$3D

!radius1	= #$27	;The radius to use when the extra bit is clear. The current value is 2 blocks.
!radius2	= #$47	;The radius to use when the extra bit is set.   The current value is 4 blocks.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA !xlo,x
STA !initxlo,x
LDA !xhi,x
STA !initxhi,x
LDA !ylo,x
STA !initylo,x
LDA !yhi,x
STA !inityhi,x

STZ !anglelo,x
STZ !anglehi,x
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Quit:
RTS

Main:
LDA !7FAB10,x
AND #$04
BEQ Offscreen3
LDA #$05
BRA DoneWithOffscreen
Offscreen3:
LDA #$03
DoneWithOffscreen:
%SubOffScreen()

JSR Graphics

LDA $13BF|!Base2
CMP #$17
BNE NoAdjust
LDA !166E,x
AND #$EF
STA !166E,x
NoAdjust:
LDA !14C8,x
CMP #$08
BNE Quit
LDA $9D
BNE Quit

LDA !167A,x

JSL $01803A|!BankB

LDA $13BF|!Base2
CMP #$17
BNE NoAdjustment

NoAdjustment:
LDA !initxlo,x
AND #$10
LSR #4
BNE isOdd

LDA $13BF|!Base2
CMP.b #$139-$DC
BNE .norm

REP #$20
LDA !rotationSpeed3
BRA DoneWithRotation

.norm
REP #$20
LDA !rotationSpeed1
BRA DoneWithRotation

isOdd:
LDA $13BF|!Base2
CMP.b #$139-$DC
BNE .norm

REP #$20
LDA !rotationSpeed4
BRA DoneWithRotation

.norm
REP #$20
LDA !rotationSpeed2
DoneWithRotation:
STA $02
SEP #$20

LDA $7FAB10,x
AND #$04
BEQ Option1
LDA !radius2
BRA DoneWithOptions
Option1:
LDA !radius1
DoneWithOptions:
STA $00

LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
CLC
ADC $02
SEP #$20
STA !anglehi,x
XBA
STA !anglelo,x

LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
STA $01
SEP #$20
JSR SIN

LDA !inityhi,x
XBA
LDA !initylo,x
REP #$20
CLC
ADC $03
SEP #$20
STA !ylo,x
XBA
STA !yhi,x


LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
CLC
ADC #$0080
STA $01
SEP #$20
JSR SIN
LDA !initxhi,x
XBA
LDA !initxlo,x
REP #$20
CLC
ADC $03
SEP #$20
STA !xlo,x
XBA
STA !xhi,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:		
			LDA $14
			AND #$04
			LSR #2
			STA !1626,x
			NoIncrement:
		
			PHY
			LDY #$01
			LDA $94
			SEC : SBC $E4,x
			STA $0F
			LDA $95
			SBC !14E0,x
			BPL LABELTHING
			DEY
			LABELTHING:
			TYA
			ASL #6
			STA $0E
			LDA !property
			ORA $0E
			STA $0E
			PLY

			%GetDrawInfo()

			REP #$20
			LDA $00
			STA $0300|!Base2,y
			SEP #$20

			LDA !1626,x
			PHX
			TAX
			LDA tiles,x
			PLX
			STA $0302|!Base2,y
	
			LDA $0E
			STA $0303|!Base2,y

			INY #4

			LDY #$02                ; \ we've already set 460 so use FF
			LDA #$00                ; | A = number of tiles drawn - 1
			JSL $01B7B3|!BankB      ; / don't draw if offscreen
QuitGraphics:		RTS                     ; return


;-------------------------------;SIN JSL
SIN:	PHP			;From: Support.asm's JSL.asm
	PHX			;By: 
				;Comment Translation+Addition By: Fakescaper
	TDC			;LDA #$0000
	LDA $01			;This determines the Ypos if you're using it for sprite movement
	REP #$30		;16-bit AXY
	ASL A			;$00     = Radius
	TAX			;$01/$02 = Angle ($0000-$01FF)
	LDA $07F7DB|!BankB,x	;SMW's 16-bit CircleCoords table
	STA $03			;
				;
	SEP #$30		;8bit AXY
	LDA $02			;
	PHA			;
	LDA $03			;
	STA $4202		;
	LDA $00			;
	LDX $04			;
	 BNE .IF1_SIN		;
	STA $4203		;
	ASL $4216		;
	LDA $4217		;
	ADC #$00		;
.IF1_SIN			;
	LSR $02			;
	 BCC .IF_SIN_PLUS	;
				;
	EOR #$FF		;XOR
	INC A			;
	STA $03			;
	 BEQ .IF0_SIN		;
	LDA #$FF		;
	STA $04			;
	 BRA .END_SIN		;
				;
.IF_SIN_PLUS			;
	STA $03			;
.IF0_SIN			;
	STZ $04			;
.END_SIN			;
	PLA			;
	STA $02			;$02
	PLX			;
	PLP			;
	RTS			;Return
;-------------------------------;