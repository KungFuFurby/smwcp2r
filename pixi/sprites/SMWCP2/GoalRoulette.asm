;It's hard to keep track of authors of such collab man, noone comments that on files
;this sucks
;I could even claim as my own and noone would question but meh, I'm not a pirate

;btw sprite was seriously optimized, and more stuff was added. thank you you're welcome

!goal_flag = $1696|!Base2	;a.k.a. Disable Screen Barrier Flag.
!GoalSong = $04

TILEMAP:
	db $24,$E8,$44,$E8,$26,$E8,$24,$E8
PROPERTIES:
	db $38,$34,$3A,$34,$7A,$34,$3A,$34

print "INIT ",pc
	LDA !E4,x
	CLC
	ADC #$08
	STA !E4,x
	LDA !14E0,x
	ADC #$00
	STA !14E0,x
	LDA !D8,x
	SEC
	SBC #$09
	STA !D8,x
	LDA !14D4,x
	SBC #$00
	STA !14D4,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR SpriteCode
	PLB
	RTL

;===================================
;Sprite Function
;===================================

SpriteCode:
	LDA !1534,x
	DEC
	BEQ FROZEN
	LDA $14
	LSR #3
	CLC
	ADC $15E9|!Base2
	AND #$07
	STA !1510,x
	BRA normal

FROZEN:
	LDA !1528,x
	CMP #$20
	BCS PAUSE
	INC !1528,x
	LDA !D8,x
	BEQ SCREENUP
	DEC !D8,x
	JMP Graphics

SCREENUP:
	LDA #$FF
	STA !D8,x
	STZ !14D4,x
normal:
	JSR Graphics
	LDA !14C8,x
	EOR #$08
	ORA $9D
	BNE return

	LDA #$03
	%SubOffScreen()

	LDA !1534,x
	BNE return

	JSL $01A7DC|!BankB
	BCC return

Rising:
	STZ $1411|!Base2
	STZ $7B
	LDA #$01
	STA $76

	LDA #$07
	STA $0F00|!Base2		;move down all status counters
	LDA #$FF
	STA $0F01|!Base2
	STA $0F02|!Base2		;all status bar timers constantly this value so they are always displayed
	STA $0F03|!Base2

	LDA #$14
	STA $1DFC|!Base2
	LDA #!GoalSong
	STA $1DFB|!Base2
	LDA #$01
	STA !1534,x

	LDA !7FAB10,x
	AND #$04
	BEQ .normexit

	LDA #$06
	BRA +

.normexit
	LDA #$01
+
	STA !goal_flag
	JSL $07FC3B|!BankB

return:
	RTS


PAUSE:
	JSR Graphics
	LDA !1528,x
	CMP #$5A
	BEQ DECIDE
	INC !1528,x
	RTS
	
DECIDE:
	LDA !C2,x
	BEQ +

	LDA #$07
	STA $0F00|!Base2
	STA $0F01|!Base2
	STA $0F02|!Base2		;all status bar timers constantly this value so they are always displayed
	STA $0F03|!Base2

	LDA $0EFC|!Base2		;!CoinCounterYPos from RedoneSMWCHUD.asm
	CMP #$08			;!StopYPos from RedoneSMWCHUD.asm
	BNE .nomore			;so, if the counter isn't being displayed, coins aren't incremented.

	LDA !1594,x
	BEQ .nomore

	LDA $14
	AND #$01
	BNE .nomore

	DEC !1594,x
	INC $13CC|!Base2
	LDA #$01
	STA $1DFC|!Base2
.nomore
	RTS

+
	INC !C2,x

	LDA !1510,x
	JSL $0086DF|!BankB
	dw shroom
	dw ten_coins
	dw nothing
	dw one_coin
	dw flower
	dw thirty
	dw one_up
	dw five

shroom:
	LDA $19			; make big only when small
	BNE flower_already_got_it
	LDA #$02
	STA $71
	LDA #$2F
	STA $1496|!Base2
	BRA flower_FinalizePwrUp

flower:
	LDA $19			; don't remove cape
	CMP #$02
	BCS .already_got_it
	LDA #$04
	STA $71
	LDA #$03
	STA $19
	LDA #$20
	STA $149B|!Base2

.FinalizePwrUp
	LDA #$0A
	STA $1DF9|!Base2
	INC $9D
	RTS

.already_got_it	
	LDA #$0B
	STA $1DFC|!Base2
	RTS

one_up:
	INC $18E4|!Base2
	RTS

nothing:
	LDA #$20
	STA $1DF9|!Base2
	RTS

one_coin:
	LDA #1
	BRA StoreCoins

ten_coins:
	LDA #10
	BRA StoreCoins

five:
	LDA #5
	BRA StoreCoins

thirty:
	LDA #30

StoreCoins:
	STA !1594,x
-	RTS

;===================================
;Graphics Code
;===================================

Graphics:
	LDA !C2,x
	BNE -

	%GetDrawInfo()
	REP #$20
	LDA $00				; xy pos
	STA $0300|!Base2,y
	SEP #$20

	LDA !1510,x			; do the animation
	PHX
	TAX
	LDA TILEMAP,x
	STA $0302|!Base2,y
	LDA PROPERTIES,x		; props
	ORA $64
	STA $0303|!Base2,y
	PLX
	LDY #$02
	LDA #$00
	JSL $01B7B3|!BankB
	RTS