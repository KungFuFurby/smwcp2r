;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; World 4A Boss (Jelectro)
;;
;;    Created by cstutor89
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constants
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!HIT_POINTS = $03
	!DANGER_ZONE = $02
	!ORIGINAL_PALETTE = $37
	!ELEC_PALETTE = $35
	!HURT_PALETTE = $39	;actually charge palette
	!hurtpal = $3F		;actual flash when hurt palette

!projectile = $B1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INIT/MAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc
	STZ $C2,x
	LDA #$FF
	STA $1540,x
	STZ $1FD6,x
	LDA #!ORIGINAL_PALETTE 
	STA $15F6,x
	LDA #$08
	STA $14C8,x
	RTL

PRINT "MAIN ",pc
	PHB		;\
	PHK		; | Change the data bank to the one our code is running from.
	PLB		; | This is a good practice.
	JSR SPRITECODE	; | Jump to the sprite's function.
	PLB		; | Restore old data bank.
	RTL		;/ And RETURN.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite Main Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITECODE:	JSR GRAPHICS
		LDA $9D
		BNE RETURN
		JSR HITDETECTION
		JSL $81A7DC
		BCC CHOOSEST
		JSL $80F5B7

CHOOSEST:	LDA $C2,x
		JSL $8086DF
	dw FOLLOW
	dw STALL
	dw FLYUP
	dw ELECTRICITY
	dw HOMING
	dw SWEEP
	dw HURT
	dw GOAL
	dw POSITIONUP
	dw POSITIONDOWN
	dw RAIN
	dw RETURN

RETURN:		RTS

HITDETECTION:	LDA $1558,x
		BNE RETURN	
		LDA $14D4,x
		BNE RETURN
		LDA $D8,x
		CMP #$88
		BCC RETURN
		CMP #$D9
		BCS RETURN
		LDA $14E0,x
		BEQ X1_CHECK
		CMP #$FF
		BEQ X2_CHECK
		BRA RETURN

X1_CHECK:	LDA $E4,x
		CLC
		ADC #$07
		cmp #$E0
		bcs +
		CMP #$D0
		BCS WAS_HIT
		LDA $E4,x
		cmp #$09
		bcc +
		CMP #$19
		BCC WAS_HIT	
		+
		RTS

X2_CHECK:	LDA $E4,x
		CMP #$F8
		BCS WAS_HIT
		RTS

WAS_HIT:	INC $1528,x		; Increase the HP's used.
		LDA #$68
		STA $1540,x
		STA $1558,x		; Set HURT Invincibility
		LDA #$06
		STA $C2,x		; Set Jelectro's State to HURT (6).
		LDA #$28                ; \ sound effect
		STA $1DFC               ; /
		STZ $AA,x
		STZ $B6,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 0: FOLLOW Mario
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ACCELERATIONX:	db $01,$FF
MAXSPEEDX:	db $0C,$F4
ACCELERATIONY:	db $01,$FF
MAXSPEEDY:	db $0C,$F4
STATE_MACH:	db $00,$01,$02,$00,$01,$04,$00,$01,$03,$00,$01,$08,$00,$01,$04,$00
STATE_TIME:	db $FF,$40,$58,$FF,$FF,$FF,$FF,$FF,$FF

FOLLOW:		LDA $1540,x
		BEQ NEXT_STATE
		LDA $14
		AND #$01
		BNE APPLYSPEED
		%SubHorzPos()		;\ if max horizontal speed in the appropriate
		LDA $B6,x		; | direction achieved,
		CMP MAXSPEEDX,y		; |
		BEQ MAXXSPEED		;/ don't change horizontal speed
		CLC			;\ else,
		ADC ACCELERATIONX,y	; | accelerate in appropriate direction
		STA $B6,x		;/
MAXXSPEED:	%SubVertPos()		;\ if max vertical speed in the appropriate
		LDA $AA,x		; | direction achieved,
		CMP MAXSPEEDY,y		; |
		BEQ APPLYSPEED		;/ don't change vertical speed
		CLC			;\ else,
		ADC ACCELERATIONY,y	; | accelerate in appropriate direction
		STA $AA,x		;/
APPLYSPEED:	JSL $818022		; apply x speed
		JSL $81801A		; apply y speed
		RTS

NEXT_STATE:	PHX
		LDA $1FD6,x
		CLC
		ADC #$01
		AND #$0F

CMP #$06
BNE +

LDY $1528,x
BNE +

LDA #$00

+
		STA $1FD6,x
		TAX
		LDA STATE_MACH,x
		PLX
		STA $C2,x
		PHX
		TAX
		LDA STATE_TIME,x
		PLX
		STA $1540,x
		STZ $AA,x
		STZ $B6,x
		LDA $14D4,x
		STA $192C
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 1: STALL Time
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STALL:		LDA $1540,x
		BEQ NEXT_STATE
		AND #$1F
		TAY
		LDA SWEEP_SPEEDX,y
		STA $AA,x		
		JSL $81801A		
		RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 2: Fly To Corner
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FLY_DIRECT:	db $08,$F8

FLYUP:		LDA $D8,x
		CMP #$30
		BCC CHANGE_FLY
		LDA $1540,x
		CMP #$57
		BNE CALC_FLY
		JSR DETER_DIR
CALC_FLY:	DEC $AA,x
		JSL $818022		; apply x speed
		JSL $81801A		; apply y speed
		RTS

DETER_DIR:	JSL $81ACF9
		LDA $148D
		AND #$01
		TAY
		LDA FLY_DIRECT,y
		STA $B6,x
		STZ $1936
		LDA #$01
		STA $1937
		LDA $B6,x
		CMP #$10
		BNE END_FLY
		LDA #$02
		STA $1936
		LDA #$FF
		STA $1937
END_FLY:	RTS

CHANGE_FLY:	LDA #$05
		STA $C2,x
		LDA #$02
		STA $1540,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 3: Shoot ELECTRICITY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

T_SPEEDX:	db $24,$DC,$40,$C0
T_SPEEDY:	db $00,$00,$00,$00

ELECTRICITY:	LDA $1540,x
		BNE SHOOTCHARGE
		JMP NEXT_STATE

SHOOTCHARGE:	LDA $1540,x
		AND #$3F
		BEQ SHOOTELEC
		CMP #$28
		BCS GLOWELEC
		LDA #!ORIGINAL_PALETTE  
		STA $15F6,x
		JMP FOLLOW
		
SHOOTELEC:	LDA #!ELEC_PALETTE
		STA $15F6,x

PHX
LDX #$01

rays:
STZ $03
LDA #!projectile
SEC
%SpawnSprite()

LDA #$08
STA !9E,y
LDA #$02
STA !1528,y

LDA ShotSpds,x
STA !B6,y

		PHX
		LDX $15E9
		LDA $E4,x
		CLC
		ADC #$08
            	STA $00E4,y
           	LDA $14E0,x
		ADC #$00
               	STA $14E0,y
		LDA $D8,x
		CLC
		ADC #$08
            	STA $00D8,y
           	LDA $14D4,x
		ADC #$00
               	STA $14D4,y
		PLX
DEX
BPL rays

PLX
		LDA #$10
		STA $1DF9
		RTS

ShotSpds:
db $28,$D8

GLOWELEC:
LDA $14
LSR
AND #$01
BEQ +

		LDA #!ORIGINAL_PALETTE
		BRA ++
+
		LDA #!ELEC_PALETTE 
++
		STA $15F6,x
		RTS	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 4: HOMING 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HOMING_ACCX:	db $01,$FF
HOMING_MAXX:	db $20,$E0
HOMING_ACCY:	db $01,$FF
HOMING_MAXY:	db $20,$E0

HOMING:
LDA $160E,x
CMP #$FF
BNE MOVE_HOME

LDA #!ORIGINAL_PALETTE 
STA $15F6,x
STZ $160E,x
JMP NEXT_STATE

MOVE_HOME:
INC $160E,x	;increment this crap every frame please

LDA $160E,x
CMP #$28
BCC leflash
CMP #$54
BCS APPLYSPEED2
CMP #$48
BNE simpacc

JSR aimin
BRA APPLYSPEED2

simpacc:
%SubHorzPos()
LDA $B6,x
CMP HOMING_MAXX
BEQ .noxacc

CLC
ADC HOMING_ACCX,y
STA $B6,x

.noxacc
REP #$20
LDA $96
PHA
CLC
ADC #$0010
STA $96
SEP #$20
%SubVertPos()
REP #$20
PLA
STA $96
SEP #$20

LDA $AA,x
CMP HOMING_MAXY
BEQ noyacc

;LDA $14
;AND #$01			;I (BD) did this so it'd be easier to lure the boss to the spikes but well it's too dumb so it was reverted
;BNE noyacc

LDA $AA,x
CLC
ADC HOMING_ACCY,y
STA $AA,x
BRA noyacc

leflash:
STZ !AA,x
STZ !B6,x

LDA $160E,x
CMP #$37
BNE +

STZ $1DF9
LDA #$25
STA $1DF9

LDA #$39
STA $160E,x

+
LDA $14
LSR
AND #$01
BEQ noyacc

LDA #!ORIGINAL_PALETTE
BRA +

noyacc:
LDA #!HURT_PALETTE

+
STA !15F6,x

APPLYSPEED2:	JSL $818022		; apply x speed
		JSL $81801A		; apply y speed
		RTS

aimin:
LDA #$B0
STA $160E,x

!no = 1

if !no == 0
LDA $14E0,x	;load sprite X pos, high byte
STA $01		;store to scratch RAM for later calculation.
LDA $E4,x	;load sprite X pos, low byte
STA $00		;store to scratch RAM for later calculation.

LDA $14D4,x	;load sprite Y pos, high byte
STA $03		;store to scratch RAM for later calculation.
LDA $D8,x	;load sprite Y pos, low byte
STA $02		;store to scratch RAM for later calculation.

REP #$20	;16-bit A
LDA $02		;load 16-bit sprite Y pos from scratch RAM
SEC		;set carry
SBC #$0012	;subtract #$12 so we get some aiming alignment
STA $02		;store result back.

LDA $00		;load 16-bit sprite X pos from scratch RAM
SEC		;set carry
SBC $94		;subtract player's X pos within the level from it
STA $00		;store result back.
LDA $02		;load 16-bit sprite Y pos from scratch RAM
SEC		;set carry
SBC $96		;subtract player's Y pos within the level from it
STA $02		;store result back.
SEP #$20	;8-bit A

LDA #$20		;load index for aiming speed
%Aiming()		;at long last, call aiming (speed calculator) routine.

LDA $02			;load calculated Y speed from scratch RAM
STA $AA,x		;store it to sprite's Y speed.
LDA $00			;load calculated X speed from scratch RAM
STA $B6,x		;store it to sprite's X speed.
endif

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 5: SWEEP Grounds
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

START_POSX:	db $48,$70,$A8
START_POSY_LO:	db $08,$18,$08
START_POSY_HI:	db $00,$01,$00
FINISH_POSY_LO:	db $48,$28,$48
FINISH_POSY_HI:	db $01,$00,$01
SWEEP_SPEEDX:	db $00,$01,$02,$04,$07,$0B,$0F,$16,$16,$0F,$0B,$07,$04,$02,$01,$00,$FF,$FE,$FD,$FB,$F9,$F4,$F0,$EA,$EA,$F0,$F4,$F9,$FB,$FD,$FE,$FF
SWEEP_SPEEDY:	db $18,$E8,$18

SWEEP:		LDA $1540,x
		CMP #$01
		BEQ FIRSTSWEEP
		LDY $1936
		LDA FINISH_POSY_HI,y
		CMP $14D4,x
		BNE NOTTHEREYET
		LDA $1936
		CMP #$01
		BEQ GOING_UP
		LDA FINISH_POSY_LO,y
		CMP $D8,x
		BCC CHANGESWEEP
		BRA NOTTHEREYET
GOING_UP:	LDA FINISH_POSY_LO,y
		CMP $D8,x
		BCS CHANGESWEEP
NOTTHEREYET:	LDA SWEEP_SPEEDY,y
		STA $AA,x
		LDA $1564,x
		AND #$3F
		SEC
		SBC #$20
		STA $B6,x
		LDA $1564,x
		AND #$40
		BEQ UPDATE_SWEEP
		LDA $B6,x
		EOR #$FF
		STA $B6,x
UPDATE_SWEEP:	JSL $818022		; apply x speed
		JSL $81801A		; apply y speed
		RTS
STOPSWEEP:	JMP NEXT_STATE

CHANGESWEEP:	LDA #$C0
		STA $1564,x
		LDA $1936
		CMP #$01
		BEQ NEXT_SWEEP
NEXT_SWEEP:	LDA $1936
		CLC
		ADC $1937
		STA $1936
		CMP #$FF
		BEQ STOPSWEEP
		CMP #$03
		BEQ STOPSWEEP
FIRSTSWEEP:	LDA #$C0
		STA $1564,x
		LDY $1936
		LDA START_POSX,y
		STA $E4,x
		STZ $14E0,x
		LDA START_POSY_LO,y
		STA $D8,x
		LDA START_POSY_HI,y
		STA $14D4,x
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 6: HURT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HURT:		LDA $1558,x
		BNE CHECKDEATH
STZ $160E,x
		LDA $1528,x
		CMP #!DANGER_ZONE
		BNE GOSTATE

GOSTATE:	LDA #!ORIGINAL_PALETTE
		STA $15F6,x
		JMP NEXT_STATE
CHECKDEATH:	LDA $1528,x
		CMP #!HIT_POINTS
		BEQ SETDSTATE

LDA $14
LSR
AND #$01
BNE +

LDA #!ORIGINAL_PALETTE
BRA ++

+
LDA #!hurtpal

++
STA $15F6,x
JMP FOLLOW
		
SETDSTATE:	LDA #$20
		STA $1540,x
		LDA #$07
		STA $C2,x
		LDA #!hurtpal
		STA $15F6,x
		RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 7: GOAL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
GOAL:		LDA $1540,x
		BNE GOAL_WAIT

		LDA #$0B
		STA $1696|!Base2	;uberasm victory flag by blind devil
		LDA #$04
		STA $14C8,x             ; Destroy Sprite 
		LDA #$1F
		STA $1540,x

GOAL_WAIT:	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 8: Going Up Position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
POSITIONUP:	LDA $14D4,x
		BNE G_UP
		LDA $D8,x
		CMP #$30
		BCS G_UP
		INC $C2,x
		LDA #$30
		STA $E4,x
		LDA #$08
		STA $D8,x
		STZ $AA,x
		RTS
G_UP:		DEC $AA,x
		JSL $81801A		; apply y speed
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State 9: Going Down Position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

POSITIONDOWN:	LDA $D8,x
		CMP #$40
		BCC KEEP_GOING
		INC $C2,x
		LDA #$FF
		STA $1540,x
		STZ $157C,x
		RTS
KEEP_GOING:	INC $AA,x
		JSL $81801A		; apply y speed
		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State A: Electric RAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
RAIN:		LDA $1540,x
		BNE MAYBESHOWER
		LDA #!ORIGINAL_PALETTE
		STA $15F6,x
		JMP NEXT_STATE
MAYBESHOWER:	LDA #!ELEC_PALETTE 
		STA $15F6,x
		LDA $1540,x
		AND #$1F
		BEQ CREATE_BALL
		LDA $E4,x
		CMP #$B0
		BCS CHANGE_DIR
		CMP #$30
		BCC CHANGE_DIR
		BRA NO_CHANGE_DIR
CHANGE_DIR:	LDA $157C,x
		EOR #$01
		STA $157C,x
NO_CHANGE_DIR:	LDA $157C,x
		BEQ SHOWER_RIGHT
		DEC $E4,x
		RTS
SHOWER_RIGHT:	INC $E4,x
		RTS

CREATE_BALL:	JSL $82A9DE             ; \ get an index to an unused sprite slot, RETURN if all slots full
                BMI NOBALL 	         ; / after: Y has index of sprite being generated

		LDA #$08                ; \ set sprite status for new sprite
              	STA $14C8,y             ; /

              	PHX
		LDA #!projectile
               	TYX
               	STA $7FAB9E,x
               	PLX

		LDA #$08
		STA $7FAB10,x

		JSL $81ACF9
		LDA $148D
		AND #$0F
		CLC
		ADC #$04
		CLC
              	ADC $E4,x               ; \ set x position for new sprite
            	STA $00E4,y

           	LDA $14E0,x             ;  |
               	STA $14E0,y             ; /

		LDA $D8,x               ; \ set y position for new sprite
               	SEC                     ;  | (y position of generator - 1)
             	SBC #$00                ;  |
             	STA $00D8,y             ;  |
            	LDA $14D4,x             ;  |
          	SBC #$00                ;  |
              	STA $14D4,y             ; /

		PHX                     ; \ before: X must have index of sprite being generated
                TYX                     ;  | routine clears *all* old sprite values...
                JSL $87F7D2             ;  | ...and loads in new values for the 6 main sprite tables
                JSL $8187A7             ;  get table values for custom sprite
		JSL $81ACF9
		LDA $148D
		AND #$01 
             	ORA #$88
             	STA $7FAB10,x
		PLX                     ; / 

		LDA #$28
		STA !AA,y
		LDA #$00
		STA !B6,y

		LDA #$10
		STA $1DF9
NOBALL:		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Jelectro GRAPHICS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_DISP:		db $10,$00,$10,$00
Y_DISP:		db $10,$10,$00,$00
TILE:		db $22,$20,$02,$00,$26,$24,$06,$04
TILE_S6:	db $62,$60,$42,$40,$66,$64,$46,$44

GRAPHICS:	%GetDrawInfo()
		LDA $14
		LSR A
		LSR A
		LSR A
		AND #$01
		ASL
		ASL
		STA $03
		LDA $15F6,x
		STA $04		
		LDA $C2,x
		CMP #$06
		BEQ STATE_6G
		CMP #$07
		BEQ STATE_6G
			
STATE_0G:	PHX
		LDX #$03
GRAPH_LOOP:	LDA $00
		CLC
		ADC X_DISP,x
		STA $0300,y
	
		LDA $01
		CLC
		ADC Y_DISP,x
		STA $0301,y
		
		PHX
		TXA
		CLC
		ADC $03
		TAX
		LDA TILE,x
		STA $0302,y
		PLX
		
		LDA $04
		STA $0303,y
		
		INY
		INY
		INY
		INY
		DEX
		
		BPL GRAPH_LOOP
	
GRAPH_END:	PLX
		LDY #$02		; Y ends with the TILE size .. 02 means it's 16x16
		LDA #$03		; A -> number of tiles drawn - 1.
					; I drew 2 tiles, so 2-1 = 1. A = 01.

		JSL $81B7B3		; Call the routine that draws the sprite.
		RTS			; Never forget this!

STATE_6G:	PHX
		LDX #$03
GRAPH_LOOP6:	LDA $00
		CLC
		ADC X_DISP,x
		STA $0300,y
	
		LDA $01
		CLC
		ADC Y_DISP,x
		STA $0301,y
		
		PHX
		TXA
		CLC
		ADC $03
		TAX
		LDA TILE_S6,x
		STA $0302,y
		PLX
		
		LDA $04
		STA $0303,y
		
		INY
		INY
		INY
		INY
		DEX
		
		BPL GRAPH_LOOP6
		JMP GRAPH_END