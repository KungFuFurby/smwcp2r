;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!FrameNum = $C2
!Direction = $157C
!Carrying = $1510
!Moving = $1504
!ClawPos = $1528
!WaitTimer = $1540
!InteractWaitTimer = $15AC
!JustGotFlag = $1594
!OpenClawTimer = $154C
!ClawRange = $7FAB34
!ClawSpriteNum = #$2B

                    print "INIT ",pc
                    STZ !FrameNum,x
                    STZ !Carrying,x
					LDA $7FAB10,x
					AND #$04
					LSR #2
                    STA !Direction,x
                    STZ !WaitTimer,x
                    STZ !JustGotFlag,x
					STZ !Moving,x
					STZ !ClawPos,x
                    RTL

	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $10,$F0



SPRITE_ROUTINE:	  	

					LDA $9D
					BNE .sendreturn
					LDA $14C8,x
					CMP #$08
					BEQ +
					.sendreturn
					JMP return
					+
					JSL $019138
					LDA !Carrying,x
					BEQ +
					STZ $140D
					+
					LDA !Carrying,x
					BNE +
					LDA !ClawPos,x
					BEQ +
					DEC !ClawPos,x
					+
					LDA $1588,x
					BEQ +
					LDA !Moving,x
					BEQ +
					STZ !Moving,x
					LDA #$30
					STA !WaitTimer,x
					LDA !Direction,x
					EOR #$01
					STA !Direction,x
					+
					LDA !InteractWaitTimer,x
					BNE +
					LDA !Carrying,x
					BNE +
					LDA !ClawPos,x
					STA $00
					STZ $01
					LDA $14D4,x
					PHA
					XBA
					LDA $D8,x
					PHA
					REP #$20
					CLC
					ADC $00
					SEP #$20
					STA $D8,x
					XBA
					STA $14D4,x
					JSL $81A7DC
					PHP
					PLA
					STA $00
					PLA
					STA $D8,x
					PLA
					STA $14D4,x
					LDA $00
					PHA
					PLP
					BCC +
					ldy #$0C
					-
						;Unsafe, doesn't check to make sure custom spite. Don't mix with sume brother's lightning.
						lda $009E,y
						cmp.b !ClawSpriteNum
						bne .NotClaw
							lda !Carrying,y
							bne +
						.NotClaw
						dey
						bpl -
					LDA #$01
					STA !Carrying,x
					STA !JustGotFlag,x
					STA !Moving,x
					+
					LDA !WaitTimer,x
					BNE .Waiting
					LDA !Moving,x
					BEQ +
					LDY !Direction,x
					LDA XSpeeds,y
					STA $B6,x
					STZ !JustGotFlag,x
					LDA !Carrying,x
					BEQ +++
					LDA $77
					BIT #$03
					BEQ ++
					STZ !Carrying,x
					LDA #$20
					STA !InteractWaitTimer,x
					++
					LDA $77
					BIT #$08
					BEQ ++
					++
					LDA $15
					BIT #$04
					BEQ ++
					LDA $77
					BIT #$04
					BNE +++
					LDA !ClawPos,x
					CMP !ClawRange,x
					BEQ +++
					INC !ClawPos,x
					BRA +++
					++
					LDA $15
					BIT #$08
					BEQ +++
					LDA !ClawPos,x
					BEQ +++
					LDA $77
					BIT #$08
					BNE +++
					DEC !ClawPos,x
					+++
					LDA $16
					BIT #$80
					BEQ +
					LDA !Carrying,x
					BEQ +
					LDA #$20
					STA !InteractWaitTimer,x
					STZ !Carrying,x
					LDA #$30
					STA !OpenClawTimer,x
					+
					BRA .NotWaiting
					.Waiting
					LDA !JustGotFlag,x
					BNE +
					LDA !WaitTimer,x
					AND #$07
					CMP #$07
					BNE +
					LDA !FrameNum,x
					BEQ ++
					DEC !FrameNum,x
					++
					LDA !Carrying,x
					BEQ +
					STZ !Carrying,x
					LDA #$20
					STA !InteractWaitTimer,x
					+
					STZ $B6,x
					.NotWaiting
					LDA !OpenClawTimer,x
					AND #$07
					CMP #$07
					BNE +
					LDA !FrameNum,x
					BEQ +
					DEC !FrameNum,x
					+
					JSL $018022
					LDA !Carrying,x
					Bne + 
						jmp return
					+
					LDA #$02
					STA !FrameNum,x
					STZ $7D
					STZ $7B
					LDA $14E0,x
					XBA
					LDA $E4,x
					REP #$20
					sta $00
					sec
					sbc $94
					sta $02
					bpl +
						eor #$FFFF
						inc
					+
					cmp #$0004
					lda $00
					bcc +
						lda $94
						clc
						adc #$0004
						sta $04
						lda $02
						bpl ++
							lda $04
							sec
							sbc #$0008
							sta $04
						++
						lda $04
					+
					STA $94
					SEP #$20
					LDA $19
					BNE +
					REP #$20
					LDA #$0013
					STA $00
					SEP #$20
					BRA ++
					+
					REP #$20
					LDA #$001B
					STA $00
					SEP #$20
					++
					LDA !ClawPos,x
					STA $02
					STZ $03
					LDA $14D4,x
					XBA
					LDA $D8,x
					REP #$20
					CLC
					ADC $00
					CLC
					ADC $02
					sta $00
					sec
					sbc $96
					sta $02
					bpl +
						eor #$FFFF
						inc
					+
					cmp #$0008
					lda $00
					bcc +
						lda $96
						clc
						adc #$0008
						sta $04
						lda $02
						bpl ++
							lda $04
							sec
							sbc #$0010
							sta $04
						++
						lda $04
					+
					STA $96
					SEP #$20
					LDA #$24
					STA $13E0
					return:
					LDA #$05 : %SubOffScreen()
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine

TileSize:
db $02,$02,$02

TileProperties:
db $00,$00,$40

XOff:
db $00,$F8,$08

YOff:
db $00,$18,$18

ClawTiles:
db $80,$82,$84									;Tiles to use for the first, second, and third frames of the claw

SUB_GFX:            %GetDrawInfo()     	;Get all the drawing info, duh
					LDA !Carrying,x				;This is a bit confusing, but these are the tile numbers. Didn't make any sense to use a table for this
					BEQ +
					LDA #$E4					;Tile number for the green block
					BRA ++
					+
					LDA #$E2					;Tile number for the red block
					++
					STA $02
					LDA !FrameNum,x
					PHX
					TAX
					LDA ClawTiles,x				;The tile to use for the claw. Defined above.
					STA $03
					STA $04
					
					LDX #$02
					.GFXLoop
					LDA $00
					CLC
					ADC XOff,x
					STA $0300,y
					
					LDA $01
					CLC
					ADC YOff,x
					CPX #$01
					BCC +
					PHX
					LDX $15E9
					CLC
					ADC !ClawPos,x
					PLX
					+
					STA $0301,y
					
					LDA $02,x
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					ORA TileProperties,x
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA #$04
					LDY #$FF
					JSL $81B7B3					;/and then draw em
					EndIt:
					RTS