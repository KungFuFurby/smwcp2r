;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Yoshi's Island Chomp Rock (NON DYNAMIC VERSION)
; Programmed by SMWEdit
;
; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!PlatformY = $FFD6      ; Y position of platform relative to sprite
!PlatformL = $FFF3      ; Left X boundary
!PlatformR = $000C      ; Right X boundary

!LeftPushT = $FFD8      ; Y position of top of left push area
!LeftPushB = $0000      ; Y position of bottom of left push area
!LeftPushX = $FFF0      ; X position for pushing
!LeftPushW = $0000      ; extent (forward) of interaction field

!RightPushT = $FFD8     ; Y position of top of left push area
!RightPushB = $0000     ; Y position of bottom of left push area
!RightPushX = $0010     ; X position for pushing
!RightPushW = $0001     ; extent (backward) of interaction field

!LeftRollMax = $FFFA    ; max for standing and making rock roll left
!RightRollMax = $0006   ; max for standing and making rock roll right

!SpriteKillSFX = $37    ; The sound effect to make when killing sprites.
!SpriteKillBank = $1DFC

; Names for sprite tables and temporary variables; do not touch these.
!sprite_offset = !1528
!sprite_start_offset = !1504
!sprite_flip_xy = !151C

!sprite_standing_last_frame = !1626

!sprite_last_x_low = !1534
!sprite_last_x_high = !1570
!sprite_last_y_low = !1594
!sprite_last_y_high = !1602

!scratch_x = $04
!scratch_y = $06

!scratch_x2 = $08
!scratch_y2 = $0A

!scratch = $0C

print "INIT ",pc
    LDA !E4,x
    STA !sprite_start_offset,x
    JSR StorePosition
    RTL

print "MAIN ",pc
    PHB : PHK : PLB
    JSR SpriteCode
    PLB
    RTL

StorePosition:
    LDA !E4,x                   ; \ 
    STA !sprite_last_x_low,x    ;  | store current position
    LDA !14E0,x                 ;  | to sprite tables for
    STA !sprite_last_x_high,x   ;  | use next time sprite
    LDA !D8,x                   ;  | routine is called.
    STA !sprite_last_y_low,x    ;  | It's used for moving mario
    LDA !14D4,x                 ;  | while he's standing on it
    STA !sprite_last_y_high,x   ; /
Return:
    RTS

SpriteCode:
    JSR Graphics
    LDA !14C8,x                 ; \  return if
    CMP #$08                    ;  | sprite
    BNE Return                  ; /  status != 8
    LDA $9D                     ; \ return if
    BNE Return                  ; / sprites locked
    LDA #$00
    %SubOffScreen()             ; only process sprite while on screen

    JSR PositionOffsetStart     ; interaction improvement offset

    LDA $187A|!Base2            ; \ don't shift
    BEQ NoYoshi                 ; / if not on Yoshi
    LDA $96                     ; \ 
    CLC                         ;  | offset Y
    ADC #$10                    ;  | by #$10
    STA $96                     ;  | again to
    LDA $97                     ;  | compensate
    ADC #$00                    ;  | for yoshi
    STA $97                     ; /
NoYoshi:
    LDA !sprite_last_x_low,x    ; \ 
    STA !scratch_x2             ;  | store sprite's old
    LDA !sprite_last_x_high,x   ;  | X and Y positions
    STA !scratch_x2+1           ;  | into scratch
    LDA !sprite_last_y_low,x    ;  | RAM for use
    STA !scratch_y2             ;  | in some of the
    LDA !sprite_last_y_high,x   ;  | following code
    STA !scratch_y2+1           ; /
    LDA !E4,x                   ; \ 
    STA !scratch_x              ;  | store sprite X
    LDA !14E0,x                 ;  | and Y position
    STA !scratch_x+1            ;  | into scratch
    LDA !D8,x                   ;  | RAM for use
    STA !scratch_y              ;  | in some of the
    LDA !14D4,x                 ;  | following code
    STA !scratch_y+1            ; /
    LDA !E4,x                           ; \ 
    SEC                                 ;  | set offsets
    SBC !sprite_start_offset,x          ;  | for rotation
    PHA                                 ;  |
    AND #%00001111                      ;  |
    STA !sprite_offset,x                ;  |
    PLA                                 ;  |
    AND #%00010000                      ;  |
    STA !sprite_flip_xy,x               ; /
    LDA !sprite_standing_last_frame,x   ; \ check if mario was
    BEQ NotStandingLastFrame            ; / standing last frame
    LDA $77                             ; \  don't move mario if
    AND #%00000011                      ;  | he is hitting the side
    BNE NoMoveMario                     ; /  of an object
    PHP                                 ; \ 
    REP #%00100000                      ;  | move mario
    LDA !scratch_x                      ;  | 2 pixels for
    SEC                                 ;  | every pixel
    SBC !scratch_x2                     ;  | the sprite
    ASL A                               ;  | moves
    CLC                                 ;  |
    ADC $94                             ;  |
    STA $94                             ;  |
    PLP                                 ; /
NoMoveMario:
    STZ !sprite_standing_last_frame,x   ; zero this in case it won't be set this frame
NotStandingLastFrame:
    BRA No_NoStandJMP                   ; \ this is used when a standard

NoStandJMP:
    JMP NoStand                         ; / branch is out of range

No_NoStandJMP:
    LDA $7D                     ; \ don't stand on if
    BMI NoStandJMP              ; / mario not moving down
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get minimum
    ADC.w #!PlatformY-1         ; / Y area for standing
    CMP $96                     ; compare with mario's Y position
    BCS NoStand1                ; don't execute next command if area is under mario
    LDY #$01                    ; set Y register = 1
NoStand1:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoStandJMP              ; / then don't stand
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get maximum
    ADC.w #!PlatformY+5         ; / Y area for standing
    CMP $96                     ; compare with mario's Y position
    BCC NoStand2                ; don't execute next command if area is over mario
    LDY #$01                    ; set Y register = 1
NoStand2:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoStandJMP              ; / then don't stand
    PHP                         ; back up processor bits
    REP #%00100000              ; 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get minimum
    ADC.w #!PlatformL           ; / X area for standing
    BPL CMP1                    ; \ if area goes backward past
    LDA.w #$0000                ; / level start then assume zero
CMP1:
    CMP $94                     ; compare with mario's X position
    BCS NoStand3                ; don't execute next command if area is after mario
    LDY #$01                    ; set Y register = 1
NoStand3:
    LDA $7FFFFF
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoStandJMP              ; / then don't stand
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get maximum
    ADC.w #!PlatformR           ; / X area for standing
    BPL CMP2                    ; \ if X area goes backward past
    LDA.w #$0000                ; / level start then assume zero
CMP2:
    CMP $94                     ; compare with mario's X position
    BCC NoStand4                ; don't execute next command if area is before mario
    LDY #$01                    ; set Y register = 1
NoStand4:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoStandJMP              ; / then don't stand
    PHP                         ; \ 
    REP #%00100000              ;  | offset mario's
    LDA !scratch_y              ;  | Y position so
    CLC                         ;  | that he is
    ADC.w #!PlatformY           ;  | standing at
    STA $96                     ;  | specified offset
    PLP                         ; /
    LDA #$01                    ; \ set standing
    STA $1471|!Base2            ; / mode
    PHP                         ; back up processor bits
    REP #%00100000              ; 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get maximum X
    ADC.w #!LeftRollMax         ; / area for left stand-rolling
    BPL CMP7                    ; \ if area goes backward past
    LDA.w #$0000                ; / level start then assume zero
CMP7:
    CMP $94                     ; compare with mario's X position
    BCC NoLeftRoll1             ; don't execute next command if area is after mario
    LDY #$01                    ; set Y register = 1
NoLeftRoll1:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoLeftRoll              ; / then don't stand
    LDA $14                     ; \ 
    AND #%00000011              ;  | "slowly" increase
    BNE NoLeftRoll              ;  | speed of rock
    DEC !B6,x                   ;  |
NoLeftRoll:                     ; /
    PHP                         ; back up processor bits
    REP #%00100000              ; 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get maximum X
    ADC.w #!RightRollMax+1      ; / area for left stand-rolling
    BPL CMP8                    ; \ if area goes backward past
    LDA.w #$0000                ; / level start then assume zero
CMP8:
    CMP $94                     ; compare with mario's X position
    BCS NoRightRoll1            ; don't execute next command if area is after mario
    LDY #$01                    ; set Y register = 1
NoRightRoll1:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoRightRoll             ; / then don't stand
    LDA $14                     ; \ 
    AND #%00000011              ;  | "slowly" increase
    BNE NoRightRoll             ;  | speed of rock
    INC !B6,x                   ;  |
NoRightRoll:                    ; /
    LDA #$01                            ; \ for the next frame, indicate mario
    STA !sprite_standing_last_frame,x   ; / was standing during this frame
NoStand:
        BRA No_NoLeftPushJMP            ; \ this is used when a standard
NoLeftPushJMP:    JMP NoLeftPush        ; / branch is out of range

No_NoLeftPushJMP:
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get top
    ADC.w #!LeftPushT-1         ; / boundary for pushing
    CMP $96                     ; compare with mario's Y position
    BCS NoLeftPush1             ; don't execute next command if area is under mario
    LDY #$01                    ; set Y register = 1
NoLeftPush1:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set
    BEQ NoLeftPushJMP           ; / then don't stand
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get bottom
    ADC.w #!LeftPushB           ; / boundary for pushing
    PHY                         ; back up Y
    LDY $187A|!Base2            ; \ 
    BEQ NotYoshi1               ;  | boundary is lower
    CLC                         ;  | if mario is on Yoshi
    ADC.w #$0010                ; /
NotYoshi1:
    LDY $73                     ; \ 
    BNE NotBig1                 ;  | boundary is lower
    LDY $19                     ;  | if mario is ducking
    BEQ NotBig1                 ;  | or if he is not big
    CLC                         ;  |
    ADC.w #$0008                ; /
NotBig1:
    PLY                         ; load backed up Y
    CMP $96                     ; compare low boundary with mario's Y position
    BCC NoLeftPush2             ; don't execute next command if area is above mario
    LDY #$01                    ; set Y register = 1
NoLeftPush2:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoLeftPush              ; / don't push from left
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get left
    ADC.w #!LeftPushX-1         ; / boundary for pushing
    BPL CMP3                    ; \ if area goes backwards past
    LDA.w #$0000                ; / level start then assume zero
CMP3:
    CMP $94                     ; compare with mario's X position
    BCS NoLeftPush3             ; don't execute next command if area is after mario
    LDY #$01                    ; set Y register = 1
NoLeftPush3:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoLeftPush              ; / don't push from left
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get right
    ADC.w #!LeftPushW           ; / boundary for pushing
    BPL CMP4                    ; \ if area goes backwards past
    LDA.w #$0000                ; / level start then assume zero
CMP4:
    CMP $94                     ; compare with mario's X position
    BCC NoLeftPush4             ; don't execute next command if area is before mario
    LDY #$01                    ; set Y register = 1
NoLeftPush4:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoLeftPush              ; / don't push from left
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDA !scratch_x              ; \ 
    CLC                         ;  | keep mario at
    ADC.w #!LeftPushX           ;  | push offset position
    STA $94                     ; /
    PLP                         ; load backed up processor bits
    LDA $14                     ; \ 
    AND #%00000001              ;  | "slowly" increase
    BNE NoIncSpeedL             ;  | speed of rock
    INC !B6,x                   ;  |
NoIncSpeedL:                    ; /
    LDA !B6,x                   ; \ 
    CMP $7B                     ;  | prevent mario's speed from
    BCS OKX1                    ;  | exceeding that of the rock
    STA $7B                     ; /
OKX1:
    LDA !B6,x                   ; \ 
    BPL NotHittingWallLeft      ;  | if mario is wedged
    LDA $77                     ;  | in between the rock
    AND #%00000010              ;  | and the wall, then
    BEQ NotHittingWallLeft      ;  | bounce off of him
    JSR HitWall                 ;  | (so not to kill him)
NotHittingWallLeft:             ; /
NoLeftPush:
        BRA No_NoRightPushJMP           ; \ this is used when a standard
NoRightPushJMP:    JMP NoRightPush      ; / branch is out of range

No_NoRightPushJMP:
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get top
    ADC.w #!RightPushT-1        ; / boundary for pushing
    CMP $96                     ; compare with mario's Y position
    BCS NoRightPush1            ; don't execute next command if area is below mario
    LDY #$01                    ; set Y register = 1
NoRightPush1:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoRightPushJMP          ; / don't push from right
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_y              ; get sprite's Y position
    CLC                         ; \ offset to get bottom
    ADC.w #!RightPushB          ; / boundary for pushing
    PHY                         ; back up Y
    LDY $187A|!Base2            ; \ 
    BEQ NotYoshi2               ;  | boundary is lower
    CLC                         ;  | if mario is on Yoshi
    ADC.w #$0010                ; /
NotYoshi2:
    LDY $73                     ; \ 
    BNE NotBig2                 ;  | boundary is lower
    LDY $19                     ;  | if mario is ducking
    BEQ NotBig2                 ;  | or if he is not big
    CLC                         ;  |
    ADC.w #$0008                ; /
NotBig2:
    PLY                         ; load backed up Y
    CMP $96                     ; compare with mario's Y position
    BCC NoRightPush2            ; don't execute next command if area is above mario
    LDY #$01                    ; set Y register = 1
NoRightPush2:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoRightPush             ; / don't push from right
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get right
    ADC.w #!RightPushX          ; / boundary for pushing
    BPL CMP5                    ; \ if area goes backward past
    LDA.w #$0000                ; / level start then assume zero
CMP5:
    CMP $94                     ; compare with sprite's X position
    BCC NoRightPush3            ; don't execute next command if area is before mario
    LDY #$01                    ; set Y register = 1
NoRightPush3:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoRightPush             ; / don't push from right
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDY #$00                    ; Y register = 0
    LDA !scratch_x              ; get sprite's X position
    CLC                         ; \ offset to get left
    ADC.w #!RightPushW          ; / boundary for pushing
    BPL CMP6                    ; \ if area goes backward past
    LDA.w #$0000                ; / level start the assume zero
CMP6:
    CMP $94                     ; compare with sprite's X position
    BCS NoRightPush4            ; don't execute next command if area is before mario
    LDY #$01                    ; set Y register = 1
NoRightPush4:
    PLP                         ; load backed up processor bits
    CPY #$00                    ; \ if Y is not set then
    BEQ NoRightPush             ; / don't push from right
    PHP                         ; back up processor bits
    REP #%00100000              ; set 16 bit A/math
    LDA !scratch_x              ; \ 
    CLC                         ;  | keep mario at
    ADC.w #!RightPushX          ;  | push offset position
    STA $94                     ; /
    PLP                         ; load backed up processor bits
    LDA $14                     ; \ 
    AND #%00000001              ;  | "slowly" increase
    BNE NoIncSpeedR             ;  | speed of rock
    DEC !B6,x                   ;  |
NoIncSpeedR:                    ; /
    LDA !B6,x                   ; \ 
    CMP $7B                     ;  | prevent mario's speed from
    BCC OKX2                    ;  | exceeding that of the rock
    STA $7B                     ; /
OKX2:
    LDA !B6,x                   ; \ 
    BMI NotHittingWallRight     ;  | if mario is wedged
    LDA $77                     ;  | in between the rock
    AND #%00000001              ;  | and the wall then
    BEQ NotHittingWallRight     ;  | bounce off of him
    JSR HitWall                 ;  | (so not to kill him)
NotHittingWallRight:            ; /
NoRightPush:
    LDA !B6,x                   ; \ don't execute following
    BEQ NotRolling              ; / code if rock is not rolling
    JSR PositionOffsetEnd       ; reverse interaction improvement offset
    JSR KillSprites             ; kill sprites
    JSR PositionOffsetStart     ; re-do interaction improvement offset
    LDA $14                     ; \ 
    AND #%00000111              ;  | make the rock
    BNE EndSlowdown             ;  | slow down
    LDA !B6,x                   ;  |
    BPL Minus                   ;  |
Plus:
    INC !B6,x                   ;  |
    BRA EndSlowdown             ;  |
Minus:
    DEC !B6,x                   ; /
EndSlowdown:
NotRolling:
    LDA !1588,x                 ; \ 
    AND #%00000011              ;  | bounce off of
    BEQ NotHittingWall          ;  | walls
    JSR HitWall                 ; /
NotHittingWall:
    LDA $187A|!Base2            ; \ don't shift
    BEQ NoYoshi2                ; / if not on Yoshi
    LDA $96                     ; \  reverse
    SEC                         ;  | offset Y
    SBC #$10                    ;  | by #$10
    STA $96                     ;  | again to
    LDA $97                     ;  | compensate
    SBC #$00                    ;  | for yoshi
    STA $97                     ; /
NoYoshi2:
    JSR StorePosition           ; store sprite's current position for reference in next frame
    JSR PositionOffsetEnd       ; reverse interaction improvement offset

    JSL $01802A|!BankB          ; update position based on speed values
    JSL $018032|!BankB          ; interact with other sprites
    RTS

;; This temporarily offsets mario's and the sprite's
;; Y positions so rock doesn't have a push glitch
;; when it's at the top of the level

!PerceptionOffset = $40

PositionOffsetStart:
    LDA $96                     ; \ 
    CLC                         ;  | add specified
    ADC.b #!PerceptionOffset    ;  | offset to
    STA $96                     ;  | mario
    LDA $97                     ;  |
    ADC #$00                    ;  |
    STA $97                     ; /
    LDA !D8,x                   ; \ 
    CLC                         ;  | add specified
    ADC.b #!PerceptionOffset    ;  | offset to
    STA !D8,x                   ;  | sprite
    LDA !14D4,x                 ;  |
    ADC #$00                    ;  |
    STA !14D4,x                 ; /
    RTS                         ; \ 

PositionOffsetEnd:
    LDA $96                     ;  | subtract
    SEC                         ;  | specified
    SBC #!PerceptionOffset      ;  | offset from
    STA $96                     ;  | mario
    LDA $97                     ;  |
    SBC #$00                    ;  |
    STA $97                     ; /
    LDA !D8,x                   ; \ 
    SEC                         ;  | subtract
    SBC #!PerceptionOffset      ;  | specified
    STA !D8,x                   ;  | offset from
    LDA !14D4,x                 ;  | sprite
    SBC #$00                    ;  |
    STA !14D4,x                 ; /
    RTS

;; SPRITE KILLER - kills enemies
;; - important note: the reason I only check for "interact with stars/cape/fire/bricks"
;;   is because some sprites with specially programmed shell interaction have this
;;   as the only way to tell if they should be killed.

KillSprites:
    LDY.b #!SprSize                 ; load number of times to go through loop
-   CPY #$00                        ; \ zero? if so,
    BEQ +                           ; / end loop
    DEY                             ; decrease # of times left+get index
    STX $06                         ; \  if sprite is
    CPY $06                         ;  | this sprite
    BEQ -                           ; /  then ignore it
    LDA !14C8,y                     ; \  if sprite is not
    CMP #$08                        ;  | in a "tangible"
    BCC -                           ; /  mode, don't kill
    LDA !167A,y                     ; \  if sprite doesn't
    AND #%00000010                  ;  | interact with stars/cape/fire/bricks
    BNE -                           ; /  don't continue
    JSL $03B69F|!BankB              ; \ 
    PHX                             ;  | if sprite is
    TYX                             ;  | not touching
    JSL $03B6E5|!BankB              ;  | this sprite
    PLX                             ;  | don't continue
    JSL $03B72B|!BankB              ;  |
    BCC -                           ; /
    LDA.b #!SpriteKillSFX           ; \ play kill
    STA.w !SpriteKillBank|!Base2    ; / sound
    LDA !1656,y                     ; \  force sprite
    ORA #%10000000                  ;  | to disappear
    STA !1656,y                     ; /  in smoke
    LDA #$02                        ; \ set sprite into
    STA !14C8,y                     ; / death mode (status=2)
+   RTS

;; subroutine for bouncing
;; I put the code in a subroutine
;; because it is used more than once
;; throughout the main code

HitWall:
    LDA !B6,x                       ; \ decide which way rock is going to
    BPL HitPositive                 ; / determine which handler code to use
HitNegative:
    LDA #$00                        ; \ 
    SEC                             ;  | if speed is negative
    SBC !B6,x                       ;  | (which means left movement)
    LSR A                           ;  | then handle it properly
    STA !B6,x                       ;  |
    INC !sprite_start_offset,x      ;  | .. also alter base position for rock
    BRA EndHit                      ; /

HitPositive:
    LDA !B6,x                       ; \ 
    LSR A                           ;  | if speed is positive
    STA !B6,x                       ;  | (which means right movement)
    LDA #$00                        ;  | then handle it properly
    SEC                             ;  |
    SBC !B6,x                       ;  |
    STA !B6,x                       ;  | .. also alter base position for rock
    DEC !sprite_start_offset,x      ; /
EndHit:
    RTS

!Y_Disp = $F8           ; shift for whole rock

!TilesDrawn = $08       ; \ scratch RAM
!TileScratch = $03      ; / addresses

Frames:
    db $00,$00,$04,$04
    db $08,$08,$0C,$0C
    db $00,$00,$04,$04
    db $08,$08,$0C,$0C

Tilemap:    db $00,$02,$20,$22
X_Disp:     db $F8,$08,$F8,$08
Y_Disp:     db $F8,$F8,$08,$08

Graphics:
    %GetDrawInfo()              ; get info to draw tiles
    LDA !sprite_flip_xy,x       ; \ store flip
    STA $02                     ; / to scratch RAM
    STZ !TilesDrawn             ; zero tiles drawn
    JSR DrawRock                ; draw rock
    JSR SetTiles                ; set tiles / don't draw offscreen
    RTS

DrawRock:
    LDA !sprite_offset,x        ; \ 
    PHY                         ;  | set frame according
    TAY                         ;  | to offset
    LDA Frames,y                ;  |
    PLY                         ; /
    STA !TileScratch            ; store tile into scratch RAM

    PHX                         ; back up X
    LDX #$00                    ; load X with zero
-   CPX #$04                    ; end of loop?
    BNE NoReturnFromLoop        ; if not, then don't end
    BRA ReturnFromLoop          ; if so, end

NoReturnFromLoop:
    LDA $00                     ; get sprite's X position
    PHY                         ; \ 
    LDY $02                     ;  | offset by
    BEQ NoFlip                  ;  | this tile's
    SEC                         ;  | X position
    SBC X_Disp,x                ;  | (add or
    BRA EndFlip                 ;  | subtract

NoFlip:
    CLC                         ;  | depending on
    ADC X_Disp,x                ;  | direction)
EndFlip:
    PLY                         ; /
    STA $0300|!Base2,y          ; set tile's X position
    LDA $01                     ; get sprite's Y position
    PHY                         ; \ 
    LDY $02                     ;  | offset by
    BEQ NoFlip2                 ;  | this tile's
    SEC                         ;  | Y position
    SBC Y_Disp,x                ;  | (add or
    BRA EndFlip2                ;  | subtract

NoFlip2:
    CLC                         ;  | depending on
    ADC Y_Disp,x                ;  | direction)
EndFlip2:
    PLY                         ; /
    CLC                         ; \ rock Y
    ADC #!Y_Disp                ; / offset
    STA $0301|!Base2,y          ; set tile's Y position
    LDA !TileScratch            ; load tile # from scratch RAM
    CLC                         ; \ shift tile right/down
    ADC Tilemap,x               ; / according to which part
    STA $0302|!Base2,y          ; set tile #
    PHX                         ; back up X (index to tile data)
    LDX $15E9|!Base2            ; load X with index to sprite
    LDA !15F6,x                 ; load palette info
    ORA $64                     ; add in priority bits
    PHX                         ; \ 
    LDX $02                     ;  | flip the tile
    BEQ NoFlipXY                ;  | X and Y if
    ORA #%11000000              ;  | address set
NoFlipXY:
    PLX                         ; /
    STA $0303|!Base2,y          ; set extra info
    PLX                         ; load backed up X
    INC !TilesDrawn             ; another tile was drawn
    INY                         ; \ 
    INY                         ;  | index to next slot
    INY                         ;  |
    INY                         ; /
    INX                         ; next tile to draw
    JMP -                       ; loop (BRA is out of range)

ReturnFromLoop:
    PLX                         ; load backed up X
    RTS

SetTiles:
    LDA !TilesDrawn             ; \ don't do it
    BEQ +                       ; / if no tiles
    LDY #$02                    ; #$02 means 16x16
    DEC A                       ; A = # tiles - 1
    JSL $01B7B3|!BankB          ; don't draw if offscreen
+   RTS
