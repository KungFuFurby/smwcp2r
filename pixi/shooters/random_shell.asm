;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RANDOM Shell Shooter, by Yoshicookiezeus
;; Adapted from Davros' Normal Shooter sprite
;;
;; Description: Randomly generates blue, red, and green Koopa shells.
;;
;; Uses first extra bit: NO
;;  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!SOUND_TO_GEN = $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
			PRINT "INIT ",pc              
			PRINT "MAIN ",pc                                    
			PHB                     
			PHK                     
			PLB                     
			JSR SPRITE_CODE_START   
			PLB                     
			RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main bullet bill shooter code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;               

SPRITE_TO_GEN:		db $04,$05,$06,$07

SPRITE_CODE_START:	LDA $17AB,x 		; \ RETURN if it's not time to generate
			BNE RETURN		; /
			LDA #$50		; \ set time till next generation = 50
			STA $17AB,x		; /
			LDA $178B,x		; \ don't generate if off screen vertically
			CMP $1C			;  |
			LDA $1793,x		;  |
			SBC $1D			;  |
			BNE RETURN		; /
			LDA $179B,x		; \ don't generate if off screen horizontally
			CMP $1A			;  |
			LDA $17A3,x		;  |
			SBC $1B			;  |
			BNE RETURN		; / 
			LDA $179B,x		; \ ?? something else related to x position of generator??
			SEC			;  | 
			SBC $1A			;  |
			CLC			;  |
			ADC #$10		;  |
			CMP #$10		;  |
			BCC RETURN		; /
			JSL $82A9DE		; \ get an index to an unused sprite slot, RETURN if all slots full
			BMI RETURN		; / after: Y has index of sprite being generated

GENERATE_SPRITE:
			LDA #$0A		; \ set sprite status for new sprite
			STA !14C8,y		; /

			PHX
			LDA #$03
			JSL RANDOM
			TAX
			LDA SPRITE_TO_GEN,x	; \ set sprite number for new sprite
			PLX

			STA !9E,y		; /
			LDA $179B,x		; \ set x position for new sprite
			STA !E4,y		;  |
			LDA $17A3,x		;  |
			STA !14E0,y		; /
			LDA $178B,x		; \ set y position for new sprite
			SEC			;  | (y position of generator - 1)
			SBC #$01		;  |
			STA $00D8,y		;  |
			LDA $1793,x		;  |
			SBC #$00		;  |
			STA !14D4,y		; /
			PHX			; \ before: X must have index of sprite being generated
			TYX			;  | routine clears *all* old sprite values...
			JSL $87F7D2		;  | ...and loads in new values for the 6 main sprite tables
			PLX			; / 
RETURN:			RTS			; RETURN

RANDOM:			PHX
			PHP
			SEP #$30
			PHA
			JSL $81ACF9		; RANDOM number generation routine
			PLX
			CPX #$FF		;\ Handle glitch if max is FF
			BNE NORMALRT		;|
			LDA $148B		;|
			BRA ENDRANDOM		;/
NORMALRT:		INX			; Amount in plus 1
			LDA $148B		;\
			STA $4202		;| Multiply with hardware regsisters
			STX $4203		;|
			NOP			;|
			NOP			;|
			NOP			;|
			NOP			;/
			LDA $4217
ENDRANDOM:		PLP
			PLX
			RTL