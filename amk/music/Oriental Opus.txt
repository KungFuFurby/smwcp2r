#SPC
{
	#title "Oriental Opus"
	#author "Todd"
	#comment "Oriental Level 4"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 
;r4/
t52 w160
@14 q7f v150 y0 $ED $3D $E1 $EE $00 p0,0
r1^2^4
o4f16g+16a+16>c16
d+2^4<g+16a+16>c16d+16
g+2^4<d+16f16g+16a+16
>c2^4o3a+16>d+16f16g+16
f1
o5f8d+16f16d+16c16<a+16g+16f8d+8f4
o4 @0 y10 $ED $5A $E3 p5,10
(1)[r8f8g+8^16$DD$01$05$AE^16>c8^16$DD$01$05$B3^16<a+8>c8
<a+4g+8^16$DD$01$05$A9^16^2
r8f8g+8^16$DD$01$05$AE^16>c8^16$DD$01$05$B3^16<a+8>c8
<a+4^16$DD$01$05$AC^16a+8>c2
f4f8^16$DD$01$05$B3^16f8^16$DD$01$05$B0^16^4
<a+4g+8^16$DD$01$05$A9^16^2
r8f8g+8^16$DD$01$05$AE^16>c8^16$DD$01$05$B3^16<a+8>c8
<a+8^16$DD$01$05$AC^16f8^16$DD$01$05$A7^16f2]
(1)
[r1]8
(2)[g+2f2^4
g+4f4d+4
f1^1]
;
#1
;r4/
@14 q7f v150 y0 $ED $3D $E1 $EE $00 p0,0
[r1]2^4
o4f16g+16a+16>c16d+2
r4<g+16a+16>c16d+16g+2
r4<d+16f16g+16a+16>c2
r4o3a+16>d+16f16g+16f2
o5c8<a+16>c16<a+16g+16f16d+16c8<g+8f4
o4 @0 y10 $ED $5A $E3 p5,10 $EE $22
(1)
(1)
[r1]8
(2)
;
#2
;r4/
@18 q7f v190 y10 $ED $0C $E3 p5,10
[r1]15
o4(3)[f4f8g+8g+4a+8>c8
<a+4g+8f8f2
f4g+8a+32r32g+32a+32>c4d+4
f4d+4c2
<f4d+8f32r32d+32f32g+4a+4
>c4<a+8>c8<a+2
f4g+8a+8g+4f4
d+4c4f4r4]
[r1]11^4^8^16
(4)[a+16>c2]
;
#3
;r4/
@4 q7f v150 y10 $ED $1C $88 p20,10
[r1]2
o3[f1]5
[r1]8
f2g+2
d+2a+2
f2g+2
d+2>c2
<f2g+2
d+2a+2
f2g+2
>c2f2
r8c8f8g8g+8>c8<g8g+8
g4f8d+8c+2
r8c8f8g8g+8>c8<g8g+8
g4g+8a+8g2
>c4c8c8c8<g+8g+4
g8d+8f8d+8d2
r8c+8f8g8g+8>c8<g8g+8
g8f8c+8c8c2
g+1^1
d+1
d1
;
#5
;r4/
@4 q7f v150 y10 $ED $1C $88 p20,10
[r1]15
o3c2d+2
<a+2>f2
c2d+2
<a+2>g+2
c2d+2
<a+2>f2
c2d+2
f2>c2
r8f8g+8a+8>c8d+8<a+8>c8
<a+4g+8g8f2
r8f8g+8a+8>c8d+8<a+8>c8
<a+4>c8c+8c2
f4f8d+8f8c8c4
<a+8g8g+8g8f2
r8f8g+8a+8>c8d+8<a+8>c8
<a+8g+8g8e8f2
f1^1
<a+1^1
;
#7
;r4/
@18 q7f v150 y10 $ED $1C $88 p20,10
[r1]23^8
o3g+8>c8d+8f8g+8d+8f8
d+4c8c8<a+2
r8g+8>c8d+8f8g+8d+8f8
d+4f8g8f4e4
g+4g+8g+8g+8f8f4
d+8c8c+8c8<a+2
r8g+8>c+8d+8f8g+8d+8f8
d+8c+8<a+8g8g+2
>c1^1
f1^1
;
#4
;r4/
(80)[ @26f8 @26f16 @26f16 @26f8 @26f16 @26f16 @26f16^32r32v150 @26c32v180 @26c32v210 @26c32 @26c32v240 @26f8 @26f8]15
@18 q7f v200 y10 $ED $0C $E3 p5,10 $EE $22
;[r1]15
o4(3)
[r1]11^4^8^16
(4)
;

#6
;v0c4/
$EF $FF $46 $46
$F1 $02 $32 $01
 q7f v240 y10
o3v240 				;[ @26 f8 @26f16 @26f16 @26f8 @26f16 @26f16 @26f16^32r32v150 @26c32v180 @26c32v210 @26c32 @26c32v240 @26f8 @26f8]23
[r1]15
(80)8
[ @26f1]3
 @26f2^4v150 @26c16v180 @26c16v210 @26c16 @26c16
v240[f1]3
 @26f4 @26f4 @26f2
 @26f2^4 @26f4
[ @26f2]2
[ @26f1]2
;                

#amk=1
