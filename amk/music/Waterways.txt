;An SMW Central Production II - "Water" - SNN

#SPC
{
	#title "Waterways"
	#author "S.N.N."
	#comment "Water/Reef Level 1"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

w160 
$EF $FF $34 $34 $F1 $07 $6A $01

#0 t42

o3 v200
(1)[@22a+2]14
v190 @22a+4
v170 @22a+4
v150 @22a+4
v130 @22a+4
v110 @22a+4
v90  @22a+4
v70  @22a+4
v50  @22a+4
[v200 @22 a+8 v100 @22 a+8 v130 @22 a+8 v165 @22 a+8]16

[v200 a+8 v130 a+8 v190 a+8 v120 a+8
v200 a+8 v190 a+8 v150 a+8 v100 a+8
v200 a+8 v130 a+8 v120 a+8 v130 a+8
v200 a+8 v190 a+8 v150 a+8 v100 a+8]4

v200 a+8 v190 a+8 v180 a+8 v170 a+8
v160 a+8 v150 a+8 v130 a+8 v100 a+8
(1)8

#1

r2o3 
(10)[@23 v242 a+1]7
r1^2 v225
[@21c2@21c2
@21c4@21c4@21c2]4
(11)[@29 v255 c+2 v225 @10 e2]1
(12)[@21 c2 @10 e2]2
@21 c2 @10 e4 @10 e8 @10 e16 @10 e16
(11)
(12)3
@21 c2 @10 e4 @10 e8 @10 e16 @10 e16
r2
(10)4

#2

[r1]4 @0
r2 $ED $5E $EA v155
o6d16<a16f16d16a16f16d16c16
v150 d16 v130 d16 v110 d16 v100 d16 v90 d16 v80 d16 v70 d16 v60 d16r1^1^2
v150
>d16<a16f16d16a16f16d16c16f16d16c16<a16>d16c16<a16g16
o5
v115 @9 $ED $4D $EE
(20)[d8d16f16d16e16d8d8r4^8]1
c8c16e16c16d16c8c8r4^8
(20)
e8e16f16g16f16e8c8c16d16e16d16c16r16
(20)
c8c16e16c16d16c8c16r16c16e16c16c+16c+8
d1
>d16<a16f16d16a16f16d16c16
v120 d16 v110 d16 v100 d16 v90 d16 v80 d16 v70 d16 v60 d16 v50 d16

v120
(80)[y7 d16r16< y9 a16r16 y11 d16r16 y13 a16r16>]2

(81)[y7 g16r16 y9 d16r16< y11 a16r16> y13 d16r16]2

[y7 f16r16 y9 c16r16< y11 f16r16> y13 c16r16]2

(80)4

(81)4

[y7 <a16r16> y9 c16r16 y11 d16r16 y13 g16r16]4

v110 y7 <a16r16> y9 c16r16 y11 v100 d16r16 y13 g16r16
v90 y7 <a16r16> y9 c16r16 y11 v80 d16r16 y13 g16r16
v70 y7 <a16r16> y9 c16r16 y11 v60 d16r16 y13 g16r16
v50 y7 <a16r16> y9 c16r16 y11 v40 d16r16 y13 g16r16
y10
[r1]10

#3

[r1]9
@11 $ED $04 $ED v190
o3d1
c1
d1
f1
d1
c2c+2
d1^1

@11 $ED $09 $ED v170
o4
(35)[d4^8f4^8d4
c4^8e4^8c4
d4^8f4^8]1

g4
f4^8g4^8d4

(35)

d4
d1^1
[r1]3

#7

[r1]9
@11 $ED $04 $ED v170
o3a1
g1
a1
>c1
<a1
g2g+2
a1^1

@11 $ED $04 $ED v190
o3
a4^8>c4^8<a4
g4^8b4^8g4
a4^8>c4^8d4
c4^8d4^8<a4
a4^8>c4^8<a4
g4^8b4^8g4
a4^8>c4^8<a4
a1^1
[r1]10

#5

[r1]9 @8 $ED $7F $ED v250
<d1
f2g4c4
d1
f4f4c4c+4
d1
f2c4c+4
d1^1
(50)[d4^8f4^8d4
c4^8e4^8c4]1
d4^8f4^8g4
a4^8f4^8d4
(50)
d8^16f8^16d8d4c8c+8
d1^1
[r1]10

#6

[r1]9 y9
@13 $ED $4A $ED v225 p10,94 o4
(60)[d8r4^8g8r4^8f8]1 r4^8g8r4^8
(60) r4^8g8r4^8
(60) r4^8
f+8r4^8
[g8r4^8]2
g1
o5 y10
@13 $ED $4A $ED v225 p10,94
d4^8f4^8a4
g4^8c4^8d4
g4^8f4^8e4
c4^8d4^8e4
d4^8f4^8a4
g4^8c4^8d4
f8^16e8^16f8g4c8c+8
d1^1
[r1]10

#4

@3
o4
r2^4  
y9 d4 y11 g2^4
y9 d4 y11 f2^4
y9 d4 y11 g2^4
y9 d4 y11 c2^4
y9 d4 y11 g2^4
y9 d4 y11 f2^4
y9 g4 y11 d2^4r1^4
@13 $ED $4A $ED v225 p10,94

y10
a8r4^8>d8r4^8
c8r4^8d8r4^8
<a8r4^8>c8r4^8
c8r4^8d8r4^8
<a8r4^8>d8r4^8
c8r4^8c+8r4^8
d8r4^8d8r4^8
d1
o4g1
a1
a1
f1
g1
a1
>c2d4<g4
a1^1<
[r1]10                

#amk=1
