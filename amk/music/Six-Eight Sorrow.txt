#amk 2
#SPC
{
        #title "Six Eight Sorrow"
	#author "TrapFanatic"
	#game "SMW Central Production 2"
	#comment "Abstract/Dream Level 3"
        #length "4:00"
}
#samples
{
#default
"bells.brr"
"wind.brr"
}
#instruments
{
@8   $BF $F2 $B8 $1E $00     ;@30 bass
@0   $F7 $E7 $B8 $06 $00     ;@31 flute
"bells.brr" $FF $E9 $B8 $04 $4E      ;@32 bell
@13    $BE $6F $00 $03 $00    ;@33 harp
@1     $86 $A9 $B8 $03 $00    ;@34 strings 1
@6     $85 $E7 $CF $03 $00    ;@35 strings 2
@2     $8E $EF $CF $01 $7f    ;@36 crystal
"wind.brr" $ff $e0 $00 $03 $7f;@37 wind
@3     $F7 $EF $CF $06 $00    ;@38 whistle
@9     $Ff $f1 $CF $02 $a6    ;@39 piano
} 
$ef$ff$20$20
$f1$04$60$01

#0 @30y11v200 w200 t46
o3
(1)[r8g+16r16g+16r16]
r8a16r16a16r16
(1)
(2)[r8f+16r16f+16r16]
(1)
r8a16r16a16r16
d+8e8d+8c+8<a4
/>(1)2
(2)2
(3)[r8e16r16e16r16]3
(4)[r8d+16r16d+16r16]
(1)2
(2)2
(1)4
(3)2
(2)
(4)
(1)4
(3)2
(2)
(4)
r8a16r16a16r16
(1)
(2)
(4)
(1)2
(2)2
(3)3
(4)
(1)2
(2)2
(1)4
(3)2
(2)
(4)
(1)4
(3)2
(2)
(4)5

;bridge

(1)
r8a16r16a16r16
(1)
(2)
(1)
r8a16r16a16r16
(3)
(4)
(1)
r8a16r16a16r16
(1)
(2)
(1)
r8a16r16a16r16
d+8e8d+8c+8<a4

;extension1
o3
(1)3
r8a16r16a16r16
(2)4
(1)3
(2)
(4)2
<r8b16r16b16r16
r8b16r16b16r16r1
r1
r1
@38v160y8o3p16,62
g+2.f+4.
e4.d+4c+1^4
g+2^8.a16b4
a4f+4g+2.
g+2.
>c+2.
@30p0,0y11v180
o3(1)2
r2.
(1)2
r2.
v200(1)
(2)
r2.
d+8e8d+8c+8<a4;

#1 @30 y10 v220
p4,32
o3
c+4.c+4.
c+4.<b4.>
c+4.c+4.
<d+8e8d+8c+8<a4
/o3c+4.c+4.
<b4.b4.
a4.a4.
a4.g+4.
>c+4.c+4.
<b4.b4.
>c+4.c+4.
c+4.c+4.
<a4.a4.
b4.g+4.
>c+4.c+4.
c+4.c+4.
<a4.a4.
b4.g+4.
>c+4.c+4.
<b4.g+4.
>c+4.c+4.
<b4.b4.
a4.a4.
a4.g+4.
>c+4.c+4.
<b4.b4.
>c+4.c+4.
c+4.c+4.
<a4.a4.
b4.g+4.
>c+4.c+4.
c+4.c+4.
<a4.a4.
b4.g+4.
g+4.g+4.
g+4.g+4.

;bridge

>c+4.c+4.
c+4.<b4.>
c+4.c+4.
<a4.g+4.
>c+4.c+4.
c+4.<b4.>
c+4.c+4.
<d+8e8d+8c+8<a4

;extension1

>>
c+4.c+4.
c+4.<a4.
b4.b4.
b4.b4.
>c+4.c+4.
c+4.<b4.
g+4.g+4.
f+4.f+4.
v180g+2.g+2.
g+2.g+2.
>(5)[y10c+8y11g+16r16g+16r16]
y10c+4.
c+8y11a16r16a16r16
y10c+4.
<b8>y11f+16r16f+16r16
y10<b4.
>(5)3
c+4.
c+8y11a16r16a16r16
y10c+4.
<b8>y11f+16r16f+16r16
y10<b4.
>(5)2
y10
[c+4.]8
v200c+4.c+4.
c+4.<b4.
>c+4.c+4.
<d+8e8d+8c+8<a4;

#2 @37 y10 v100
o2[$DC$C0$03c1$DC$C0$0A^1$DC$C0$10^1]100

#3 
r1
r1
r1
/@31y10v155o5p12,70 e2.d+4.
<b4.g+1.
>e2^8..e32f+4.
d+4.d+8e8d+8c+1^8
e2.f+4.
d+4.d+4e16d+16c+1^8
e2.f+4
e4f+4g+8a16g+8a16g+8
a16g+8a16g+8f+8e8d+8c+8<b8
@32p0,0
<e2.^2.
<g+2.^2.
>e2.^2.
d+2.^2.
c+2.^2.
c+2.^2.
c+2.d+2.
c+2.g+2.

;bridge

c+1.r1.
c+1.r1.

v140@36o5y12p16,62c+4.g+4.f+4
g+8a4.<f+4.>f+4.
e2.
c+4.g+4.f+4
g+8a4e8d+4.<g+4.
b4.g+4.
c+2.r1
r1
r4
@38v160
[g+4.e8c+8g+8a4b4e4f+4.c+8<a8>f+8g+2.]2
>c+4<b8>c+4<g+8b4
a4f+4c+2.
[r2]100;

#4 
r1
r1
r1
/r8p0,0@33v190
o4
[y8f+4>y13c+4<y8f+4]3
y11g+4>c4<y6c4
*
f+4y13b4y8f+4
*
y13>c4y8<e4c8
r16
y8<g+8b8>d+8.d+4^16
y12<a8>c+8f+8.f+4
y10d+4d+2...^4.
y8<g+8b8>d+8.d+4^16
y12<a8>c+8f+8.f+8.g+8.
y13g+4
y11g+4e4
y9e4
y7e4<g+8

p16,32@34y8v205>>e2.d+4.
<b4.g+2.....^4...
>d+32.e2^8
d+16e16f+4e4
d+4d+8e8d+8c+1^8
c+4
c+16d+16e4.d+4e16
f+16a4.g+4a8g+4
a8g+8a8g+8f+8e8d+8c+4
c+16d+16e4.d+4e8
f+4.d+2^8..
f+32g+2.

;bridge

v140@36p16,62o4
y10c+2.y8a4.f+4.
g+2.e2.
>y12g+2.a4.f+4.g+2.e4.d+4.

;extension 1
p0,0@33v190
o3
y8g+4>c+4y12g+4
y8<g+4>y12c+4r4
y8<f+4b4>y12f+4
y8<f+4>y12d+4f+4
y8<g+4>c+4y12g+4
y8<g+4>y12c+4f+4
y8<d+4g+4>y12d+4
y8<d+4y12g+4f+4
@34o5v70p16,70$ED$02$E9y9g+1^1^1
>c+1^1^1
c+1^1^1
<g+1^1^1^1^1^1;

#5
r1
r1
r1
/p0,0@33v190
o4
[y6c+4y11g+4g+4]3
>y13c+4y8<c+4g+4
*
y6<b4>y11g+4g+4
*
y13>c+4y10<g+4y6d+4

y8c+8<a8>c+8e4e8
y12f+8<b8>d+8g+4e4
y8e4y12c+1^8

y8c+8<a8>c+8e4e8
y12f+8<b8>d+8g+4f+8

y13a4f+4
y11f+4
y9f+4d+4
y7d+4
p16,32@34y12v200g+2.f+4.
e4.d+4.c+1^8
g+2.
b4a4
g+4g+4f+8g+1^8
e2^8..
f+2.
r32d+4.c+1^8
e2.
f+2.
c+2.
c4.d+4.

;bridge
c+1$E8$FF$00^1r1r1r1r1

;extension
o4p0,0@33v190
r8
y8g+4y12<g+4>c+4
y8g+4a4.r8
y8f+4y12<f+4b4
y8b4>g+4y12e4
y8g+4y12<g+4>c+4
y8g+4<f+4y12b4
y8>d+4y12<d+4g+4
y8b4a4y12e8
@34o6v70p16,70$ED$02$E9y11c+1^1^1
g+1^1^1
g+1^1^1
c+1^1^1^1^1^1;

#6 
r1r1r1
/o2@35y9v180p8,40c+2.
<b2.
a1^8
g+4.
>c+2.
<b2.
>c+1.
<a2.
b4.g+4.
>c+1.
<a2.b4.g+4.
>c+2.<b4.g+4.
>c+2.
<b2.
a2.
a4.g+4.
>c+2.
<b2.
>c+1.
<a2.
b4.g+4.
>c+1.
<a2.
b4.g+4.
g+1.

;bridge
r1.
r1.
>c+1^1^1
;extension

c+1^8
<a4.b1.
>c+1^8
<a4.g+1.
g+1^1^1
>c+1^1^1
c+1^1^1
<g+1^1^1
>c+1^8<b4.>c+1.;

#7 
r1r1r1/@39y7v179
[r2]21o3a4.g+4.f+4.e8d+8c+8
g+2.f+4.
e4.d+8<g+8b8>c+1^8
g+2.b4
a4g+4g+4f+8g+1^8
e2.f+8e8
f+8g+8f+8e8d+4.c+1^8
e2.f+4
r2c+2.
c4.d+4.

;bridge

c+1.
c+2.
e4.d+4.
c+1.
c+2.
d+8e8d+8c+8<a4

;extension

>c+4.<g+4.
b4.a4.
b4.b4.
b8>f+8f+8g+8f+8d+8
c+4.<g+4.
b4.a4.
g+4.>d+4.
<b8g+8f+8g+8f+8e8
[c+8g+8g+8>c+2.<f+8g+8e8]6
>[<c+8g+8g+8>c+2.r4.]2[r2]50
