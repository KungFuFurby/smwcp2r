;Revised by Pink Gold Peach

#SPC
{
	#title "Fun at the Carnival"
	#author "MidiGuy"
	#comment "Carnival Level 2"
	#game "SMW Central Production 2"
}
#halvetempo
#samples
{
#default
"undeserved/snare.brr"
"tom.brr"
"CCymbal.brr"
}
#instruments
{
@8  $BF $F1 $B8 $1E $00                 ;@30 bass
@13 $8F $F3 $7F $03 $00                 ;@31 piano
@5  $BF $F1 $00 $07 $00                 ;@32 pizzicato
@3  $9F $48 $B8 $03 $00                 ;@33 xylophone
@6  $DA $60 $00 $03 $00                 ;@34 trump
"undeserved/snare.brr" $00 $00 $50 $0f $00 ;@35
"tom.brr"   $FF $F6 $00 $07 $00         ;@36
"CCymbal.brr"	$AF $51 $00 $03 $C0     ;@37
}
$EF$FE$1F$1F
$F1$06$30$00


#0 t52 w215

@30o2v255y10l8q7F
p0,0
[er<b>r]3e<b>c+d+/@30o2v255y10l8q7F
p0,0
[er<br>d+r<br>c+r<g+rbrf+r
ar>arg+rer<f+ra+rbr>b<b>
er<br>d+r<br>c+r<g+rbrf+r
ar>arbr<br>er<br>e]1<b>c+d+
*rf+g
[g+rd+rg+rc4
c+rg+r>c+r<f4
f+rc+r]1f+ra+4
brf+rbrf+g
*a+rf+r<b16r16rr2>b<b>
[er<br>d+r<br]1>c+r<g+rbrf+r
ar>arg+rer<f+ra+rbr>b<b>
*<arbr>c+r>c+<c+
l16
@30y10o2v255e8
@33y7o3v190bg+
@30y10o1v255b8
@33y7o3v190g+e
@30y10o2v255e8
@33y7o3v190e<b
@30y10o1v255b8
@33y7o3v190b>e
@30y10o2v255l8bag+f+ed+c+<b
l16
@30y10o2v255e8
@33y7o3v190g+e
@30y10o1v255b8
@33y7o3v190e<b
@30y10o2v255e8
@33y7v190bg+
@30y10o1v255b8
@33y7o2v190g+e
@30y10v255e8
@33y7v190br
@30y10o1v255l8
brbrbr
@37o3b1r4r2[r1]3

#1r1r1/
@32l8o4v245y9q2F
p10,20
[e<b>eg+f+4d+4ed+c+r<b2>
c+<a>c+ed+4<b4>c+<a>g+rf+2
e<b>eg+f+4d+4ed+c+rb4eq7F
e&>c+16r16q2F
<bag+f+4d+4e2]1r2
*ref+gq7F
[g+rd+rg+r>c^16^16&q2F
c+<bag+f+ed+c+q7F
f+r]1c+rf+ra+^16^16&q2F
bag+f+ed+c+q7F
g*q2F
f+g+q7F
a+rf+rl16b8

@33v230y8
c+dd+rc+r<b>rd+f+br

@32v245y9l8<b>q2F
e<b>eg+b4q7F
eb16>c16q2F
c+<bag+b4rq7F
e&>c+16r16q2F
c+d+e<b4q7F
ef+16g+16q2F
ag+f+ed+c+<b>d+
e<b>eg+b4e^16q7F
l16e&>c+8r<f+&>d+8r<g+&>e4r8<b8&>g+2e2
f+2d+^4^8ee1r2^4<b8^8&>e8[r4]15<<v235b8

#2@31o4v230y8l8q2F
p0,0
[rg+]6<rbrb/$E5$00$00$00 @31o4v230y8l8q2F
p0,0
[rg+rg+rf+rf+rererd+rd+
rererererf+rf+q7F
d+eff+q2F
rg+rg+rf+rf+rererd+rd+
rerg+rf+rd+rg+reg+]1q7F
g+ag+q2F
*q7F
g+f+gq2F
[rd+rd+rd+q7F
rg+q2F
rerereq7F
rfq2F
rc+rc+]1rc+q7F
rf+q2F
rd+rd+rd+q7F
rd+q2F
*rf+rf+q7F
l16f+bc+d+f+d+dd+f+c+d+f+bp10,20
@34$E5$00$03$20y9v200d+ef+l4
b^2>cc+2<g+ba>c+<g+b>c+dd+2
e2d+<b>ef+g+c+8l16<ef+b2g+2
@34y9v200o4b8
@33y7v190<bf+>
@34y9v200b8
@33y7v190<f+d+>
@34y9v200b8
@33y7v190<d+<b>>
@34y9v200b8
@33y7v190<b>d+>
@34y9v200<b1>e8rr
@34y8v210<g+4a4b4>e8[r1]4

#3@31o4v230y12l8q2F
p0,0
[re]8/@31o4v230y12l8q2F
p0,0
[rererd+rd+rc+rc+r<brb>
rc+rc+r<brbr>c+<ra+q7F
b>c+dd+q2F
rererd+rd+rc+rc+r<brb>
rc+rerd+r<b>rer<b>e]1q7F
ef+eq2F
*q7F
ec+dq2F
[rcrcrcq7F
rd+q2F
rc+rc+rc+q7F
rc+<q2F
ra+ra+]1ra+>q7F
rc+<q2F
rbrbrbq7F
rbq2F
*>rc+rc+q7F
l16d+<rf+bf+rbf+<b>rf+bf+r
@33y12<v190l8q3F
b>reb>ed+r<brrc+g+>c+<bf+d+<b
ra>ear<g+b>g+r<a>aa+bag+f+
e<b>eg+r<bf+ba>c+<f+b>c+q7F
l16ed+c+rg+r
@33y13o4v190g+e
@31y12v230er
@33y13v190e<b
@31y12v230o4g+r
@33y13o3v190bg+
@31y12v230o4er
@33y13o3v190bg+
@31y12v230o4g+r
@33y13v190f+d+
@31y12v230f+r
@33y13v190d+<b
@31y12v230o4br
@33y13o3v190bf+
@31y12v230o4f+r
@33y13o3v190d+f+
@31y12v230o4d+r
@33y13v190e<b
@31y12v230o4er
@33y13o3v190bg+
@31y12v230o4g+r
@33y13o3v190g+e
@31y12v230o4er
@33y13o3v190e<b
@31y12v230o4g+r
@33y13o3v190er
@31y12v230o4er

@34v210e4e4f+4b8[r1]4

#4r1r1/@32l8o4v80y12q2F
p10,20
r16^32[e<b>eg+f+4d+4ed+c+r<b2>
c+<a>c+ed+4<b4>c+<a>g+rf+2
e<b>eg+f+4d+4ed+c+rb4eq7F
e&>c+16r16q2F
<bag+f+4d+4e2]1r2
*ref+gq7F
[g+rd+rg+r>c^16^16&q2F
c+<bag+f+ed+c+q7F
f+r]1c+rf+ra+^16^16&q2F
bag+f+ed+c+q7F
g*q2F
f+g+q7F
a+rf+rb8[r4]3<b>q2F
e<b>eg+b4q7F
eb16>c16q2F
c+<bag+b4rq7F
e&>c+16r16q2F
c+d+e<b4q7F
ef+16g+16q2F
ag+f+ed+c+<b>d+
e<b>eg+b4e^16q7F
l16e&>c+8r<f+&>d+8r<g+&>e4r8<b8&>g+2e2
f+2d+^4^8ee1r2^4<b8^8&>e8[r4]16                

#5y10l8
$FA$03$30
p0,0
v245[o3@21c@35c]5@21c@35c

@36v200y6by8gy11ey14c/

[o3y10v245@21c@35c]13@21c@35c

@36v200y6b16y7b16y9gy12ey14c

*14@21c8

@36v200y7b16b16y10g16g16y13e

*13@21c@35c8

@36v200y6b16y7b16y9gy12ey14c

*14@21c8

@36v200y7b16b16y10g16g16y13e

*6@21c@35c8

@36v200y8ay12f

*5@21c@35c8

@36v200y6by8gy11ey15c

*6@21c@35c8

@36v200y8ay12f

*3@21c@35v160c16v200c16v245cv120c32v155c32v180c32v215c32l16v245ccrv200cv245crcrc4l8

*13@21c@35c8

@36v200y6b16y7b16y9gy12ey14c

*3l16@21c8@35cv180cv245@21c8@35cc@21c8@35cc@21c8@35cv200cv245cv175cv215cv230c*6crcrv170cv190cv210cv230c*4@21c8

@36v200y8b16y12f16l8

y10v245
@35cv150cv210cv160cv230cv190cl16
[v245c8v120c32v155c32v180c32v215c32
v245crcv190cv245
v245c8v120c32v155c32v180c32v215c32
v245crcv190cv245
v245c8v120c32v155c32v180c32v215c32
v245cv180crcv245crcv180cv245cr]1
v180cv210c*r8

#6r1r1/@34l16o4v200y9q7F
p10,20
[r1]7r2r4c+d+ef+l4
$E5$00$03$20 b^2>c
c+2<g+ba>c+<g+b>c+dd+2
e2d+<b>c+2d+<g+a>c+<b>d+e2r2[r1]8

$E5$00$00$00

@31o4v230y12l8q2F
p0,0
rere[rd+]2
rere<rbrb>
[rc+]2<rbrb>
rc+r<a+q7F
b>c+dd+q2F
rere[rd+]2
rc+<rb>rc+q7F
c+d+

$E5$00$04$20
@34y11v200>p10,20
e2<b2>d+2<f+2>e1<br

v170<
y10b4>c+4d+4e8[r1]4


#7r1r1/
[r2]48
@31y8o4v230l8q2F
p0,0
[rg+]2[rf+]2
[rg+]2[rd+]2
[re]4[rf+]2q7F
d+eff+q2F
[rg+]2[rf+]2
rerd+req7F
ef+q2F
[r<b>re]2
rd+rf+rd+r<b>*2r<bq7F

@15v225y10o3g+4a4b4>e1[r1]4


#amk=1
