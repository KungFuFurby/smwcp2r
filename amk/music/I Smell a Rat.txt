#SPC
{
	#author "Ruberjig"
	#comment "Intro cutscene theme"
	#game "SMW Central Production 2"
	#title "I Smell a Rat"
}


$EF $FF $14 $14 ; reverb
$F1 $07 $a0 $00	;


;;;;;;;;;;;;;;;;;;;
;cdefgab>c   key
;;;;;;;;;;;;;;;;;;;;
#0 w160	$de $18 $0c $40 t60 v180


(1)[
@0 $ed $0E $58 o3 l24
cdef gab>c defed c<bag fedc8r
cdef gab>c  defg ab>cd efga>cr8
@1 $ed $0c $50 o4 l8
g12g24g24g12g12g12g12g2
a12a24a24a12a12a12a12a2
b12b24b24b12b12b12b12b2>

$ed $0c $50
c12^24r24c12<a12g12a12

$ed $0c $4C t54
>c1<]
/
(2)[@1 $ed $0F $33 o4 t54 l8 $de $18 $0c $40
r^4cgfedc4 de4 d2.
<b>fedc<b4> cd4 c2.
cgfedc4 de4 a2
<a>c4<a4>cd4 e16d16c1


@0 $ed $0C $68 o4
rcgfedc8..<b32> d&e4 d2.
<b>fedc<b8..a32> c&d4 c2.
cgfedc8..<b32> d&e4 a2
<a>c4<a8..b32>c&d8..c32 e16d16c^2

@6 $ed $0D $4C o4 t54
fab>
c^2<
fab>
c.<b.a> c.<b.a
b^1... >c16<b16

a^2
agf
c.d.e d.e.f
;;;;;;;;;;;;;;;;;;;;;;;
;c-d-e- fg-ga->c-r1
;;;;;;;;;;;;;;;;;;;;;;;;
g-.f.e- g.g-.f 
a-.g.g- >c-<a->c-e-g1^1]



#1
@10 o3 l24
[q7f e q7d e e q7b e e q7a e]21
e12 e12 e12
e4.

l8

@12o5
e24 @12o5e24@12o5e24 
[@12o5e]4
@12o5e4
/


 o3
[@21e4@21e@12o5e@21e@12o5e@22e@21e]25
@21e4@21e@12o5e@21e@12o5e
#2 $de $18 $0c $40 v120


(21)[
@0 $ed $0E $58 o3 l24 r8
cdef gab>c defed c<bag fedc8r
cdef gab>c  defg ab>cd efga>c
@1 $ed $0c $50 o3 l8 v180
g12g24g24g12g12g12g12g2
a12a24a24a12a12a12a12a2
b12b24b24b12b12b12b12b2>

$ed $0c $50
c12^24r24c12<a12g12a12

$ed $0c $4C t54
>c1<]
/
(22)[@1 $ed $0F $33 o3 l8 $de $18 $0c $40
r^4cgfedc4 de4 d2.
<b>fedc<b4> cd4 c2.
cgfedc4 de4 a2
<a>c4<a4>cd4 e16d16c1


@0 $ed $0C $68 o3
rcgfedc8..<b32> d&e4 d2.
<b>fedc<b8..a32> c&d4 c2.
cgfedc8..<b32> d&e4 a2
<a>c4<a8..b32>c&d8..c32 e16d16c^2

@6 $ed $0D $4C o3 t54
fab>
c^2<
fab>
c.<b.a> c.<b.a
b^1... >c16<b16

a^2
agf
c.d.e d.e.f
;;;;;;;;;;;;;;;;;;;;;;;
;c-d-e- fg-ga->c-r1
;;;;;;;;;;;;;;;;;;;;;;;;
g-.f.e- g.g-.f 
a-.g.g- >c-<a->c-e-g1^1]



#3 v200


(31)[
@1 $ed $01 $64 o4
c1^1
@1 $ed $0c $68 o4 l8
g12g24g24g12g12g12g12g2
a12a24a24a12a12a12a12a2
b12b24b24b12b12b12b12b2>
c12^24r24c12<a12g12a12

$ed $0c $4C 
>c1<]
/
(32)[@5 $ed $0E $2E o5 l8 v160
r^4cg4e4c4 d4. d2.
<b>f4d4<b4> c4. c2.
cg4e4c4 d4. a2
<a>c4<a4>d4.ec1


@1 $ed $0E $2E o5
rcg4e4c4 d4. d2.
<b>f4d4<b4> c4. c2.
cg4e4c4 d4. a2
<a>c4<a4>d4.ec^2


@3 $ed $0D $E8 o3 
fab>
l24 ccc q7e ccc q7d ccc q7c ccc q7b ccc q7a ccc q79 ccc q78 ccc q7f l8 
l24 ccc q7e ccc q7d ccc q7c ccc q7f l8 
l24 ccc q7e ccc q7d ccc q7c ccc q7f l8 
<
l24 bbb q7e bbb q7d bbb q7c bbb q7b bbb q7a bbb q79 bbb q78 bbb q7f l8 r2..>c<
l24 aaa q7e aaa q7d aaa q7c aaa q7b aaa q7a aaa q79 aaa q78 aaa q7f l8
c.d.e d.e.f
;;;;;;;;;;;;;;;;;;;;;;;
;c-d-e- fg-ga->c-r1
;;;;;;;;;;;;;;;;;;;;;;;;
g-.f.e- g.g-.f 
a-.g.g- >c-<a->c-e-
l24 ggg q7e ggg q7d ggg q7c ggg q7b ggg q7a ggg q79 ggg q78 ggg q77 ggg q76 ggg q75 ggg q74 ggg q73 ggg q72 ggg q71 ggg q7f l8 
r1]

#4 v120


@1 $ed $01 $64 o5
c1^1
@1 $ed $0c $68 o5 l8
g12g24g24g12g12g12g12g2
a12a24a24a12a12a12a12a2
b12b24b24b12b12b12b12b2>
c12^24r24c12<a12g12a12

$ed $0c $4C 
>c1<
/
@8 $ed $0E $D4 o3 l8 v140
r4
[g4cgrg4. g4cgrg4. e4<a>ere4. e4<a>ere4.  g4cgrg4. a4dara4. >c4<f>crc4.< g4cgrg4. ]2
[da]8
[cg]8
[da]8
cg16g16 cg16g16 da16a16 da16a16 e>c16c16< e>c16c16< geg>c<
[f>c16c16<]8

#5 v180


@1 $ed $01 $64 o2
c1^1
@4 $ed $0D $68 o5 l8 v120
g1
a1
b1>
c4<a6a12

$ed $0c $4C 
>c1<
/
@1 $ed $0E $D4 o2 l8 v220 $ee $18
r4
[g4cgrg4. g4cgrg4. e4<a>ere4. e4<a>ere4.  g4cgrg4. a4dara4. >c4<f>crc4.< g4cgrg4. ]2
[da]8
[cg]8
[da]8
cg16g16 cg16g16 da16a16 da16a16 e>c16c16< e>c16c16< geg>c<
[f>c16c16<]8


#6 v230


@1 $ed $01 $64 o3
c1^1
@1 $ed $0D $68 o2 l8 v220
g1
a1
b1>
c4<a6a12

$ed $0c $4C 
>c1<
/
@6 $ed $0E $D4 o3 l8 v140 $ee $08
r4
[g4cgrg4. g4cgrg4. e4<a>ere4. e4<a>ere4.  g4cgrg4. a4dara4. >c4<f>crc4.< g4cgrg4. ]2
[da]8
[cg]8
[da]8
cg16g16 cg16g16 da16a16 da16a16 e>c16c16< e>c16c16< geg>c<
[f>c16c16<]8

#7 $de $18 $0c $40 $ee $10 r16 v80

(1)
/
r
(2)

#amk 2