#amk 2

#option TempoImmunity
#option NoLoop

#samples
{
	#default
	"shekere.brr"
	"guiro.brr"
	"CPanFlute.brr"
}


#instruments
{
	"shekere.brr"	$00 $00 $58 $04 $C3 ;@30
	"guiro.brr"	$8F $F3 $00 $04 $C3 ;@31
	"CPanFlute.brr"	$AF $AD $00 $04 $00 ;@32
}

w200t50

#0 q7F @3 v200 y10 o3
c+8d16e8 q78e16q7F d16c+16<b8e16a16b8e8
a8. q78a8.q75a8. r4..
/r1

#1 q7F @3 v150 y10 o3
c+8g+16g+8 q78g+16q7F c+8f+8f+8<g+8>d+8
e8. q78e8.q75 e8. r4..
/r1

#2 q7F @8 $ED$3E$50 v220 y14 o3 p13,48
c+8<b16>c+8. <f+8b8b8f+8g+8
a2
/r1

#3 q7F @8 $ED$3E$50 v255 y6,1,1 o2 p13,48
c+8<b16>c+8. <f+8b8b8f+8g+8
a2
/r1

#4 y11,0,1 v110 @30 o4
[[q7Fc8 q78c16q75c16]]4 v120
@31 y9,1,0 q7F <g4 $DD$00$30>d ^4
/r1

#5 q4F @32 v120 y10,1,1 o4
c+8d8 q7F e8 d16c+16< q4F b8 q7F g+16a16 q4F b8g+8
q7F
a4 $E8$60$00 ^4
/r1