;The main melody was made by Bloop. Blind Devil ported Bloop's MIDI by ear, expanded the song and made some minor changes to improve it.
;Revamped by Pinci.


#SPC
{
	#title "Let the Show Begin!"
	#author "Blind Devil & Bloop"
	#comment "Carnival Castle theme"
	#game "SMW Central Production 2"
}
#path "let the show begin"
#samples
{
	#optimized
	#AMM
	"bright.brr"
	"pizzicato.brr"
	"snare.brr"
	"triangle.brr"
	"orchestra.brr"
	"sax.brr"
	"kick.brr"
	"bass.brr"
}
#instruments
{
	"bright.brr" $FF $F3 $B8 $02 $9C 
	"pizzicato.brr" $AF $6E $B8 $09 $F0
	"snare.brr" $7F $00 $70 $06 $00
	"pizzicato.brr" $9F $8C $B8 $09 $F0
	"orchestra.brr" $FF $F3 $B8 $04 $48 
	"triangle.brr" $FF $59 $B8 $0A $A0
	"sax.brr" $FE $E8 $B8 $07 $00
	"triangle.brr" $FF $52 $B8 $0A $A0
	"kick.brr" $FF $E0 $B8 $04 $00
	"bass.brr" $AF $26 $B8 $0F $F0
}

$EF $D7 $30 $D0
$F1 $04 $52 $01

#0  w200 $F4 $02 t46 q7F v220 y8 @31 o2 l8
(1)[ebbea+a+eaaeg+e]
/(1)4
(3)[f+df+ged+f+df+c+dd+]
(1)
(3)
(1)2 @33 
>c^^<a^^
b2^4
(2)[e^e<b^b
a^a>d^d
e^e<b^b]
a^ab^b
(2)
a^ab^^
^2^4
l8^^l16>  $F4 $03 $E3 $E0 $2E v200 @39 $ED $5D $EA ab
[l8>cegdf+g<
ebbebl16ab]3
l8>cegdf+g<
@39bb^a^>c<
b2^4
$ED $5D $EF
(1)
(6)[ae]3
f+<b>f+gg+a
(1)
(4)[>ccc<bbba+a+a+aag+]
(1)
(6)3
f+<b>f+gg+a
(1)
(4)2 v220 @31 $F4 $03
(1)

#1 o4 l16
(11)[r2r4]2
/(11)6
 q7F v180 y12 @1 (14)[ $DF e8 $DD $14 $04 a bebee8 $DE $0C $0C $30 a+8e8
$DF e8 $DD $14 $04 g aeae $DE $0C $0C $30 e8g8e8
f+8d8f+df8e8d+8
f+8d8f+dc+8d8d+8]2
e2^4
^2^4 
(11)2 $DE $18 $11 $30
@4v140o4l8
g^^f+^^
b2^4
l16
(12)[<e8brbr
<b8>brbr
c8>crcr<
<d8>brar
e8brbr
<b8>brbr
c8>crcr<]
<b8>brbr
(12)
<b8>b^^^
^2^4
^^^4
@1 y12 v220 o4 $DE $30 $12 $2D l8
g^^f+^^
g2^4
g^^f+^^
e2^4
g^^f+^^
g2^4
g^^a^^
@4v180o3l8p0,0 $ED $0B $F3
bb^a^>c<
b2^4 
@30 v180 o4 l16
(15)[e8bebee8a+8e8
e8gegee8g+8e8
e8aeaee8g8e8
f+8bf+bf+b8f+8b8
e8bebee8a+8e8
e8gegee8g+8e8l8]
(16)[>c<fdbfda+fdafdl16]
(15)
(16)2
(11)2

#2 q7F v160 o4 l8
(11)2
/(11)18 $DE $18 $11 $30 
@4 y08 v140o4 $E3 $C0 $23
e^^c+^^
d+2^4 $DF 
@30 y07 o5 l16 v150 
[egl8f+eb^l16abl8
>c<bagl16f+el8d+l16
egl8f+eb^l16ab
>c<ba>c<bal8b^^]2
(11)
^^4
(11)2l16
[edef+g^f+^e^d^
e2^4]2
edef+g^f+^g^a^
@34o3v200l8bb^a^>c
rr@36 $DE $18 $11 $30 v175 l16 abagf+ed+f+
(21)[l8e^^^^l16gel8
g $DD $18 $30 $2C ^^^^^
a^^^^l16ge
f+2^4
l8e^^^^l16gel8
g $DD $18 $30 $2C ^^^^^]
(22)[>c<fdbfda+fdafd]
(21)
(22)2
$ED $5A $EC e2^4
^2^4

#3 q7F v220 y10 @25 o4 l16
(11)2
/(11)2
(31)[r8 y10 @10v180cv220@38cc8 $F4 $03 @32<g8> $F4 $03 @10v150cv220@38cc8
r8@10v180cv220@38cc8 $F4 $03 @32<g8> y08 $F4 $03 @37v150cv220 y10 @38cc y08 @35v180c]6
(11)4
@6v200o2l8
c^^<a^^
b2^4
@4v140o3l16
(32)[r8grgr
r8f+rf+r
r8arar
r8grf+r
r8grgr
r8f+rf+r
r8arar]
r8f+rf+r
(32)
r8f+^^^
^2^4
^^^4
(11)2
v0> y10 $E8 $C8 $E6
r8@10c@38cc8 $F4 $03 @32<g8> $F4 $03 @10c@38cc8
r8r@38cc8 $F4 $03 @32<g8> $F4 $03 y08 @37 y08 v150c y10 v220@38cc y08 @35v180c
(31)
r8 y10 @10v180cv220@38cc8 $F4 $03 @32<g8> $F4 $03 @10v150cv220@38cc8<
<@10e8e8 $F4 $03 @32gg $F4 $03 @10e8e8 $F4 $03 @32gg
g2^4> $F4 $03 
(31)3
(33)[ y10 $F4 $03 @32g $F4 $03 @26c^@26c^@26c]4
(31)3
(33)7
 y10 $F4 $03 @32g $F4 $03 @26c^@26c $F4 $03 @32g^
<a2^4
^2 $F4 $03 ^4

#7 q7F v220 @1 o4 l8
(11)2
/@1(11)24 y08 $DE $0C $19 $2D
g^^f+^^
a^^g^f+
g^^f+^^
a^^f+^^
^2^4
^^4
(11)4 $DE $30 $12 $2D
c^^d^^
e^^d^^
c^^d^^
r2r4 $DF 
@34v220o2b2^4o4
(11)6
(41)[c^^<b^^
a+^^a^^]
(11)6
(41)2
e2^4
^2^4


#5 q7F v230 y15 o4 l16
(11)2
/(11)2
(51)[ y10 @38c8 y14 @22dddd]24
(11)5l8
^^^l16 $F4 $03 @13 $ED $4E $8F gf+l8 t20 d+^ v255 $F4 $03 t35 
[l16 y10 @38cc@10c@38c@10c8
l8@38cc@10c]7
l16@38cc@10c@38c@10c8
@38cr@10crrr
(11)
^^^4
(11)2
v0$E8 $C8 $E6
 y10 @38c8 y14 @22dddd
 y10 @38c8 y14 @22dddd
 y10 @38c8 y14 @22dddd
 y10 @38c8 y14 @22dddd
(51)6
[y10@38c8@38c8 y14 @22dd]2
y10 @38c2^4
(51)36
(11)2

#4 q7F v255 y12 @1 o2 l8
(11)2
/(11)6
(71)[b^^a+^^
a^^g^^
f+^^g+^^]
f+^^g+^^
(71)
f+^^g+^^
e2^4
^2^4
(11)6
(11)2 $DE $06 $0C $30 
v200 @0 o3l8
(72)[gl16ef+grl8
<bl16gabrl8>
c<l16ab>crl8]
gl16f+ed+rl8
(72)l8
g^l16f+el8
f+2^4
^^4 v255
(11)8
@34b2^4
@1o2
(73)[b^^a+^^
a^^g^^
a^^g^^
f+^^f+^^
b^^a+^^
a^^g^^]
(74)[>c^^<b^^
a+^^a^^]
(73)
(74)2
(11)2                
#6
r1
r2/r2
r1
r1
r1
r1
r1
r1
r1
r1
r1
r1
r1
r1
r1
r1r2
v200 y08 $F4 $03 
[o4 @35 c16c16 @37 c8 @35 c8c8c8c8]8 r1r1r1r1r1r1r1r2.. $F4 $03 v180 y08 @30 l16 
(60)[o3b8>e<b>e<bb8>e8<b8
a8>e<a>e<ab-8>e8<b-8
b8>e<b>e<bb8>e8<b8
a8>f+<a>f+<a>f+8<a8>f+8
<b8>e<b>e<bb8>e8<b8
a8>e<a>e<ab-8>e8<b-8 l8]1
(61)[>fd<b>fd<a+>fd<a>fd<g>l16]1
(60)
(61)2
(11)2
#amk=1
