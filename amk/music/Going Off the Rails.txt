;Revised by crm0622.

#amk 2
#path "GoingOffTheRails"
#spc {
	#author "Moose"
	#game "SMW Central Production 2"
	#comment "Carnival Level 3"
	#title "Going Off the Rails"
}

#samples
{
#default
"sample_00.brr"
"sample_01.brr"
"sample_02.brr"
"sample_03.brr"
"sample_04.brr"
"sample_05.brr"
"sample_06.brr"
"sample_07.brr"
"sample_08.brr"
}

$EF $FF $00 $00
$F1 $01 $9C $01
$F5 $1B $17 $15 $13 $0C $0B $FE $FB

#instruments
{
"sample_00.brr" $FF $F2 $B8 $05 $F9 ;@30
"sample_01.brr" $FF $C8 $B8 $07 $F9 ;@31
"sample_02.brr" $FF $E0 $B8 $0A $18 ;@32
"sample_03.brr"	$FF $F6 $B8 $0D $0B	;@33(@14)
"sample_04.brr" $FF $F4 $7F $02 $AB ;@34(@2)
"sample_05.brr" $FF $F6 $BA $08 $FE ;@35(@0)
"sample_06.brr" $AF $76 $00 $02 $00 ;@36(@21)
@10 $FF $F4 $7F $05 $AB ;@37
@29 $FF $F2 $7F $02 $86 ;@38
"sample_07.brr" $FF $E8 $7F $02 $9D ;@39(@15)
"sample_08.brr"	$FD $F8 $B8 $02 $40	;@40(@22)
"sample_03.brr"	$FF $F0 $B8 $0D $0B	;@41(@14)
}

#0 w255 t66 $F4 $02 q7f
(1)[@36 v150 o4 y9 c8 @37 c8 @36 c8 @37 c8 @36 c8 @37 c16 @37 q7c c16 @36 q7f c8 @37 c8]3
@36 q7f c8 @37 c8 @37 c16 @36 c16 @36 c16 @36 c16 @37 c4 @36 c4
(1)2
(2)[@36 c8 @36 c8 @38 c8 @36 c8 @36 c8 @36 c8 @38 c4 @36 c8 @36 c8 @38 c4 @36 c8 @36 c8 @38 c4]
(3)[@36 c8 @36 c8 @38 c8 @36 c8 @36 c8 @36 c8 @38 c4 @36 c8 @36 c8 @38 c4 @38 c8 @38 c8 @38 c4]
(1)6
(2)
(3)
(1)2
@36 c4 @38 c12 c12 c12 @36 c6 @38 c6 c6 @36 c8 @38 c16 c16 @36 c8 @38 c8 @36 c2
(1)3
@36 o4 c8 @37 c8 @36 c8 @37 c8 @36 c8 @37 c16 @37 q7c c16 @36 q7f c4
(1)4
(2)3
(3)
(1)13
[r1]3 r2 @36 c4 @38 c12 c12 c12
(2)
(3)

#1
@33 v137 o2 y10 f4 c4 f4 c4 c4 g4 f4 c4 f4 c4 f4 c4<g4>d4 c8 r4^8 f4 c4 f4 c4 c4 g4 f4 c4
(4)[@30 a+8 a+8 a+8 a+8 a8 a8>d4<g8 g8>c4<f8 f8 f4]2
@33 f4 c4 f4 c4<a+4>f4<a+4>f4 c4 f4 c4<f4 a+4>f4<a+4>f4 c4 g4 c4 g4 f4 c4 f4 c4
(4)2
@33 f4 c4 f4 c4 f4 c4 f4 c4<g2 a+6 a+6 a+6 g2 r2
[>c4<g4 a+4 f4]3 >c4<g4 a+4 c4 [>c4<g4 a+4 f4]3 >c4<g4 a+4>c4
[@30 >f8 f8 f8 f8 e8 e8 a4 d8 d8 g4 c8 c8 c4]4
@33 <c4<g4>c4<g4 f4>c4<f4>c4<g4>c4<g4>c4<f4>c4<f4>c4
<g4>d4<g4>d4 c4<g4>c4<g4 a+4 a+4>d+4 d+4<g+4 g+4 g+4 g+4 g4 g4>c4 c4<g+4 g+4 g+4 g+4 g4 g4>c4 c4
@41 v112 <f2 g2 g+2 @33 a8 f8>f12 c12<a12
v137 >(4)4

#2
@31 v125 o4 y10 q6e c4 c8. <b16>c16 r16<a8. r16 a16 r16 a+16 r16 a16 r16 g+16 r16 a16 r16 f4 r8 f16 r16
>c4 c8. <b16>c16 r16<a8. r16 a16 r16 b16 r16 b16 r16>c16 r16 d16 r16 e8 @34 o4 g16 $DD $00 $0C $B6 r16 @2 c4
@31 o4 c4 c8. <b16>c16 r16<a8. r16 a16 r16 a+16 r16 a16 r16 g+16 r16 a16 r16 f4 r8 f16 r16
>(6)[y6 q2e d8 d8 c+8 c+8 c8 c8 d8 r8<a+8 a+8>c8 r8<a8 a8 a8 r8]
>(7)[y6 d8 d8 c+8 c+8 c8 c8 d8 r8 f8 f8 g8 r8 f8 f8 f8 r8]
y10 <q6e f4 f8. e16 f4 d+4 d8 c8 d8 d+8 f4 r8 q2e f8 q6e c16<b16>c16 c+16 d8. d+16 f4 r4<a+16 a16>c16<a+16>d8. d+16 f4 r8 q2e f8
q6e e16 d+16 e16 d+16 e8 f8 g4 f8. e16 f16 e16 f16 e16 f8 g8 a4 q7e a16 g+16 g16 f16
q2e >(6)
(7)
y10 q6e <f8. e16 f8 g8 a4 f4 a+8. a16 g8 a+8 a4 f4 g4 g12 g12 g+12 a+6 g+6 f6 g2 r8 @34 o4 g16 $DD $00 $0C $B0 r16 @2 g4
@31 o4 y6 q7e (5)[c16 r16 c16<b16>c16 r16<g16 r16 a+8. a16 g8 f8 e16 f16 g16 e16 f16 r16 d16 r16<a+8 a16 r16 g8>c16 r16]
>c16 r16 c16<b16>c16 r16<g16 r16 a+8. a16 g8 f8>c16 r16<g24 a+24>c24 e8 c16 r16<a+8 g16 r16 @39 v150 >c4
@31 v125
(5)
c16 r16 c16<b16>c16 r16<g16 r16 a+8.a16 g8 f8>c16 r16<g24 a+24>c24 e8 c16 r16<a+8 g16 r16>c8. <a+16
[q2e a8 a8 g+8 g+8 g8 g8 a8 r8 f8 f8 g8 r8 e8 e8 e8 r8]3
q2e a8 a8 g+8 g+8 g8 g8 a8 r8>c8 c8 d8 r8 c8 c8 c8 r8
y10 q6e c4 c8. <b16>c4<a+4 a8 g8 a8 a+8>c4 r8 q2e c8 q6e <g16 f+16 g16 g+16 a8. a+16>c4 r4<f16 e16 g16 f16 a8. a+16>c4 r8 q2e c8
q6e <b16 a+16 b16 a+16 b8>c8 d4 c8. <b16>c16<b16>c16<b16>c8 d8 e4 q7e e16 d+16 d16 c16
q6e c+4<g12 a+12>c+12 f4 d+8. c+16 c16 c+16 c16 c+16 c8<g+8>c2
<a+4>c+16 c16<a+16 g+16 g4>c8. <a+16 g+8. g16 g+8. a+16>c4<g+4
a+4 a+8. g+16 g8>c+8 c8<g8
y6 q7e a2 a+2 b2>c4 f12 e12 d+12
q2e (6)3
d8 d8 c+8 c+8 c8 c8 d8 r8 f8 f8 g8 r8 f8 f8 @39 v150 q7e f4

#3
@32 v162 o4 y6 q78 r8 f8 r8 f8 r8 f8 r8 f8 r8 e8 r8 e8 r8 f8 r8 f8
r8 f8 r8 f8 r8 f8 r8 f8 r8 f8 r8 f8 e8 r4.
r8 f8 r8 f8 r8 f8 r8 f8 r8 e8 r8 e8 r8 f8 r8 f8
@31 o3 y14 q2d (8)[a+8 a+8 a+8 a+8 a8 a8 a8 r8 g8 g8 g8 r8 f8 f8 f8 r8]
(9)[a+8 a+8 a+8 a+8 a8 a8 a8 r8 a+8 a+8 a+8 r8 a8 a8 a8 r8]
@32 o4 y6 q78 [r8 f8 r8 f8 r8 f8 r8 f8]4 r8 e8 r8 e8 r8 e8 r8 e8 r8 f8 r8 f8 r8 f8 r8 f8
@31 o3 y14 q2d
(8)
(9)
@32 o4 y6 q78 r8 f8 r8 f8 r8 f8 r8 f8 r8 f8 r8 f8 r8 f8 r8 f8<b4 b12 b12>c12 d6 c6 d6<b2 r2
@31 o3 y14 q7d (10)[e16 r16 e16 g16 e16 r16 c16 r16 f4 d4 c16 d16 e16 c16 d16 r16<a+16 r16 g8. r16 d8 e16 r16]
>e16 r16 e16 g16 e16 r16 c16 r16 f4 d4 e16 r16 c24 d24 e24 g8 e16 r16 d8<a+16 r16 @39 v137 >c4
@31 v162 y14 (10)
e16 r16 e16 g16 e16 r16 c16 r16 f4 d4 e16 r16 c24 d24 e24 g8 e16 r16 d8<a+16 r16>e8. r16
q2d [f8 f8 f8 f8 e8 e8 e8 r8 d8 d8 d8 r8 c8 c8 c8 r8]3
f8 f8 f8 f8 e8 e8 e8 r8 f8 f8 f8 r8 e8 e8 e8 r8
@32 o5 y6 q78 [r8 c8 r8 c8 r8 c8 r8 c8]4 r8<b8 r8 b8 r8 b8 r8 b8 r8>c8 r8 c8 r8 c8 r8 c8
r8 c+8 r8 c+8 r8 d+8 r8 d+8 r8 c8 r8 c8 r8 c8 r8 c8
r8<a+8 r8 a+8 r8>c8 r8 c8 r8<g+8 r8 g+8 r8 g+8 r8 g+8 r8 g8 r8 g8 r8 g8 r8 g8
@31 o3 y14 q7d f2 g2 g+2 a2
q2d (8)3
a+8 a+8 a+8 a+8 a8 a8 a8 r8 a+8 a+8 a+8 r8 a8 a8 @39 v137 q7e f4

#4
@32 v162 o3 y14 q78 r8 a8 r8 a8 r8 a8 r8 a8 r8 g8 r8 g8 r8 a8 r8 a8
r8 a8 r8 a8 r8 a8 r8 a8 r8 b8 r8 b8>c8 r4.
<r8 a8 r8 a8 r8 a8 r8 a8 r8 g8 r8 g8 r8 a8 r8 a8
(11)[@35 v112 o3 y10
q7b d16 f16 a+16>d16<d16 f16 a+16>d16<c+16 e16 g16>c+16<d16 f16 a16>d16<d16 g16 a+16>d16<c16 e16 g16>c16<c16 f16 a16>c16<c16 f16 a16>c16
o3 d16 f16 a+16>d16<d16 f16 a+16>d16<c+16 e16 g16>c+16<d16 f16 a16>d16<d16 g16 a+16>d16<c16 e16 g16>c16<c16 f16 a16>c16<f4]
@32 v162 o3 y14 q78 [r8 a8 r8 a8 r8 a8 r8 a8 r8 a+8 r8 a+8 r8 a+8 r8 a+8]2
r8 a+8 r8 a+8 r8 a+8 r8 a+8 r8 a8 r8 a8 r8 a8 r8 a8
(11)
@32 o3 y14 q78 r8 a8 r8 a8 r8 a8 r8 a8 r8 a8 r8 a8 r8 a8 r8 a8 g4 g12 g12 g+12 a+6 g+6 a+6 g2 r2
[r1]8
@35 v112 o2 y10 q7b
[a16>c16 f16 a16<g+16>c16 f16 g+16<g16 b16>e16 g16<a16>c16 e16 a16<a16>d16 f16 a16<g16 b16>d16 g16<g16>c16 e16 g16<g16>c16 e16 g16]2
o3 a16>c16 f16 a16<g+16>c16 f16 g+16<g16 b16>e16 g16<a16>c16 e16 a16<a16>d16 f16 a16<g16 b16>d16 g16<g16>c16 e16 g16<g16>c16 e16 g16
o3 a16>c16 f16 a16<g+16>c16 f16 g+16<g16 b16>e16 g16<a16>c16 e16 a16<a16>d16 f16 a16<b16>d16 g16 b16 c16 e16 g16>c16<c4
@32 v162 o4 y14 q78 [r8 e8 r8 e8 r8 e8 r8 e8 r8 f8 r8 f8 r8 f8 r8 f8]2 r8 f8 r8 f8 r8 f8 r8 f8 r8 e8 r8 e8 r8 e8 r8 e8
r8 f8 r8 f8 r8 g8 r8 g8 r8 f8 r8 f8 r8 f8 r8 f8
r8 d8 r8 d8 r8 e8 r8 e8 r8 c8 r8 c8 r8 c8 r8 c8 r8 d8 r8 d8 r8 e8 r8 e8
[r1]6
@35 v112 o3 y10
q7b d16 f16 a+16>d16<d16 f16 a+16>d16<c+16 e16 g16>c+16<d16 f16 a16>d16<d16 g16 a+16>d16<c16 e16 g16>c16<c16 f16 a16>c16<c16 f16 a16>c16
o4 d16 f16 a+16>d16<d16 f16 a+16>d16<c+16 e16 g16>c+16<d16 f16 a16>d16<d16 g16 a+16>d16<c16 e16 g16>c16<c16 f16 a16>c16<f4

#5
(12)[@40 v112 o5 y10 q7e c8 @40 c16 @40 c16]14
@40 c8 r4.
(12)22
@40 c8 @40 c8 @40 c4
(12)38
@40 c8 @40 c8 @40 c4
(12)8
@40 c4 c12 c12 c12 c6 c6 c6 c8 c16 c16 c8 c16 c16 c2
(12)15 @40 c8 r8
(12)47 @40 c8 r8
(12)52
@40 c8 c8 c8 c8 c8 c8 c4 c8 c8 c4 c8 c8 c4
c8 c8 c8 c8 c8 c8 c4 c8 c8 c4 c8 c8 c16 c16 c16 c16
(12)15 @40 c8 r8                
