;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Vine Ledge - Acts like a ledge when standing on it, press down and acts like a vine.
;Unlike VineCloud, the climbing value is automatically set so Mario doesn't just fall
;if down isn't being pressed. When climbing up, it pops Mario up above the ledge so he
;doesn't stick at the top.
;
;For BTSD
;
;An ASM Hax�
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball
JMP TopCorner : JMP BodyInside : JMP HeadInside

MarioBelow:
MarioAbove:
MarioSide:
TopCorner:
BodyInside:
HeadInside:
	LDA $74
	BNE climbing
	LDA $15
	BIT #$08
	BNE climb_up_maybe
	AND #$04
	BEQ ledge
	
sides_check:
	REP #$20
	LDA $9A
	AND #$FFF0
	SEC
	SBC $94
	SEP #$20
	CMP #$FC		; too much to the right
	BMI ledge
	CMP #$06		; too much to the left
	BPL ledge
	
make_climb:
	LDA #$1F
	STA $8B
	RTL
	
climbing:
	LDA $15
	AND #$08
	BEQ make_climb
	LDA $74			; maybe
	CMP #$1D
	BEQ make_climb
	CMP #$1F		; LOL it works
	BEQ make_climb
	CMP #$14
	BEQ make_climb
	RTL
	
climb_up_maybe:
	REP #$20
	LDA $98
	AND #$FFF0
	SEC
	SBC $96
	SEP #$20
	CMP #$1F		; it's $1F-$20 when on top
	BMI sides_check

SpriteV:
SpriteH:
MarioCape:
MarioFireball:

ledge:
	LDY #$01
	STZ $1693|!addr
return:
	RTL

print "Acts like a ledge when standing on it, press down and acts like a vine."