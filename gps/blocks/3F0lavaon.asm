;Act like tile 5.

db $42
JMP mario : JMP mario : JMP mario : JMP Main : JMP Main : JMP Main : JMP Main : JMP mario : JMP marioIN : JMP marioIN

mario:
	LDA $14AF
	BNE solid

LDA $75			;load player is in water flag
BEQ Return		;if not, return.

marioIN:
PHY			;preserve Y
JSL $00F606|!bank	;kill the player
PLY			;restore Y
	RTL

Main:
	LDA $14AF
	BEQ Return

solid:
	LDY #$01
	LDA #$30
	STA $1693
Return:
	RTL