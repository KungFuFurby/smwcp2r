db $42
JMP Main : JMP Main : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Main:
	LDA #$15
	STA $1DFC|!addr

	LDA $7FC075
	CLC
	ADC #$01
	STA $7FC075

	LDA $7FC075
	CMP #$03
	BNE erase

	LDA #$29
	STA $1DFC|!addr

	LDA $7FC0FC
	ORA #$01
	STA $7FC0FC

erase:
	LDA #$02		;Load #$02 to A.
	STA $9C			;Store it to Map16 Tile Generator.
	JSL $00BEB0|!bank	;Generate the Blank Tile.
	LDY #$00		;Leave the Y Index with Zero.
Return:
	RTL			;Return from this Code.