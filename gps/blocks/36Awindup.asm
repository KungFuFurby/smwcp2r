; Wind_Up Block
; by Superyoshi
; -PERSONAL COMMENT------
; My first block!
; Thanks to InvisibleCoinBlock for helping me!
; -----------------------
; If Mario touches this he will slowly fly up
; act like 25
; -----------------------
; CUSTOMIZE
; Change F0 in lda #$F0 to the speed mario should fly up
; -----------------------
; LICENSE
; Credit is required, but it doesn't have to be in the ending.
db $42
JMP MarioTouch : JMP MarioTouch : JMP MarioTouch : JMP Return : JMP Return : JMP Return : JMP Return : JMP MarioTouch : JMP MarioTouch : JMP MarioTouch

MarioTouch:
	LDA #$FF	; Y speed Mario should have
	STA $7D		; set actual Y speed to the above value
	RTL		; return

Return:
	RTL