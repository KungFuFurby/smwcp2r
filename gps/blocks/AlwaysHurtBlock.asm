db $37

JMP Main : JMP Main : JMP Main : JMP return : JMP return : JMP return : JMP return
JMP Main : JMP Main : JMP Main
JMP Main : JMP Main

Main:
PHX		;preserve X just in case
PHY		;preserve Y just in case

JSL $80F5B7	;hurt player subroutine

PLY		;restore Y.
PLX		;restore X.

return:
RTL		;return.

print "A block that hurts the player from all sides. Unlike the muncher, this block will always hurt, regardless of the Silver P-Switch status."