db $42

JMP Mario : JMP Mario : JMP Mario : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

Mario:
LDA $7FC0F8
AND #$01	;one shot trigger 0 flag
BEQ Return
LDA $7FC08A	;exanimation slot A
BNE Return

	LDA #$80
	STA $7D
	STA $1406|!addr
	LDA #$FF
	STA $1404|!addr
	RTL

Sprite:
LDA $7FC0F8
AND #$01	;one shot trigger 0 flag
BEQ Return
LDA $7FC08A	;exanimation slot A
BNE Return

	LDA !14C8,x
	CMP #$02
	BEQ Return
	LDA #$90
	STA !AA,x
Return:
	RTL


print "This block launches both player and sprites upwards with high speed only when One Shoot Trigger 0 frame is zero and active."