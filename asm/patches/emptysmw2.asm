;BD note: this isn't patched with the batch because it's meant to be a ROM cleaner.
;As of August 7th, 2020, I apply this patch to all my base ROMs when porting them, and I
;patch it before doing anything else, even LM modifications.

if read1($00FFD5) == $23
        sa1rom
endif

;   Default sprite memory setting.  If you're using No Sprite Tile Limits, you may want to set this to 10.
!SprMem         = $10

;   Setting this to 1 will nuke the No Yoshi intros too, pointing them to blank levels. Set to 0 to restore them.
!KillEntrances  = 1

org $05E000
        rep 512 : dl $078000            ;   repoint all Layer 1 pointers
        rep 512 : dl $FFE8EE            ;   repoint all Layer 2 pointers
        rep 512 : dw $8006              ;   repoint all sprite pointers
        pad $05FFFF                     ;   clear all data in tables $05F000-$05FFFF

padbyte $FF
org $078000
        db $00,$00,$00,$00,$00,$FF      ;   blank data for layer 1
        db !SprMem,$FF                  ;   blank data for sprites
        pad $0780EC                     ;   clear all level data from $078008-$0780EC

org $068000
        pad $06A5B9                     ;   clear all level data from $068000-$06A5B8

org $06A600
        pad $06C964                     ;   clear all level data from $06A600-$06C963

org $06D000
        pad $06F539                     ;   clear all level data from $06D000-$06F538

org $078100
        pad $07A179                     ;   clear all level data from $078100-$07A178

org $07A600
        pad $07C226                     ;   clear all level data from $07A600-$07C225

org $07C300
        pad $07E76F                     ;   clear all level data from $07C300-$07E76E

if !KillEntrances == 1
        org $05DA17
                JMP $DAD7               ;   skip past the No Yoshi handler
        org $05D766
                pad $05D78A             ;   clear the pointers (so, free space)
else
        if read1($05DA17) == $4C
                org $05DA17
                        SEP #$30
                        db $AD
        endif

        org $05D766
                dl NY1FG
                dl NY2FG
                dl NY3FG
                dl NY4FG
                dl NY5FG
                dl NY6FG

                dl $FFE8EE
                dl $FFD900
                dl $FFD900
                dl $FFE684
                dl $FFDF59
                dl $FFE8EE
        org $078008
        NY1FG:
                db $C0,$6F,$49,$04,$05,$6C,$31,$03,$6E,$34,$02,$70,$30,$01,$14,$00
                db $82,$79,$20,$1F,$0B,$06,$80,$FF
        NY2FG:
                db $20,$4E,$39,$0B,$01,$14,$00,$82,$79,$B0,$1F,$0B,$07,$84,$FF
        NY3FG:
                db $20,$4E,$29,$11,$02,$78,$C0,$05,$78,$C6,$0A,$79,$D1,$13,$79,$D7
                db $19,$FF
        NY4FG:  ;   really, only one byte is different
                db $20,$4E,$29,$11,$06,$78,$C0,$05,$78,$C6,$0A,$79,$D1,$13,$79,$D7
                db $19,$FF
        NY5FG:  ;   again, only one byte is different!
                db $20,$4E,$29,$11,$08,$78,$C0,$05,$78,$C6,$0A,$79,$D1,$13,$79,$D7
                db $19,$FF
        NY6FG:  ;   another 1 byte difference...
                db $20,$6E,$39,$0B,$01,$14,$00,$82,$79,$B0,$1F,$0B,$07,$84,$FF
endif

