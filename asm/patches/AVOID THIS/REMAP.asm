;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; LM1.7x Tilemap Remapper Patch v1.2.2 (for LM1.91+)
; coded by edit1754
;
; must be patched to a LM1.7+ edited ROM
;
; Allows you to remap the layer 1 and 2 tilemaps in the VRAM
; Allows you to directly swap layer 1 and 2 right as they get uploaded
; Allows you to disable uploading tiles to certain layers
;  - useful if you want to upload tilemaps yourself with your own code (ex: bypassing BG map16)
;
; To change the VRAM address for a certain layer for a certain level, find
; the level's entry in the correct table. This number is the high byte of
; one-half of the starting VRAM address.
;
; To swap layers 1 and 2, enable the first flag in the Extra Flags table
; for your level. So %0000 -> %0001
;
; To disable L1_Load/L1_Gameplay, enable the second flag in the Extra Flags table
; for your level. So %0000 -> %0010
;
; To disable L2_Load/L2_Gameplay, enable the third flag in the Extra Flags table
; for your level. So %0000 -> %0100
;
; To force layer priority in the FG, enable the fourth flag in the Extra Flags table
; for your level. So %0000 -> %1000
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
header
lorom

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Things you can change
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!FreeRAM1 = $067A	; 2 bytes	\  change if you have an
		!FreeRAM2 = $067C	; 2 bytes	 | ASM hack that uses them
		!FreeRAM3 = $067E	; 1 byte	/ 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Things you probably shouldn't change
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!LevelNum = $010B

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; levelnum.ips (disassembly) - credit goes to BMF for this
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ORG $05D8B9
		; JSR LevelNumCode

; ORG $05DC46
; LevelNumCode:	LDA.b $0E  		; Load level number, 1st part of hijacked code
		; STA.w !LevelNum		; Store it in free RAM
		; ASL A    		; 2nd part of hijacked code
		; RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Hijacks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
org read3($00A5A3)+$10
    hijacks_start:

org $1FA450
old_hijacks_start:

!hijacks_diff = (old_hijacks_start-hijacks_start)

org $1FA450-!hijacks_diff
autoclean	JML SetUpScreen_Hijack
		NOP #6

org $1FA505-!hijacks_diff-2
		ORA.w !FreeRAM1		; .commitl1

org $1FA575-!hijacks_diff-2
		ORA.w !FreeRAM2		; .commitl2

org $1FA7DB-!hijacks_diff-2
		JML L1Load_Hijack	; L1Load
		NOP

org $1FA84F-!hijacks_diff-2
		JML L2Load_Hijack	; L2Load
		NOP

org $1FA8C3-!hijacks_diff-2
		JSL L1Gameplay_Hijack	; L1GameplayX
		NOP

org $1FA92C-!hijacks_diff-2
		JSL L1Gameplay_Hijack	; L1GameplayY
		NOP

org $1FA995-!hijacks_diff-2
		JSL L2Gameplay_Hijack	; L2GameplayX
		NOP

org $1FA9FE-!hijacks_diff-2
		JSL L2Gameplay_Hijack	; L2GameplayY
		NOP

org $1FAA85-!hijacks_diff-2
 		ORA.w !FreeRAM1

org $1FAA90-!hijacks_diff-2
		AND.w #$7C1F		; increase range

org $1FAB1C-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FABA7-!hijacks_diff-2
		ORA.w !FreeRAM1

org $1FAC4B-!hijacks_diff-2
		ORA.w !FreeRAM1

org $1FACBF-!hijacks_diff-2
		ORA.w !FreeRAM1

org $1FAB27-!hijacks_diff-2
		AND.w #$7C1F		; increase range

org $1FAD6A-!hijacks_diff-2
		ORA.w !FreeRAM1

org $1FADEF-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FAE98-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FAF12-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FAF84-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FB0CF-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FB145-!hijacks_diff-2
		ORA.w !FreeRAM2

org $1FB55E-!hijacks_diff-2
		ORA.w !FreeRAM2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

freecode

reset bytes

SetUpScreen_Hijack:
		REP #%00110000
		LDA $010B
		LDX #$0000
.Loop		CMP.l LevelDataTable+0,x
		BCC .LoopNext
		CMP.l LevelDataTable+2,x
		BCS .LoopNext
.Use		SEP #%00100000
		LDA.l LevelDataTable+4,x
		STA $2107
		AND.b #%11111100
		STA.w !FreeRAM1+1
		STZ.w !FreeRAM1
		LDA.l LevelDataTable+5,x
		STA $2108
		AND.b #%11111100
		STA.w !FreeRAM2+1
		STZ.w !FreeRAM2
		LDA.l LevelDataTable+6,x
		STA.w !FreeRAM3
.Return		SEP #%00010000
		JML $9FA45A-!hijacks_diff	; jump back to custom screen setup subroutine
		
.LoopNext	TXA
		CLC
		ADC.w #$0007
		TAX
		LDA $010B
		BRA .Loop

L1Load_Hijack:
		SEP #%00100000		; 8-bit A
		LDA.w !FreeRAM3		; \
		AND.b #%00000010	;  | check if disabled
		BNE Cancel_L1_L2_Load	; /
		SEP #%00110000		; \ hijacked
		LDX.w $0100		; / code
		JML $9FA7E0-!hijacks_diff-2	; go back

L2Load_Hijack:
		SEP #%00100000		; 8-bit A
		LDA.w !FreeRAM3		; \
		AND.b #%00000100	;  | check if disabled
		BNE Cancel_L1_L2_Load	; /
		SEP #%00110000		; \ hijacked
		LDX.w $0100		; / code
		JML $9FA854-!hijacks_diff-2	; go back

Cancel_L1_L2_Load:
		SEP #%00110000
		RTL

L1Gameplay_Hijack:
		SEP #%00100000		; 8-bit A
		LDA.w !FreeRAM3		; \
		AND.b #%00000010	;  | check if disabled
		BNE Cancel_L1_L2_GP	; /
		SEP #%00110000		; \ hijacked
		LDA.w $1925		; / code
		RTL

L2Gameplay_Hijack:
		SEP #%00100000		; 8-bit A
		LDA.w !FreeRAM3		; \
		AND.b #%00000100	;  | check if disabled
		BNE Cancel_L1_L2_GP	; /
		SEP #%00110000		; \ hijacked
		LDA.w $1925		; / code
		RTL

Cancel_L1_L2_GP:
		PLA
		PLA
		PLA
		SEP #%00110000
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; "Compressed" table of level data.
; Inclusive lower bound, Exclusive upper bound, layer 1 VRAM, Layer 2 VRAM, Extra Flags
; Layer 1 VRAM / Layer 2 VRAM: $addresshighbyte|%size
; Extra Flags:
; - first bit is swap layers
; - 2nd bit is disable layer 1 upload
; - 3rd bit is disable layer 2 upload
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LevelDataTable:
		dw $0000,$0200 : db $30|%01,$38|%01,%0000	; Levels 0-1FF
		dw $0000,$0200 : db $30|%01,$38|%01,%0000	; Levels 0-1FF
		dw $0000,$0200 : db $30|%01,$38|%01,%0000	; Levels 0-1FF
		dw $0000,$0200 : db $30|%01,$38|%01,%0000	; Levels 0-1FF
		dw $0000,$0200 : db $30|%01,$38|%01,%0000	; Levels 0-1FF
		
print bytes